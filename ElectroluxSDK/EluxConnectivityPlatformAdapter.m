//
//  EluxConnectivityPlatformAdapter.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxConnectivityPlatformAdapter.h"
#import "EluxApplianceWifiScanDelegate.h"

@implementation EluxConnectivityPlatformAdapter

+(NSMutableArray*)createNewAdapters:(unsigned long)numberOfSupportedPlatforms
{
    if(numberOfSupportedPlatforms < 1)
        return nil;
    NSMutableArray* arr = [NSMutableArray arrayWithCapacity:numberOfSupportedPlatforms];
    for(int i = 0; i < numberOfSupportedPlatforms; i++)
    {
        EluxConnectivityPlatformAdapter* adapter = [[super alloc]init];
        [arr addObject:adapter];
    }
    return arr;
}

-(BOOL)connect:(id<EluxApplianceDiscoveryDelegate>)listener
{
    NSLog(@"Error: Supposed to be overriden");
    return NO;
}
-(BOOL)disconnect
{
    NSLog(@"Error: Supposed to be overriden");
    return NO;
}
-(BOOL)registerConnectionStateListener:(id<EluxPlatformConnectionStateDelegate>)listener
{
    if(!mConnectionStateListeners)
        mConnectionStateListeners = [[NSMutableArray alloc]init];
    if([mConnectionStateListeners containsObject:listener])
        return NO;
    [mConnectionStateListeners addObject:listener];
    return YES;
}
-(BOOL)unregisterConnectionStateListener:(id<EluxPlatformConnectionStateDelegate>)listener
{
    if(!mConnectionStateListeners)
        return NO;
    if(![mConnectionStateListeners containsObject:listener])
        return NO;
    [mConnectionStateListeners removeObject:listener];
    return NO;
}
-(BOOL)assignPlatformAppliance:(id<EluxPlatformApplianceProtocol>)platformAppliance toEluxAppliance:(EluxAppliance*)appliance
{
    NSLog(@"Error: Supposed to be overriden");
    return NO;
}
-(id<EluxPlatformApplianceProtocol>)getPlatformAppliance:(EluxAppliance*)appliance
{
    NSLog(@"Error: Supposed to be overriden");
    return nil;
}
-(void)onboard          :(EluxAppliance*)appliance
         withWifiScanner:(EluxGenericWifiScan*)netInfo
       appliancePassword:(NSString*)appliancePassword
   andOnboardingListener:(id<EluxOnboardingDelegate>)listener
{
    NSLog(@"Error: Supposed to be overriden");
}
-(void)onboard          :(EluxAppliance*)appliance
                  toSSID:(NSString*)SSID
            withPassword:(NSString*)password
   andOnboardingListener:(id<EluxOnboardingDelegate>)listener
{
    NSLog(@"Error: Supposed to be overriden");
}
-(void)offboard:(EluxAppliance*)appliance withOffboardingListener:(id<EluxOffboardingDelegate>)listener
{
    NSLog(@"Error: Supposed to be overriden");
}
-(BOOL)wifiScan:(EluxAppliance*)appliance withApplianceWifiScan:(EluxApplianceWifiScan*)scanResult
{
    NSLog(@"Error: Supposed to be overriden");
    return NO;
}

-(BOOL)wifiScan:(EluxAppliance*)appliance withApplianceWifiScanDelegate:(id<EluxApplianceWifiScanDelegate>)scanResult
{
    NSLog(@"Error: Supposed to be overriden");
    return NO;
}

-(BOOL)searchForAvailableAppliances:(id<EluxApplianceDiscoveryDelegate>)listener
{
    NSLog(@"Error: Supposed to be overriden");
    return NO;
}

-(EluxRemoteProperty*)getPropertyWithName:(NSString*)name inIterface:(NSString*)interfaceName inObject:(NSString*)objectPath withPort:(NSInteger)portNumber andBusName:(NSString*)busName
{
    NSLog(@"Error: Supposed to be overriden");
    return nil;
}

-(BOOL)setPropertyWithName:(NSString*)name inIterface:(NSString*)interfaceName inObject:(NSString*)objectPath withPort:(NSInteger)portNumber andBusName:(NSString*)busName toValue:(NSString*)value
{
    NSLog(@"Error: Supposed to be overriden");
    return NO;
}

-(BOOL)startMessagesConsumer
{
    NSLog(@"Error: Supposed to be overriden");
    return NO;
}

-(void)stopMessagesConsumer
{
    NSLog(@"Error: Supposed to be overriden");
}

-(BOOL)registerListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit
{
    NSLog(@"Error: Supposed to be overriden");
    return NO;
}

-(void)unRegisterListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit
{
    NSLog(@"Error: Supposed to be overriden");
}

@end
