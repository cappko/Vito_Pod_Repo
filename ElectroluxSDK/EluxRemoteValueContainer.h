//
//  EluxRemoteValueContainer.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>
#import "EluxAppliance.h"


@interface EluxRemoteValueContainer : NSObject
{
    eluxPropertyType mType;
    id mValue;
}

-(EluxRemoteValueContainer*)initWithType:(eluxPropertyType)type andValue:(id)value;
-(enum eluxPropertyType)getType;
-(id)getValue;

@end
