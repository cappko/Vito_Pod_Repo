//
//  EluxOnboardingManager.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>
#import "EluxApplianceWifiScanDelegate.h"


@class EluxPlatformsManager;
@class EluxOnboardableDiscoveryManager;
@class EluxIskWifiManager;
@class EluxAppliance;
@class EluxSSIDInfo;

@protocol EluxOnboardableApplianceDiscoveryDelegate;
@protocol EluxOnboardingDelegate;
@protocol EluxOffboardingDelegate;
@protocol EluxApplianceWifiScanDelegate;

@interface EluxOnboardingManager : NSObject <EluxApplianceWifiScanDelegate>
{
    NSString* TAG;
    EluxPlatformsManager* mPlatformsManager;
    EluxOnboardableDiscoveryManager* mDiscoveryManager;
    id<EluxApplianceWifiScanDelegate> mWifiScanDeelgate;
    id<EluxOnboardingDelegate> mOnboardingDelegate;
    id<EluxOffboardingDelegate> mOffboardingDelegate;
    EluxIskWifiManager* mWifiHelper;
}


/**
 Creates an EluxOnboardingManager singleton
 
 @param pm Instance of EluxPlatformsManager
 @return If successfull returns an instance of EluxOnboardingManager, nil otherwise
 */
+(EluxOnboardingManager*)sharedInstanceWithPlatformManager:(EluxPlatformsManager*)pm;


/**
 Tries to find all SSIDs that are available to the appliance that is about to be onboarded

 @param listener An instance of class that conforms to EluxApplianceWifiScanDelegate protocol. The listener receives a list of available SSIDs or a particullar NSError.
 @return Returns YES if the process of searching successfully started, NO otherwise.
 */
-(BOOL)searchForAvailableSSID:(id<EluxApplianceWifiScanDelegate>)listener;

/**
 Tries to onboard the appliance, that the iOS device is connected to.

 @param appliance Can be nil. Just for future compatibility.
 @param ssidInfo An instance of choosen EluxSSIDInfo object, that was obtained by calling searchForAvailableSSID. May contain password. If not, delegate method passwordForSSID is called.
 @param listener An instance of class that conforms to EluxOnboardingDelegate protocol. The listener is informed when the onboarding finishes (successfully or not). Listener is also responsible for passing a password if needed.
 @return Returns YES if the onboarding process successfully started, NO otherwise.
 */
-(BOOL)onboardAppliance:(EluxAppliance*)appliance toSSID:(EluxSSIDInfo*)ssidInfo withListener:(id<EluxOnboardingDelegate>)listener;

/**
 Tries to offboard given appliance

 @param appliance Appliance that is supposed to be offboarded
 @param listener An instance of class that conforms to EluxOffboardingDelegate protocol. The listener is informed when the offboarding finishes (successfully or not).
 @return Returns YES if the offboarding process successfully started, NO otherwise.
 */
-(BOOL)offboardAppliance:(EluxAppliance*)appliance withListener:(id<EluxOffboardingDelegate>)listener;


/**
 Sets password for the given EluxSSIDInfo instance

 @param password Password to be set
 @param ssid EluxSSIDInfo instance 
 */
-(void)setPassword:(NSString*)password forSSID:(EluxSSIDInfo*)ssid;

@end
