//
//  EluxPlatformApplianceProtocol.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxPlatformApplianceProtocol.h"

#ifdef NOT_PROTOCOL_ONLY

@implementation EluxPlatformAppliance

-(BOOL)isSupported
{
    return NO;
}

-(enum EluxApplianceType)getType
{
    return UNKNOWN;
}

-(BOOL)connect
{
    return NO;
}

-(BOOL)isConnected
{
    return NO;
}

-(BOOL)disconnect
{
    return NO;
}

-(BOOL)getRemotePropertyValue:(EluxRemoteProperty*) prop withRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener
{
    return NO;
}

-(BOOL)setRemotePropertyValue:(EluxRemoteProperty*) prop
     withRemoteValueContainer:(EluxRemoteValueContainer*)value
  andRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegte>)listener
{
    return NO;
}


-(BOOL)setApplianceStateListener:(id<EluxApplianceStateDelegate>)listener
{
    return NO;
}

-(BOOL)removeApplianceStateListener:(id<EluxApplianceStateDelegate>)listener
{
    return NO;
}

/**
 * Return list of supported paths (identifiers) of subunits, that can be controlled
 * by this platform device.
 *
 * @return List os supported platform device subunit paths.
 */
-(NSArray*)getSupportedSubUnitPaths
{
    return nil;
}

/**
 *
 * @return an array of available remote methods.
 */
-(NSArray*)getRemoteMethods:(NSString*)subUnitPath
{
    return nil;
}


/**
 *
 * @return collection of available remoteproperties.
 */
-(NSArray*)getRemoteProperties:(NSString*)subUnitPath
{
    return nil;
}


/**
 * Triggrer invocation of method on a remote device.
 * @param remoteMethod AjMethod to be invoked.
 * @param args Arguments of method invocation.
 * @param listener Remote action progress listener.
 * @return true, if invocation was requested successfully, false otherwise.
 */

-(BOOL)invokeMethod         :(EluxRemoteMethod*)remoteMethod
               withArguments:(NSArray*)args
 andRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener
{
    return NO;
}


/**
 * Return information about method arguments
 *
 * @param remoteMethod remote method to be queried.
 *
 * TODO - to be removed
 * @deprecated, use method.getArgumentTypes instead.
 */
-(NSArray*)getMethodArgumentTypes:(EluxRemoteMethod*)remoteMethod __deprecated
{
    return nil;
}


/**
 * Check, if the appliance is onboarded.
 * @return true on success, false on fail
 */
-(BOOL)isOnboarded;
{
    return NO;
}


/**
 * Check, if the appliance can be onboarded to local network.
 * @return true on success, false on fail
 */
-(BOOL)isOnboardable
{
    return NO;
}

/**
 * Check, if the appliance can be offboarded from local network.
 * @return true on success, false on fail
 */
-(BOOL)isOffboardable
{
    return NO;
}

@end
#endif //NOT_PROTOCOL_ONLY

