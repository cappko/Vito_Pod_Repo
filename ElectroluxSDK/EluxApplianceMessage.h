//
//  EluxApplianceMessage.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

@class EluxAppliance;

@interface EluxApplianceMessage : NSObject

@property(nonatomic, retain)NSString* messageID;
@property(nonatomic, assign)NSString* messageType;
@property(nonatomic, retain)NSString* applianceID;
@property(nonatomic, retain)NSString* applianceName;
@property(nonatomic, retain)NSString* busName;
@property(nonatomic, retain)NSString* richIconUrl;
@property(nonatomic, retain)NSString* appId;
@property(nonatomic, retain)NSString* richAudioUrl;
@property(nonatomic, retain)NSString* richIconObjPath;
@property(nonatomic, retain)NSString* richAudioObjPath;
@property(nonatomic, retain)NSString* text;
@property(nonatomic, retain)NSString* lang;

@end



@protocol EluxApplianceMessagesDelegate <NSObject>

/**
 * Called when an appliance broadcasts a message
 *
 * @param message A message broasted by an appliance
 * @param appliance The appliance that broadcasted the message
 */
-(void)didReceiveMessage:(EluxApplianceMessage*)message FromAppliance:(EluxAppliance*)appliance;

@end
