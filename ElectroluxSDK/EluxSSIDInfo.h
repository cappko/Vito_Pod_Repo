//
//  EluxSSIDInfo.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

static NSString * const AUTH_OPEN_STR = @"OPEN";
static NSString * const AUTH_WEP_STR = @"WEP";
static NSString * const AUTH_WPA_AUTO_STR = @"WPA_AUTO";
static NSString * const AUTH_WPA_CCMP_STR = @"WPA_CCMP";
static NSString * const AUTH_WPA_TKIP_STR = @"WPA_TKIP";
static NSString * const AUTH_WPA2_AUTO_STR = @"WPA2_AUTO";
static NSString * const AUTH_WPA2_CCMP_STR = @"WPA2_CCMP";
static NSString * const AUTH_WPA2_TKIP_STR = @"WPA2_TKIP";
static NSString * const AUTH_WPS_STR = @"WPS";

@interface EluxSSIDInfo : NSObject

@property(nonatomic, retain)NSString* name;
@property(nonatomic, retain)NSString* password;
@property(nonatomic, retain)NSString* authMethodString;
@property(nonatomic, assign)NSInteger authMethod;

//EluxSSIDInfo used only as a simple container, maybe not completely necessary to implement setters/getters
/*
-(void)setName:(NSString*)_name;
-(void)setPassword:(NSString*)_password;
-(void)setAuthMethod:(NSInteger)_authMethod;
-(void)setAuthMethodString:(NSString *)_authMethodString;

-(NSString*)getName;
-(NSString*)getPassword;
-(NSInteger)getAuthMethod;
-(NSString*)getAuthMethodString;
*/
@end
