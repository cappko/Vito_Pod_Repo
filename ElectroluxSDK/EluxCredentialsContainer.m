//
//  EluxCredentialsContainer.m
//  simple-app
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxCredentialsContainer.h"


@implementation EluxCredentialsContainer

@synthesize password;
@synthesize userName;
@synthesize x509;
@synthesize privateKey;
@synthesize logonEntry;
@synthesize expirationTime;



-(EluxCredentialsContainer*)initWithPassword:(NSString*)pass
{
    self = [super init];
    if(self)
        self.password   = pass;
    return self;
}


-(EluxCredentialsContainer*)initWithPassword:(NSString*)pass userName:(NSString*)name x509:(NSString*)cert privateKey:(NSString*)pKey logonEntry:(NSString*)lgEntry andExpirationTime:(NSDate*)expTime
{
    self = [super init];
    if(self)
    {
        self.password   = pass;
        self.userName   = name;
        self.x509       = cert;
        self.privateKey = pKey;
        self.logonEntry = lgEntry;
        self.expirationTime = expTime;
    }
    return self;
}


@end
