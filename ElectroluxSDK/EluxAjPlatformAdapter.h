//
//  EluxAjPlatformAdapter.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//


#import "EluxConnectivityPlatformAdapter.h"

@class EluxAjService;
@class EluxApplianceManager;
@class EluxAjApplianceContainer;
@class EluxRemoteProperty;
@class EluxSubUnit;
@protocol EluxApplianceDiscoveryDelegate;

@interface EluxAjPlatformAdapter : EluxConnectivityPlatformAdapter 
{
    NSString* TAG;
    EluxAjService* mAjService;
    EluxAjApplianceContainer* mAjDeviceContainer;
    EluxApplianceManager* mApplianceManager;
    id<EluxApplianceDiscoveryDelegate> mSDKApplianceDicoveryListener;
    BOOL mAjServiceBound;
}

-(EluxAjPlatformAdapter*)initWithApplianceManager:(EluxApplianceManager*)applianceManager;



@end
