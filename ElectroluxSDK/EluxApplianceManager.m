//
//  EluxApplianceManager.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxApplianceManager.h"

#import "EluxPlatformApplianceProtocol.h"
#import "EluxPlatformsManager.h"
#import "EluxAppliance.h"
#import "EluxConnectivityPlatformType.h"
#import "EluxConnectivityPlatformAdapter.h"
#import "EluxApplianceDiscoveryDelegate.h"
#import "EluxErrors.h"
#import "EluxErrorCodes.h"

@implementation EluxApplianceManager

-(EluxApplianceManager*)initWithPlatformsManager:(EluxPlatformsManager*)pm
{
    self = [super init];
    mPlatformsManager = pm;
    TAG = NSStringFromClass([EluxApplianceManager class]);
    mCachedAppliances = [[NSMutableDictionary alloc]init];
    return self;
}

-(EluxApplianceManager*)init
{
    NSLog(@"Unsupported initialization");
    return nil;
}

+(EluxApplianceManager*)sharedInstanceWithPlatformManager:(EluxPlatformsManager*)pm
{
    static EluxApplianceManager* sharedInstance = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedInstance = [[EluxApplianceManager alloc] initWithPlatformsManager:pm];
    });
    return sharedInstance;
}

-(void)setPlatformsManager:(EluxPlatformsManager*)pm
{
    mPlatformsManager = pm;
}

-(EluxPlatformsManager*)getPlatformsManager
{
    return mPlatformsManager;
}

/**
 * Returns a NSDictionary in form: <NSString uniqueId, EluxAppliance appliance>
 * @return Returns a NSDictionary in form: <NSString uniqueId, EluxAppliance appliance>
 */
-(NSDictionary*)getCachedAppliances;
{
    return mCachedAppliances;
}

/**
 * Add an appliance to the collection of appliances managed by EluxApplianceManager
 * @param appliance an appliance to be added
 */
-(void)addApplianceToCachedAppliances:(EluxAppliance*)appliance
{
    if(!appliance)
        return;
    if(!mCachedAppliances)
        mCachedAppliances = [[NSMutableDictionary alloc]init];
    if(![appliance getUniqueIdString])
        [appliance setUniqueIdString:[[NSString alloc]initWithString: [[appliance getApplianceInfo]getApplianceId]]];
    if([mCachedAppliances objectForKey:[appliance getUniqueIdString]])//appliance allready cached
        return;
    [appliance setApplianceManager:self];
    [mCachedAppliances setObject:appliance forKey:[appliance getUniqueIdString]];
}

/**
 * Disconnect the platforms, notifies the attached discovery listeners and clear appliance cache
 */
-(void)forceClearAppliancesCache
{
    if(!mCachedAppliances)
        return;
    if(!mCachedAppliances.count)
        return;
    NSMutableArray* appliances = [NSMutableArray arrayWithArray:[mCachedAppliances allValues]];
    if(!appliances)
        return;
    for(EluxAppliance* appliance in appliances)
    {
        NSArray* arr = [mPlatformsManager getDiscoveryListeners];
        if(!arr)
            continue;
        [appliances removeObject:appliance];
        for(id<EluxApplianceDiscoveryDelegate> listener in arr)
            [listener onApplianceLost:appliance stillVisibleAppliances:appliances];
    }
    [mCachedAppliances removeAllObjects];
}

/**
 * Get appliance by its SDK wide unique ID
 * @param applianceId id of the device
 * @return Appliance or nil if not found
 */
-(EluxAppliance*)getApplianceById:(NSInteger)applianceId
{
    return [self getApplianceByStringId:[self uniqueIdIntegerToString:applianceId]];
}

/**
 * Get appliance by its SDK wide unique ID
 * @param applianceId id of the device
 * @return Appliance or nil if not found
 */
-(EluxAppliance*)getApplianceByStringId:(NSString *)applianceId
{
    if(!mCachedAppliances)
        return nil;
    if(!mCachedAppliances.count)
        return nil;
    if(!applianceId)
        return nil;
    if(!applianceId.length)
        return nil;
    return [mCachedAppliances objectForKey:applianceId];
}

/**
 * Create fresh instance of appliance.
 * @param platform Platform the device was created for
 * @param appInfo Appliance info object
 * @return new instance of Appliance
 */
-(EluxAppliance*)createAppliance:(EluxConnectivityPlatformType*)platform withApplianceInfo:(EluxApplianceInfo*)appInfo
{
    return [EluxApplianceManager createAppliance:platform withApplianceInfo:appInfo];
}

/**
 * Create fresh instance of appliance.
 * @param platform Platform the device was created for
 * @param appInfo Appliance info object
 * @return new instance of Appliance
 */
+(EluxAppliance*)createAppliance:(EluxConnectivityPlatformType*)platform withApplianceInfo:(EluxApplianceInfo*)appInfo
{
    if(!platform || !appInfo)
        return nil;
    EluxAppliance* appliance = [[EluxAppliance alloc]init];
    [appliance setApplianceInfo:appInfo];
    [appliance setConnectivityPlatform:platform];
    return appliance;
}

/**
 * Gets remote Platform appliance for SDK appliance.
 * @param appliance Appliance
 * @return Plaform appliance or nil if not found.
 */
-(id<EluxPlatformApplianceProtocol>)getPlatformAppliance:(EluxAppliance*)appliance
{
    if(!appliance)
        return nil;
    NSArray* platformAdapters = [mPlatformsManager getPlatformAdapters];
    if(!platformAdapters)
        return nil;
    
    NSInteger index = [self getPlatformIndex:[appliance getConnectivityPlatform]];
    EluxConnectivityPlatformAdapter* adapter = [platformAdapters objectAtIndex: index];
    return [adapter getPlatformAppliance:appliance];
}

/**
 * Get index in array of platfrom adapters.
 * @param platform Platform
 * @return index in the array of adapters.
 */
-(NSInteger)getPlatformIndex:(EluxConnectivityPlatformType*) platform
{
    return [mPlatformsManager getPlatformIndex:platform];
}

/**
 * Converts appliance unique id represented by NSString to NSInteger
 * @param idString unique id represented by NSString
 * @return unique id represented by NSInteger
 */
-(NSInteger)uniqueIdStringToInteger:(NSString*)idString
{
    return [idString integerValue];
}

/**
 * Converts appliance unique id represented by NSInteger to NSString
 * @param idInteger unique id represented by NSInteger
 * @return unique id represented by NSString
 */
-(NSString*)uniqueIdIntegerToString:(NSInteger)idInteger
{
    return [NSString stringWithFormat:@"%ld", (long)idInteger];
}

/**
 * Try to search for all visible/available appliances
 *
 * @param listener EluxApplianceDiscoveryDelegate
 */
-(void)searchForAvailableAppliances:(id<EluxApplianceDiscoveryDelegate>)listener
{
    if(!listener)
        return;
    NSArray* platformAdapters = [mPlatformsManager getPlatformAdapters];
    if(!platformAdapters)
    {
        [listener onDiscoveryFailed:[EluxErrors createSimpleErrorWithCode:ERR_BAD_INITIALIZATION andRerrorMEssage:ERR_BAD_INITIALIZATION_STRING]];
        return;
    }
    if(!platformAdapters.count)
    {
        [listener onDiscoveryFailed:[EluxErrors createSimpleErrorWithCode:ERR_BAD_INITIALIZATION andRerrorMEssage:ERR_BAD_INITIALIZATION_STRING]];
        return;
    }
    for(EluxConnectivityPlatformAdapter* adapter in platformAdapters)
        [adapter searchForAvailableAppliances:listener];
}

/**
 * By default all remote properties from an appliance are listed.
 * This method specifies which properties we want to omit.
 * @param filteringMask A bit mask that specifies what properties to omit.
 */
-(void)setShowPorpertyListFilteringMask:(NSInteger)filteringMask
{
    mPropertyFilteringMask = filteringMask;
}

/**
 * By default all remote properties from an appliance are listed.
 * This method allows you to get the bit mask specifying which properties are omited.
 * @return The bit mask specifying which properties are omited.
 */
-(NSInteger)getShowPorpertyListFilteringMask
{
    return mPropertyFilteringMask;
}



@end
