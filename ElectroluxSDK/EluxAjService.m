//
//  EluxAjService.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <UIKit/UIKit.h>

#import "EluxAjService.h"
#import "EluxErrors.h"
#import "EluxErrorCodes.h"
#import "EluxUserInteractionCodes.h"
#import "EluxSSIDInfo.h"
#import "EluxOnboardingDelegate.h"
#import "EluxGenericWifiScan.h"
#import "EluxOffboardingDelegate.h"
#import "EluxAppliance.h"
#import "EluxApplianceManager.h"
#import "EluxConnectivityPlatformType.h"
#import "EluxAjDataConverter.h"
#import "EluxRemoteValueContainer.h"
#import "EluxAjPropertyChangeListener.h"
#import "EluxPlatformsManager.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import <AJNInit.h>
#import <AJNStatus.h>
#import <AJNBus.h>
#import <AJNBusAttachment.h>
#import <AJNProxyBusObject.h>
#import <AJNAboutProxy.h>
#import <AJNAboutObjectDescription.h>
#import <alljoyn/samples_common/AJSCAboutAnnouncement.h>
#import <alljoyn/samples_common/AJSCClientInformation.h>
#import <alljoyn/samples_common/AJSCAboutAnnouncement.h>
#import <alljoyn/samples_common/AJSCAboutDataConverter.h>
#import <alljoyn/onboarding/AJOBScanInfo.h>
#import <AJNObserver.h>

static NSString *const ONBOARDING_INTERFACE_NAME = @"org.alljoyn.Onboarding";
static NSString *const DEFAULT_REALM_BUS_NAME = @"org.alljoyn.BusNode.onboardingClient";
static NSString *const DAEMON_QUIET_PREFIX = @"quiet@";
static NSString *const DEFAULT_AUTH_PASSCODE = @"000000";
static NSString *const ONBOARDING_OBJECT_PATH = @"/Onboarding";

static NSString *const ELUX_REALM_BUS_NAME = @"com.electrolux";

#define SUPPORTED_INTERFACES @"com.electrolux.*", @"org.alljoyn.*"

#define AJ_MAX_WIFI_SCAN_ROUNDS         2

#define AJ_TASK_ID_ALL_DONE             0
#define AJ_TASK_ID_WIFI_SCAN            1
#define AJ_TASK_ID_ONBBOARD             2
#define AJ_TASK_ID_OFFBOARD             3
#define AJ_TASK_ID_SEARCH_APPLIANCES    4

@implementation EluxAjService

-(EluxAjService*)init
{
    self = [super init];
    if ([AJNInit alljoynInit] != ER_OK)
        return nil;
    
    if ([AJNInit alljoynRouterInit] != ER_OK)
    {
        [AJNInit alljoynShutdown];
        return nil;
    }
    
    [self loadNewSession];
    return self;
}

-(NSString*)getAppNAme
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
}

#pragma mark start AboutClient
-(BOOL)startAboutClient
{
    QStatus status;
    
    // Create a dictionary to contain announcements using a key in the format of: "announcementUniqueName + announcementObj"
    mClientInformationDict = [[NSMutableDictionary alloc] init];
    mDeviceSessionDict = [[NSMutableDictionary alloc] init];
    
    mCurrentSSID = [EluxGenericWifiScan getCurrentSSID];
    
    // Init AJNBusAttachment
    mClientBusAttachment = [[AJNBusAttachment alloc] initWithApplicationName:[self getAppNAme] allowRemoteMessages:YES];
    
    // Start AJNBusAttachment
    status = [mClientBusAttachment start];
    if (status != ER_OK)
    {
        NSLog(@"Failed AJNBusAttachment start with status: %d", status);
        [self stopAboutClient];
        return NO;
    }
    
    // Connect AJNBusAttachment
    status = [mClientBusAttachment connectWithArguments:@""];
    if (status != ER_OK)
    {
        NSLog(@"Failed AJNBusAttachment connectWithArguments with status: %d", status);
        [self stopAboutClient];
        return NO;
    }
    
    [mClientBusAttachment registerBusListener:self];
    [mClientBusAttachment registerAboutListener:self];
    
    if(mTaskId == AJ_TASK_ID_ONBBOARD)
        status = [mClientBusAttachment whoImplementsInterface:ONBOARDING_INTERFACE_NAME];
    else
        status = [mClientBusAttachment whoImplementsInterfaces:[NSArray arrayWithObjects: SUPPORTED_INTERFACES, nil]];
    if (status != ER_OK)
    {
        NSLog(@"Failed to registerAnnouncementReceiver with status: %d", status);
        [self stopAboutClient];
        return NO;
    }
    
    NSUUID *UUID = [NSUUID UUID];
    NSString *stringUUID = [UUID UUIDString];
    
    if(mTaskId == AJ_TASK_ID_ONBBOARD)
        mRealmBusName = [NSString stringWithFormat:@"%@-%@", DEFAULT_REALM_BUS_NAME, stringUUID];
    else
        mRealmBusName = [NSString stringWithFormat:@"%@-%@", ELUX_REALM_BUS_NAME, stringUUID];
    
    // Advertise Daemon for tcl
    status = [mClientBusAttachment requestWellKnownName:mRealmBusName withFlags:kAJNBusNameFlagDoNotQueue];
    if (status == ER_OK)
    {
        // Advertise the name with a quite prefix for TC to find it
        status = [mClientBusAttachment advertiseName:[NSString stringWithFormat:@"%@%@", DAEMON_QUIET_PREFIX, mRealmBusName]
                                   withTransportMask:kAJNTransportMaskAny];
        if (status != ER_OK)
        {
            NSLog(@"Failed to advertise name with status: %d", status);
            NSLog(@"[%@] [%@] Failed advertising: %@%@", @"ERROR", [[self class] description], DAEMON_QUIET_PREFIX, mRealmBusName);
            [self stopAboutClient];
            return NO;
        }
        else
            NSLog(@"[%@] [%@] Successfully advertised: %@%@", @"DEBUG", [[self class] description], DAEMON_QUIET_PREFIX, mRealmBusName);
    }
    else
    {
        NSLog(@"Failed to requestWellKnownName with status: %d", status);
        [self stopAboutClient];
        return NO;
    }
    
    // Enable Client Security
    mAuthenticationListenerImpl = [[EluxAjAuthenticationListenerImpl alloc] init];
    mAuthenticationListenerImpl.defaultPasscode = DEFAULT_AUTH_PASSCODE;
    status = [self enableClientSecurity];
    if (ER_OK != status)
        NSLog(@"Failed to enable security with status: %d", status);
    else
        NSLog(@"Successfully enabled security for the bus.");
    
    // For devices: Start the onboarding successful validation timeout after manual reconnecting to AllJoyn (following
    // the onboarding procedure).
    // For simulator: This code won't be run because network changes cannot be detected, so there is no way to reliably
    // begin this timeout only when the onboarder has manually switched to the target network.
    if (mIsWaitingForOnboardee && [mCurrentSSID isEqualToString:mTargetSSID])
        [self startOnboardSuccessfulTimeout];
    
    mIsAboutClientConnected = YES;
    return YES;
}

- (QStatus)enableClientSecurity
{
    QStatus status;
    status = [mClientBusAttachment enablePeerSecurity:@"ALLJOYN_SRP_KEYX ALLJOYN_ECDHE_PSK"
                               authenticationListener:mAuthenticationListenerImpl
                                     keystoreFileName:@"Documents/alljoyn_keystore/s_central.ks"
                                              sharing:NO];
    
    if (status != ER_OK)//try to delete the keystore and recreate it, if that fails return failure
    {
        NSError *error;
        NSString *keystoreFilePath = [NSString stringWithFormat:@"%@/alljoyn_keystore/s_central.ks", [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]];
        [[NSFileManager defaultManager] removeItemAtPath:keystoreFilePath error:&error];
        if (error)
        {
            NSLog(@"ERROR: Unable to delete keystore. %@", error);
            return ER_AUTH_FAIL;
        }
        
        status = [mClientBusAttachment enablePeerSecurity:@"ALLJOYN_SRP_KEYX ALLJOYN_ECDHE_PSK"
                                   authenticationListener:mAuthenticationListenerImpl
                                         keystoreFileName:@"Documents/alljoyn_keystore/s_central.ks"
                                                  sharing:NO];
    }
    return status;
}

- (void)addNewAnnouncementEntry:(AJSCClientInformation*) clientInfo
{
    if(mTaskId == AJ_TASK_ID_SEARCH_APPLIANCES)
        dispatch_async(mFoundAppliancesQueue, ^{[self processFoundAppliance: [clientInfo.announcement busName]];});
}

- (void)announcementGetMoreInfo:(NSInteger)index
{
    if(index < 0)
        index = 0;
    NSString* interface = [mClientInformationDict allKeys][index];
    NSLog(@"[%@] [%@] Requested:  [%@]", @"DEBUG", [[self class] description], interface);
}

- (bool)announcementSupportsOnboardingInterface:(NSString *)announcementKey
{
    AJSCAboutAnnouncement *announcement = [(AJSCClientInformation *)[mClientInformationDict valueForKey:announcementKey] announcement];
    AJNAboutObjectDescription *aboutObjectDescription = [[AJNAboutObjectDescription alloc] initWithMsgArg: announcement.objectDescriptionArg];
    return [aboutObjectDescription hasInterface:ONBOARDING_INTERFACE_NAME withPath:ONBOARDING_OBJECT_PATH];
}

#pragma mark stop AboutClient
- (void)stopAboutClient
{
    QStatus status;
    NSLog(@"[%@] [%@] Stop About Client", @"DEBUG", [[self class] description]);
    
    // Bus attachment cleanup
    status = [mClientBusAttachment cancelAdvertisedName:[NSString stringWithFormat:@"%@%@", DAEMON_QUIET_PREFIX, mRealmBusName]
                                      withTransportMask:kAJNTransportMaskAny];
    if (status == ER_OK)
        NSLog(@"[%@] [%@] Successfully cancel advertised name", @"DEBUG", [[self class] description]);
    else
        NSLog(@"[%@] [%@]  Failed cancel advertised name, error:%@", @"DEBUG", [[self class] description], [AJNStatus descriptionForStatusCode:status]);
    
    
    status = [mClientBusAttachment releaseWellKnownName:mRealmBusName];
    if (status == ER_OK)
        NSLog(@"[%@] [%@] Successfully release WellKnownName", @"DEBUG", [[self class] description]);
    else
        NSLog(@"[%@] [%@]  Failed release WellKnownName, error:%@", @"DEBUG", [[self class] description], [AJNStatus descriptionForStatusCode:status]);
    
    
    status = [mClientBusAttachment removeMatchRule:@"sessionless='t',type='error'"];
    if (status == ER_OK)
        NSLog(@"[%@] [%@] Successfully remove MatchRule", @"DEBUG", [[self class] description]);
    else
        NSLog(@"[%@] [%@]  Failed remove MatchRule, error:%@", @"DEBUG", [[self class] description], [AJNStatus descriptionForStatusCode:status]);
    
    // Cancel advertise name for each announcement bus
    for (NSString *key in [mClientInformationDict allKeys])
    {
        AJSCClientInformation *clientInfo = (mClientInformationDict)[key];
        status = [mClientBusAttachment cancelFindAdvertisedName:[[clientInfo announcement] busName]];
        if (status != ER_OK)
            NSLog(@"[%@] [%@] failed to cancelAdvertisedName for %@. status:%@", @"ERROR", [[self class] description], key, [AJNStatus descriptionForStatusCode:status]);
    }
    
    mClientInformationDict = nil;
    
    // Leave all sessions
    for(NSString *key in mDeviceSessionDict)
    {
        AJNSessionId sessionId = [mDeviceSessionDict[key] unsignedIntValue];
        status = [mClientBusAttachment leaveSession:sessionId];
        if (ER_OK != status)
            NSLog(@"Failed to leave session %u, %@", sessionId, [AJNStatus descriptionForStatusCode:status]);
    }
    
    mDeviceSessionDict = nil;
    [mClientBusAttachment unregisterAboutListener:self];
    
    // Stop bus attachment
    status = [mClientBusAttachment stop];
    if (status == ER_OK)
        NSLog(@"[%@] [%@] Successfully stopped bus", @"DEBUG", [[self class] description]);
    else
        NSLog(@"[%@] [%@]  Failed stopping bus, error:%@", @"DEBUG", [[self class] description], [AJNStatus descriptionForStatusCode:status]);
    
    mClientBusAttachment = nil;
    mIsAboutClientConnected = false;
    
    NSLog(@"[%@] [%@] About Client has stopped", @"DEBUG", [[self class] description]);
}


#pragma mark - Onboarding event methods

- (void)onOnboardingStarted:(NSString *)ssid
{
    // Start checking for announcement from onboardee to say that they have onboarded successfully.
    mIsWaitingForOnboardee = true;
    mTargetSSID = [NSString stringWithString:ssid];
    [mEluxOnboardingDelegate userInteractionNeeded:UI_ONBOARDING_CHANGE_WIFI andInterActionString:UI_ONBOARDING_CHANGE_WIFI_STRING];
}

- (void)checkIfOnboardingFailed
{
    // Assume the onboardee failed to onboard to the target network if an announcement wasn't received.
    if (mIsWaitingForOnboardee)
    {
        NSLog(@"Confirmation announcement not received from onboardee.");
        mIsWaitingForOnboardee = false;
        [mEluxOnboardingDelegate onApplianceOnboardingFailed:[EluxErrors createSimpleErrorWithCode:ERR_ONBOARDING_CONFIGURATION_FAILED andRerrorMEssage: ERR_ONBOARDING_CONFIGURATION_FAILED_STRING]];
    }
}

-(QStatus)initInternals
{
    NSArray* keys = [mClientInformationDict allKeys];
    if(!keys)
        return ER_FAIL;
    if(keys.count < 1)
        return ER_FAIL;
    mClientInformation = [mClientInformationDict objectForKey:[keys firstObject]];
    mOnboardeeBus = [mClientInformation.announcement busName];
    if (!mOnboardeeBus)
        return ER_FAIL;
    return ER_OK;
}

-(void)wifiScanFinalizePartTwo
{
    QStatus status = [self scan];
    
    if((status != ER_OK) ||! mOnboardeeNetworks)
        [mEluxApplianceWifiScanDelegate listOfAvailableSSIDsFailedWithError:[EluxErrors createSimpleErrorWithCode:ERR_AJ_WIFI_SCAN_FAILED andRerrorMEssage:[NSString stringWithFormat:@"%@ with AllJoyn code: %d", ERR_AJ_WIFI_SCAN_FAILED_STRING, status]]];
    else
    {
        NSMutableArray* arr = [[NSMutableArray alloc]initWithCapacity:mOnboardeeNetworks.count];
        for(int i = 0; i < mOnboardeeNetworks.count; i++)
        {
            EluxSSIDInfo* ssidInfo = [[EluxSSIDInfo alloc]init];
            AJOBScanInfo *scanInfo = mOnboardeeNetworks[i];
            ssidInfo.name = scanInfo.ssid;
            ssidInfo.authMethod = scanInfo.authType;
            ssidInfo.authMethodString = [self authIntToString:scanInfo.authType];
            [arr addObject:ssidInfo];
        }
        [mEluxApplianceWifiScanDelegate listOfAvailableSSIDsReceived:arr];
    }
    //    [self stopAboutClient];
}


-(void)wifiScanFinalize
{
    if([self initInternals] != ER_OK)
    {
        [mEluxApplianceWifiScanDelegate listOfAvailableSSIDsFailedWithError:[EluxErrors createSimpleErrorWithCode:ERR_AJ_WIFI_SCAN_FAILED andRerrorMEssage:ERR_AJ_WIFI_SCAN_FAILED_STRING]];
        return;
    }
    
    if([self startOnboardingClient] != ER_OK)
    {
        [mEluxApplianceWifiScanDelegate listOfAvailableSSIDsFailedWithError:[EluxErrors createSimpleErrorWithCode:ERR_AJ_WIFI_SCAN_FAILED andRerrorMEssage:ERR_AJ_WIFI_SCAN_FAILED_STRING]];
        return;
    }
    [self performSelector:@selector(wifiScanFinalizePartTwo) withObject:nil afterDelay:2.0];
}

-(NSString *)authIntToString:(AJOBAuthType)authType
{
    switch(authType) {
        case OPEN:
            return AUTH_OPEN_STR;
            break;
        case WEP:
            return AUTH_WEP_STR;
            break;
        case WPA_AUTO:
            return AUTH_WPA_AUTO_STR;
            break;
        case WPA_CCMP:
            return AUTH_WPA_CCMP_STR;
            break;
        case WPA_TKIP:
            return AUTH_WPA_TKIP_STR;
            break;
        case WPA2_AUTO:
            return AUTH_WPA2_AUTO_STR;
            break;
        case WPA2_CCMP:
            return AUTH_WPA2_CCMP_STR;
            break;
        case WPA2_TKIP:
            return AUTH_WPA2_TKIP_STR;
            break;
        case WPS:
            return AUTH_WPS_STR;
            break;
        default:
            return @"Unknown";
            break;
    }
}

-(void)wifiScan:(id<EluxApplianceWifiScanDelegate>)listener
{
    if(!listener)
        return;
    mEluxApplianceWifiScanDelegate = listener;
    mTaskId = AJ_TASK_ID_WIFI_SCAN;
    [self startAboutClient];
}

-(void)wifiScan:(EluxAppliance*)appliance withWifiScanListener:(id<EluxApplianceWifiScanDelegate>)listener
{
    NSLog(@"Appliance ID ommited");
    [self wifiScan:listener];
}

-(void)onboard:(EluxAppliance*)appliance toSSID:(EluxSSIDInfo*)ssid withListener:(id<EluxOnboardingDelegate>)listener
{
    if(!listener)
        return;
    mEluxOnboardingDelegate = listener;
    mTaskId = AJ_TASK_ID_ONBBOARD;
    NSError* err = [self configure:ssid];
    if(err)
    {
        [mEluxOnboardingDelegate onApplianceOnboardingFailed:err];
        return;
    }
    [self performSelector:@selector(onboardFinalizeWithSSID:) withObject:ssid afterDelay:2.0];
}

-(void)onboardFinalizeWithSSID:(EluxSSIDInfo*)ssid
{
    NSError* err;
    err = [self connect];
    if(err)
    {
        [mEluxOnboardingDelegate onApplianceOnboardingFailed:err];
        return;
    }
    [self onOnboardingStarted:ssid.name];
}

-(NSError*)configure:(EluxSSIDInfo*)ssid
{
    QStatus status;
    
    AJOBInfo obInfo;
    obInfo.SSID = ssid.name;
    obInfo.passcode = ssid.password;
    obInfo.authType = (AJOBAuthType)ssid.authMethod;
    
    NSLog(@"input SSID:%@ passcode:%@", obInfo.SSID, obInfo.passcode);
    
    if (![obInfo.SSID length])
        return [EluxErrors createSimpleErrorWithCode:ERR_ONBOARDING_EMPTY_SSID andRerrorMEssage:ERR_ONBOARDING_EMPTY_SSID_STRING];
    
    BOOL shouldConvert = NO;
    if ([AJOBSOnboarding isValidWPAKey:obInfo.passcode])
        shouldConvert = true;
    else if ([AJOBSOnboarding isValidWEPKey:obInfo.passcode])
    {
        if ([obInfo.passcode length] % 2)
            shouldConvert = true;
    }
    else
        NSLog(@"Input passcode is none of the following: WPA, WEP");
    
    if (shouldConvert)
    {
        NSString *tPasscode = [AJOBSOnboarding passcodeToHex:obInfo.passcode];
        obInfo.passcode = tPasscode;
        NSLog(@"Passcode has been converted to:%@", obInfo.passcode);
    }
    
    short resultStatus;
    
    AJNSessionId sessionId = 0;
    NSNumber* sess = [mDeviceSessionDict objectForKey:mOnboardeeBus];
    if(sess)
        sessionId = [sess unsignedIntValue];
    
    status = [mOnboardingClient configureWiFi:mOnboardeeBus obInfo:obInfo resultStatus:resultStatus sessionId:sessionId];//mSessionId];
    if (status != ER_OK)
    {
        NSString *errorMessage = [NSString stringWithFormat:@"Call to configureWiFi failed: %@ ", [AJNStatus descriptionForStatusCode:status]];
        NSLog(@"%@", errorMessage);
        return  [EluxErrors createSimpleErrorWithCode:ERR_ONBOARDING_CONFIGURATION_FAILED andRerrorMEssage:ERR_ONBOARDING_CONFIGURATION_FAILED_STRING];
    }
    return nil;
}

-(NSError*)connect
{
    AJNSessionId sessionId = 0;
    NSNumber* sess = [mDeviceSessionDict objectForKey:mOnboardeeBus];
    if(sess)
        sessionId = [sess unsignedIntValue];
    QStatus status = [mOnboardingClient connectTo:mOnboardeeBus sessionId:sessionId];//mSessionId];
    if (status != ER_OK)
        return  [EluxErrors createSimpleErrorWithCode:ERR_ONBOARDING_CONFIGURATION_FAILED andRerrorMEssage:ERR_ONBOARDING_CONFIGURATION_FAILED_STRING];
    //    [self startOnboardSuccessfulTimeout];
    return nil;
}

- (void)startOnboardSuccessfulTimeout
{
    // Begin a timeout to wait for confirmation announcement from onboardee that they have onboarded successfully.
    [NSTimer scheduledTimerWithTimeInterval:30.0
                                     target:self
                                   selector:@selector(checkIfOnboardingFailed)
                                   userInfo:nil
                                    repeats:NO];
}

-(void)offboard:(EluxAppliance*)appliance withListener:(id<EluxOffboardingDelegate>)listener
{
    //startAboutClient -> didReceiveAnnounceOnBus -> didFindAdvertisedName -> startOnboardingClient ->
    if(!listener)
        return;
    mEluxOffboardingDelegate = listener;
    mTaskId = AJ_TASK_ID_OFFBOARD;
    [self startAboutClient];
}

-(void)offboardFinalize
{
    if([self startOnboardingClient] != ER_OK)
    {
        [mEluxOffboardingDelegate onApplianceOffboardingFailed:[EluxErrors createSimpleErrorWithCode:ERR_OFFBOARDING_COULD_NOT_START_ONBOAR_CLIENT andRerrorMEssage:ERR_OFFBOARDING_COULD_NOT_START_ONBOAR_CLIENT_STRING]];
        return;
    }
    [self performSelector:@selector(offboardFinalizePartTwo) withObject:nil afterDelay:1.0];
}

-(void)offboardFinalizePartTwo
{
    AJNSessionId sessionId = 0;
    NSNumber* sess = [mDeviceSessionDict objectForKey:mOnboardeeBus];
    if(sess)
        sessionId = [sess unsignedIntValue];
    
    QStatus status = [mOnboardingClient offboardFrom:mOnboardeeBus sessionId:sessionId];//mSessionId];
    if (status == ER_OK)
        [mEluxOffboardingDelegate onApplianceOffboardingSucceeded:nil];
    else
        [mEluxOffboardingDelegate onApplianceOffboardingFailed:[EluxErrors createSimpleErrorWithCode:ERR_OFFBOARDING_COULD_NOT_START_ONBOAR_CLIENT andRerrorMEssage: [NSString stringWithFormat:@"%@, AllJoyn error: %d", ERR_OFFBOARDING_COULD_NOT_START_ONBOAR_CLIENT_STRING, status]]];
}


- (QStatus)startOnboardingClient
{
    if(!mClientBusAttachment)
        return ER_FAIL;
    
    mOnboardingClient = [[AJOBSOnboardingClient alloc] initWithBus:mClientBusAttachment listener:self];
    if(!mOnboardingClient)
        return ER_FAIL;
    
    return ER_OK;
}

- (bool)isOnSAPNetwork
{
    NSString *currentSSID;
    NSArray *supportedInterfaces = (__bridge_transfer id)CNCopySupportedInterfaces();
    id interfaceInformation = nil;
    for (NSString *interfaceName in supportedInterfaces)
    {
        interfaceInformation = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)interfaceName);
        NSDictionary *dict = interfaceInformation;
        currentSSID = dict[@"SSID"];
        NSLog(@"Current SSID: %@", currentSSID);
    }
    return [currentSSID hasPrefix:AJ_AP_PREFIX] | [currentSSID hasSuffix:AJ_AP_SUFFIX];
}


-(QStatus)scan
{
    if(!mOnboardeeNetworks)
        mOnboardeeNetworks = [[NSMutableArray alloc] init];
    [mOnboardeeNetworks removeAllObjects];
    
    unsigned short age = 0;
    NSInteger rounds = 0;
    QStatus status = ER_FAIL;
    
    NSArray* arr = [mDeviceSessionDict allKeys];
    if(!arr)
        return ER_FAIL;
    if(arr.count < 1)
        return ER_FAIL;
    
//    mSessionId = [[mDeviceSessionDict objectForKey:[arr firstObject]] unsignedIntValue];

    AJNSessionId sessionId = 0;
    NSNumber* sess = [mDeviceSessionDict objectForKey:mOnboardeeBus];
    if(sess)
        sessionId = [sess unsignedIntValue];

    while(rounds < AJ_MAX_WIFI_SCAN_ROUNDS)
    {
        status = [mOnboardingClient getScanInfo:mOnboardeeBus
                                            age:age
                                       scanInfo:mOnboardeeNetworks
                                      sessionId:sessionId];//0];//mSessionId];
        if(status == ER_OK)
            break;
        
        sleep(5);
        rounds++;
    }
    return status;
}


-(BOOL)createAllJoynBus
{
    QStatus status;
    
    NSString *appName = [self getAppNAme];
    AJNBusAttachment* clientBusAttachment = [[AJNBusAttachment alloc] initWithApplicationName:appName allowRemoteMessages:YES];
    
    status = [clientBusAttachment start];
    if (status != ER_OK)
    {
        NSLog(@"AJNBusAttachment failed to start, status: %u", status);
        return NO;
    }
    
    status = [clientBusAttachment connectWithArguments:@""];
    if (status != ER_OK)
    {
        NSLog(@"AJNBusAttachment failed to connectWithArguments, status: %u", status);
        return NO;
    }
    
    [clientBusAttachment registerBusListener:self];
    [clientBusAttachment registerAboutListener:self];
    
    status = [clientBusAttachment whoImplementsInterface:@"org.alljoyn.Onboarding"];
    if (status != ER_OK)
    {
        NSLog(@"AJNBusAttachment failed to registerAnnouncementReceiver, status: %u", status);
        return NO;
    }
    
    NSUUID *UUID = [NSUUID UUID];
    NSString *stringUUID = [UUID UUIDString];
    
    NSString* realmBusName = [NSString stringWithFormat:@"%@-%@", @"org.alljoyn.BusNode.onboardingClient", stringUUID];
    
    status = [clientBusAttachment requestWellKnownName:realmBusName withFlags:kAJNBusNameFlagDoNotQueue];
    if (status == ER_OK)
    {
        status = [clientBusAttachment advertiseName:[NSString stringWithFormat:@"%@%@", @"quiet@", realmBusName] withTransportMask:kAJNTransportMaskAny];
        if (status != ER_OK)
        {
            NSLog(@"AJNBusAttachment failed to advertise name, status: %u", status);
            NSLog(@"[%@] [%@] Failed advertising: %@%@", @"ERROR", [[self class] description], @"quiet@", realmBusName);
            return NO;
        }
        else
            NSLog(@"[%@] [%@] Successfully advertised: %@%@", @"DEBUG", [[self class] description], @"quiet@", realmBusName);
    }
    else
    {
        NSLog(@"AJNBusAttachment failed to requestWellKnownName, status: %u", status);
        return NO;
    }
    return YES;
}

#pragma mark - AJNAboutListener protocol method
// Here we receive an announcement from AJN and add it to the client's list of services avaialble
- (void)didReceiveAnnounceOnBus:(NSString *)busName
                    withVersion:(uint16_t)version
                withSessionPort:(AJNSessionPort)port
          withObjectDescription:(AJNMessageArgument *)objectDescriptionArg
               withAboutDataArg:(AJNMessageArgument *)aboutDataArg
{
    NSLog(@"Announce from: %@", busName);
    QStatus status;
    
    NSString *announcementUniqueName; // Announcement unique name in a format of <busName DeviceName>
    AJSCClientInformation *clientInformation = [[AJSCClientInformation alloc] init];
    
    // Save the announcement in a AJSCAboutAnnouncement
    clientInformation.announcement = [[AJSCAboutAnnouncement alloc] initWithBusName:busName
                                                                            version:version
                                                                        sessionPort:port
                                                               objectDescriptionArg:objectDescriptionArg
                                                                       aboutDataArg:aboutDataArg];
    
    NSDictionary *aboutDataDict = [AJSCAboutDataConverter aboutDataArgToDict:[clientInformation.announcement aboutDataArg]];
    
    // Generate an announcement unique name in a format of <busName DeviceName>
    announcementUniqueName = busName;//[NSString stringWithFormat:@"%@ %@", busName, aboutDataDict[@"DeviceName"]];
    NSLog(@"[%@] [%@] Announcement unique name [%@]", @"DEBUG", [[self class] description], announcementUniqueName);
    
    mCurrentSSID = [EluxGenericWifiScan getCurrentSSID];
    
    NSString *appId = aboutDataDict[@"AppId"];
    if (!appId)
    {
        NSLog(@"[%@] [%@] Failed to read appId for key [%@]", @"DEBUG", [[self class] description], announcementUniqueName);
        return;
    }
    
    NSLog(@"appId: %@", appId);
    
    // Dealing with announcement entries should be syncronized, so we add it to a queue
    dispatch_sync(mCreationQueue, ^{
        bool isAppIdExists = false;
        QStatus tStatus;
        
        NSString *onboardeeAppId;
        // ASABASE-711 - Check whether onboarding succeeded here instead of in OnboardingViewController.
        // Devices disconnect from AllJoyn when onboarding disconnects from Wi-Fi hotspot, which in turn
        // unregisters any callbacks set in OnboardingViewController. Note that simulators can't check the
        // current network, so it's possible for a non-onboarded device to trigger this code.
        if (mIsWaitingForOnboardee && ([mCurrentSSID isEqualToString:mTargetSSID] || [self isSimulatorDevice]))
        {
            // Get the app ID of the onboardee.
            AJSCAboutAnnouncement *clientAnnouncement = [mClientInformation announcement];
            NSDictionary *onboardeeAboutDataDict = [AJSCAboutDataConverter aboutDataArgToDict:clientAnnouncement.aboutDataArg];
            
            onboardeeAppId = onboardeeAboutDataDict[@"AppId"];
            if (!onboardeeAppId)
                return;
            
            // Check if the announcement came from the onboardee.
            if([onboardeeAppId compare:appId] == NSOrderedSame)
            {
                mIsWaitingForOnboardee = false;
                EluxConnectivityPlatformType* platformType = [EluxConnectivityPlatformType allJoyn];
                EluxApplianceInfo* applianceInfo = [[EluxApplianceInfo alloc]initWithInfoDictionary:onboardeeAboutDataDict];
                EluxAppliance* appliance = [EluxApplianceManager createAppliance:platformType withApplianceInfo:applianceInfo];
                [appliance setPlatformSpecificBus:[[NSString alloc]initWithString:busName]];
                [appliance setPlatformSpecificPort:[[NSNumber alloc]initWithInt:port]];
                [mEluxOnboardingDelegate onApplianceOnboardingSucceeded:appliance];
            }
        }
        
        // Iterate over the announcements dictionary
        for (NSString *key in mClientInformationDict.allKeys)
        {
            AJSCClientInformation *clientInfo = [mClientInformationDict valueForKey:key];
            AJSCAboutAnnouncement *announcement = [clientInfo announcement];
            
            onboardeeAppId = [AJSCAboutDataConverter aboutDataArgToDict:announcement.aboutDataArg][@"AppId"];
            if (!onboardeeAppId)
            {
                NSLog(@"[%@] [%@] Failed to read appId for key [%@]", @"DEBUG", [[self class] description], key);
                return;
            }
            
            if([onboardeeAppId compare:appId] == NSOrderedSame)
            {
                isAppIdExists = true;
                // Same AppId and the same announcementUniqueName
                if ([key isEqualToString:announcementUniqueName])
                {
                    // Update only announcements dictionary
                    NSLog(@"[%@] [%@] Got an announcement from a known device - updating the announcement object", @"DEBUG", [[self class] description]);
                    
                    (mClientInformationDict)[announcementUniqueName] = clientInformation;
                    // Same AppId but *different* announcementUniqueName
                }
                else
                {
                    NSLog(@"[%@] [%@] Got an announcement from a known device(different bus name) - updating the announcement object and UI ", @"DEBUG", [[self class] description]);
                    
                    // Cancel advertise name if the bus name has changed
                    NSString *prevBusName = [announcement busName];
                    if (!([busName isEqualToString:prevBusName]))
                    {
                        tStatus = [mClientBusAttachment cancelFindAdvertisedName:prevBusName];
                        if (tStatus != ER_OK)
                            NSLog(@"[%@] [%@] failed to cancelAdvertisedName for %@. status:%@", @"DEBUG", [[self class] description], prevBusName, [AJNStatus descriptionForStatusCode:tStatus]);
                    }
                    // Remove existed record from the announcements dictionary
                    [mClientInformationDict removeObjectForKey:key];
                    // Add new record to the announcements dictionary
                    [mClientInformationDict setValue:clientInformation forKey:announcementUniqueName];
                    [self addNewAnnouncementEntry: clientInformation];
                }
            } //if
        } //for
        
        //appId doesn't exist and there is no match announcementUniqueName
        if (!(mClientInformationDict)[announcementUniqueName] && !isAppIdExists)
        {
            // Add new pair with this AboutService information (version,port,bus name, object description and about data)
            [mClientInformationDict setValue:clientInformation forKey:announcementUniqueName];
            [self addNewAnnouncementEntry: clientInformation];
            // AppId doesn't exist and BUT there is no match announcementUniqueName
        } // else No OP
    });
    
    // Register interest in a well-known name prefix for the purpose of discovery (didLoseAdertise)
    [mClientBusAttachment enableConcurrentCallbacks];
    status = [mClientBusAttachment findAdvertisedName:busName];
    if (status != ER_OK)
        NSLog(@"[%@] [%@] failed to findAdvertisedName for %@. status:%@", @"ERROR", [[self class] description], busName, [AJNStatus descriptionForStatusCode:status]);
    
    AJNSessionOptions *options = [[AJNSessionOptions alloc] initWithTrafficType:kAJNTrafficMessages
                                                             supportsMultipoint:YES
                                                                      proximity:kAJNProximityAny
                                                                  transportMask:kAJNTransportMaskAny];
    
    AJNSessionId sessionId = [mClientBusAttachment joinSessionWithName:busName onPort:port withDelegate:self options:options];
    [mDeviceSessionDict setValue:[NSNumber numberWithUnsignedInteger:sessionId] forKey:announcementUniqueName];
}


-(void)processFoundAppliance:(NSString*)name
{
    if(!name)
        return;
    NSLog(@"processFoundAppliance for busName: %@", name);
    
    AJSCClientInformation* client = [mClientInformationDict objectForKey:name];
    if(!client)
        return;
    AJSCAboutAnnouncement* announcement = [client announcement];
    if(announcement)
    {
        if(!mApplianceManager)
            mApplianceManager = [EluxApplianceManager sharedInstanceWithPlatformManager:[EluxPlatformsManager sharedInstance]];
        if(!mApplianceManager)
            return;
        NSInteger propertyFileringMask = [mApplianceManager getShowPorpertyListFilteringMask];
        uint16_t port = [client.announcement port];
        NSString* busName = [client.announcement busName];
        
        if([self initInternals] != ER_OK)
            return;
        NSDictionary* applianceDict = [AJSCAboutDataConverter aboutDataArgToDict:[announcement aboutDataArg]];
        if(!applianceDict)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ [mApplianceDiscoveryListener onDiscoveryFailed:[EluxErrors createSimpleErrorWithCode:ERR_AJ_WRONG_ABOUT_INFO andRerrorMEssage:ERR_AJ_WRONG_ABOUT_INFO_STRING]]; });
            return;
        }
        EluxConnectivityPlatformType* platformType = [EluxConnectivityPlatformType allJoyn];
        EluxApplianceInfo* applianceInfo = [[EluxApplianceInfo alloc]initWithInfoDictionary:applianceDict];
        if(!applianceInfo)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ [mApplianceDiscoveryListener onDiscoveryFailed:[EluxErrors createSimpleErrorWithCode:ERR_AJ_WRONG_APPLIANCE_INFO andRerrorMEssage:ERR_AJ_WRONG_APPLIANCE_INFO_STRING]]; });
            return;
        }
        EluxAppliance* appliance = [EluxApplianceManager createAppliance:platformType withApplianceInfo:applianceInfo];
        if(!appliance)
        {
            [mApplianceDiscoveryListener onDiscoveryFailed:[EluxErrors createSimpleErrorWithCode:ERR_AJ_WRONG_APPLIANCE andRerrorMEssage:ERR_AJ_WRONG_APPLIANCE_STRING]];
            return;
        }
        
        [appliance setPlatformSpecificBus:[[NSString alloc]initWithString:busName]];
        [appliance setPlatformSpecificPort:[[NSNumber alloc]initWithInt:port]];
        
        AJNSessionId sessionId = 0;
        NSNumber* sess = [mDeviceSessionDict objectForKey:busName];
        if(sess)
            sessionId = [sess unsignedIntValue];
        
        if (sessionId == 0 || sessionId == -1)
        {
            AJNSessionOptions *opt = [[AJNSessionOptions alloc] initWithTrafficType:kAJNTrafficMessages
                                                                 supportsMultipoint:false
                                                                          proximity:kAJNProximityAny
                                                                      transportMask:kAJNTransportMaskAny];
            
            sessionId = [mClientBusAttachment joinSessionWithName:busName onPort:port withDelegate:self options:opt];
        }
        
        if (sessionId == 0 || sessionId == -1)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ [mApplianceDiscoveryListener onDiscoveryFailed:[EluxErrors createSimpleErrorWithCode:ERR_AJ_COULD_NOT_JOIN_SESSION andRerrorMEssage:ERR_AJ_COULD_NOT_JOIN_SESSION_STRING]]; });
            return;
        }
        
        NSString* log = @"\n";
        
        NSDictionary* dict = [EluxAjDataConverter objectDescriptionArgToDict:[announcement objectDescriptionArg]];
        NSMutableArray* subUnits = [[NSMutableArray alloc]initWithCapacity:dict.count];
        for(NSString* objectPath in dict.allKeys)
        {
            //We would like to introspect only objects in Electrolux spacename, not e.g. the "/Onboarding"
            if(![objectPath hasPrefix:@"/HaclV4/"])
                continue;
            AJNProxyBusObject* proxy = [[AJNProxyBusObject alloc] initWithBusAttachment:mClientBusAttachment
                                                                            serviceName:busName
                                                                             objectPath:objectPath
                                                                              sessionId:sessionId
                                                                         enableSecurity:YES];
            QStatus qStatus = [proxy introspectRemoteObject];
            if (qStatus != ER_OK)
            {
                NSLog(@"could not introspect: %@ of: %@", objectPath, busName);
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [mApplianceDiscoveryListener onDiscoveryFailed:[EluxErrors createSimpleErrorWithCode:ERR_AJ_COULD_NOT_INTROSPECT_OBJECT andRerrorMEssage:[NSString stringWithFormat:@"%@: %@", ERR_AJ_COULD_NOT_INTROSPECT_OBJECT_STRING, objectPath]]]; });
                continue;
            }
            
            NSMutableArray* remoteProperties = [[NSMutableArray alloc]init];
            NSMutableArray* remoteMethods = [[NSMutableArray alloc]init];
            
            for(AJNInterfaceDescription* interface in proxy.interfaces)
            {
                AJNMessageArgument* values;
                qStatus = [proxy propertyValues:&values ofInterfaceWithName:interface.name];
                if(qStatus != ER_OK)
                    continue;
                
                NSDictionary* propertyDict = nil;
                if(values.type)
                    propertyDict = [EluxAjDataConverter propertyDataArgToDict:values];
                
                if(!propertyDict)
                    continue;
                NSArray* propertyNames = [propertyDict allKeys];
                for(NSString* propertyName in propertyNames)
                {
                    if((propertyFileringMask & kFilteringMaskVersion) && [propertyName isEqualToString:@"Version"])
                       continue;
                    NSString* propertyValueString = [propertyDict objectForKey:propertyName];
                    eluxPropertyType propertyType = eluxPropertyValueTypeIvalid;
                    id propertyValue = [EluxAjDataConverter getEluxPropertyValueFromAjString:propertyValueString andType:&propertyType];
                    EluxRemoteProperty* remoteProperty = [[EluxRemoteProperty alloc]initWithName:propertyName path:interface.name valueContainer:[[EluxRemoteValueContainer alloc]initWithType:propertyType  andValue: propertyValue] isWritable:YES];
                    [remoteProperties addObject:remoteProperty];
#ifdef DEBUG
//                    NSLog(@"appliance.name: %@ interfaceName: %@, property name: %@ and value:%ld", name, interface.name, propertyName, (long)[[remoteProperty getValue]integerValue]);
                    log = [log stringByAppendingFormat:@"appliance.name: %@\n  - subUnit: %@\n    - property name: %@\n    - property value:%ld\n", name, objectPath, propertyName, (long)[[remoteProperty getValue]integerValue]];
#endif
                }
                log = [log stringByAppendingString:@"\n"];
            }
            log = [log stringByAppendingString:@"\n\n"];
            EluxSubUnit* subUnit = [[EluxSubUnit alloc]initWithPath:objectPath parentAppliance:appliance remoteProperties:remoteProperties andRemoteMethods:remoteMethods];
            [subUnits addObject:subUnit];
        }
        NSLog(@"%@", log);
        [appliance setSubUnits:subUnits];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ [mApplianceDiscoveryListener onApplianceDiscovered:appliance]; });
    }
    else
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ [mApplianceDiscoveryListener onDiscoveryFailed:[EluxErrors createSimpleErrorWithCode:ERR_AJ_WRONG_ANNOUNCEMENT andRerrorMEssage:ERR_AJ_WRONG_ANNOUNCEMENT_STRING]]; });
}

#pragma mark Property monitoring
-(BOOL)registerListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit
{
    if(!listener || !propertyName || !subUnit)
        return  NO;
    
    if(!mAjPropertyChangeListener)
        mAjPropertyChangeListener = [[EluxAjPropertyChangeListener alloc]init];
    
    EluxAppliance* parentAppliance = [subUnit getAppliance];
    if(!parentAppliance)
        return NO;
    
    EluxRemoteProperty* remotePorperty = [subUnit getPropertyByName:propertyName];
    if(!remotePorperty)
        return NO;
    
    NSString* busName = [parentAppliance getPlatformSpecificBus];
    if(!busName)
        return NO;
    if(!busName.length)
        return  NO;
    
    NSString* objectPath = [subUnit getPath];
    if(!objectPath)
        return NO;
    if(!objectPath.length)
        return NO;
    
    NSString* interfaceName = [remotePorperty getPath];
    if(!interfaceName)
        return NO;
    if(!interfaceName.length)
        return NO;
    
    NSInteger portNumber = [ ((NSNumber*)[parentAppliance getPlatformSpecificPort]) integerValue];
    
    AJNSessionId sessionId = 0;
    NSNumber* sess = [mDeviceSessionDict objectForKey:busName];
    if(sess)
        sessionId = [sess unsignedIntValue];
    
    if (sessionId == 0 || sessionId == -1)
    {
        AJNSessionOptions *opt = [[AJNSessionOptions alloc] initWithTrafficType:kAJNTrafficMessages
                                                             supportsMultipoint:false
                                                                      proximity:kAJNProximityAny
                                                                  transportMask:kAJNTransportMaskAny];
        
        sessionId = [mClientBusAttachment joinSessionWithName:busName onPort:portNumber withDelegate:self options:opt];
    }
    
    if (sessionId == 0 || sessionId == -1)
        return NO;
    
    
    AJNProxyBusObject* proxy = [[AJNProxyBusObject alloc] initWithBusAttachment:mClientBusAttachment
                                                                    serviceName:busName
                                                                     objectPath:objectPath
                                                                      sessionId:sessionId
                                                                 enableSecurity:YES];
    
    if(!proxy)
        return NO;
    
    [mClientBusAttachment enableConcurrentCallbacks];
    QStatus qStatus = [proxy registerPropertiesChangedListener:interfaceName
                                                        properties:[NSArray arrayWithObjects:propertyName, nil]
                                                          delegate:mAjPropertyChangeListener
                                                           context:nil];
    if(qStatus != ERR_OK)
        return NO;

    if(!mObservedObjects)
        mObservedObjects = [[NSMutableArray alloc]init];
    [mObservedObjects addObject:proxy];
    return [mAjPropertyChangeListener registerListener:listener forProperty:propertyName ofSubUnit:subUnit];
}

-(void)unRegisterListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit
{
    if(listener && propertyName && subUnit && mAjPropertyChangeListener)
        [mAjPropertyChangeListener unRegisterListener:listener forProperty:propertyName ofSubUnit:subUnit];
}



#pragma mark AJNBusListener protocol methods
- (void)didFindAdvertisedName:(NSString *)name
            withTransportMask:(AJNTransportMask)transport
                   namePrefix:(NSString *)namePrefix
{
    NSLog(@"didFindAdvertisedName %@", name);
    switch(mTaskId)
    {
        case AJ_TASK_ID_WIFI_SCAN:
            [self performSelector:@selector(wifiScanFinalize) withObject:nil afterDelay:2.0];
            break;
            
        case AJ_TASK_ID_ONBBOARD:
            break;
            
        case AJ_TASK_ID_OFFBOARD:
            [self performSelector:@selector(offboardFinalize) withObject:nil afterDelay:1.0];
            break;
            
        case AJ_TASK_ID_SEARCH_APPLIANCES:
            break;
            
        default:
            break;
    }
    if(mTaskId != AJ_TASK_ID_SEARCH_APPLIANCES)
        mTaskId = AJ_TASK_ID_ALL_DONE;
}

- (void)didLoseAdvertisedName:(NSString *)name
            withTransportMask:(AJNTransportMask)transport
                   namePrefix:(NSString *)namePrefix
{
    NSLog(@"didLoseAdvertisedName");
    QStatus status;
    // Find the button title that should be removed
    for (NSString *key in [mClientInformationDict allKeys])
    {
        if ([[[[mClientInformationDict valueForKey:key] announcement] busName] isEqualToString:name])
        {
            // Cancel advertise name for that bus
            status = [mClientBusAttachment cancelFindAdvertisedName:name];
            if (status != ER_OK)
                NSLog(@"[%@] [%@] failed to cancelFindAdvertisedName for %@. status:%@", @"DEBUG", [[self class] description], name, [AJNStatus descriptionForStatusCode:status]);
            // Remove the anouncement from the dictionary
            [mClientInformationDict removeObjectForKey:key];
        }
    }
}

#pragma mark Util methods
- (void)loadNewSession
{
    mIsAboutClientConnected = false;
    mCreationQueue = dispatch_queue_create("com.electrolux.alljoyn.creationQueue", NULL);
    mRealmBusName = DEFAULT_REALM_BUS_NAME;
}


-(void)connectionResultSignalReceived:(int)connectionResultCode connectionResultMessage:(NSString *)connectionResultMessage
{
    
}

- (BOOL)isSimulatorDevice
{
    bool systemVersionGreaterOrEqualTo9 = ([[[UIDevice currentDevice] systemVersion] compare:@"9.0" options:NSNumericSearch] != NSOrderedAscending);
    if (systemVersionGreaterOrEqualTo9)
    {
#ifdef TARGET_OS_SIMULATOR
        return TARGET_OS_SIMULATOR != 0;
#endif
#ifdef TARGET_IPHONE_SIMULATOR
        return TARGET_IPHONE_SIMULATOR != 0;
#endif
    }
    else
    {
        NSString *deviceModel = [[UIDevice currentDevice] model];
        return [deviceModel isEqualToString:@"iPhone Simulator"] || [deviceModel isEqualToString:@"iPad Simulator"];
    }
}

-(BOOL)shouldAcceptSessionJoinerNamed:(NSString *)joiner onSessionPort:(AJNSessionPort)sessionPort withSessionOptions:(AJNSessionOptions *)options
{
    return YES;
}

#pragma mark Appliances discovery methods
-(void)searchForAvailableAppliances:(id<EluxApplianceDiscoveryDelegate>)listener
{
    mApplianceDiscoveryListener = listener;
    mTaskId = AJ_TASK_ID_SEARCH_APPLIANCES;
    mFoundAppliancesQueue = dispatch_queue_create("com.electrolux.alljoyn.foundAppliancesQueue", NULL);
    [self startAboutClient];
}

#pragma mark property controlling
-(EluxRemoteProperty*)getPropertyWithName:(NSString*)name inIterface:(NSString*)interfaceName inObject:(NSString*)objectPath withPort:(NSInteger)portNumber andBusName:(NSString*)busName
{
    if(!mClientBusAttachment)
        [self startAboutClient];
    
    AJNSessionId sessionId = 0;
    NSNumber* sess = [mDeviceSessionDict objectForKey:busName];
    if(sess)
        sessionId = [sess unsignedIntValue];
    
    if (sessionId == 0 || sessionId == -1)
    {
        AJNSessionOptions *opt = [[AJNSessionOptions alloc] initWithTrafficType:kAJNTrafficMessages
                                                             supportsMultipoint:false
                                                                      proximity:kAJNProximityAny
                                                                  transportMask:kAJNTransportMaskAny];
        
        sessionId = [mClientBusAttachment joinSessionWithName:busName onPort:portNumber withDelegate:self options:opt];
    }
    
    if (sessionId == 0 || sessionId == -1)
        return nil;
    
    AJNProxyBusObject* proxy = [[AJNProxyBusObject alloc] initWithBusAttachment:mClientBusAttachment serviceName:busName objectPath:objectPath sessionId:sessionId enableSecurity:NO];
    QStatus qStatus = [proxy introspectRemoteObject];
    if (qStatus != ER_OK)
        return nil;
    
    AJNMessageArgument* values;
    qStatus = [proxy propertyValues:&values ofInterfaceWithName:interfaceName];
    if(qStatus != ER_OK)
        return nil;
    
    //does not work :-(
    //[proxy propertyWithName:name forInterfaceWithName:interfaceName];
    
    NSDictionary* propertyDict = nil;
    if(values.type)
        propertyDict = [EluxAjDataConverter propertyDataArgToDict:values];
    
    if(!propertyDict)
        return nil;
    
    NSString* propertyValueString = [propertyDict objectForKey:name];
    eluxPropertyType propertyType = eluxPropertyValueTypeIvalid;
    id propertyValue = [EluxAjDataConverter getEluxPropertyValueFromAjString:propertyValueString andType:&propertyType];
    return [[EluxRemoteProperty alloc]initWithName:name path:interfaceName valueContainer:[[EluxRemoteValueContainer alloc]initWithType:propertyType  andValue: propertyValue] isWritable:YES];
}

-(BOOL)setPropertyWithName:(NSString*)name inIterface:(NSString*)interfaceName inObject:(NSString*)objectPath withPort:(NSInteger)portNumber andBusName:(NSString*)busName toValue:(NSString*)value
{
    AJNSessionId sessionId = 0;
    NSNumber* sess = [mDeviceSessionDict objectForKey:busName];
    if(sess)
        sessionId = [sess unsignedIntValue];
    
    if (sessionId == 0 || sessionId == -1)
    {
        AJNSessionOptions *opt = [[AJNSessionOptions alloc] initWithTrafficType:kAJNTrafficMessages
                                                             supportsMultipoint:false
                                                                      proximity:kAJNProximityAny
                                                                  transportMask:kAJNTransportMaskAny];
        
        sessionId = [mClientBusAttachment joinSessionWithName:busName onPort:portNumber withDelegate:self options:opt];
    }
    
    if (sessionId == 0 || sessionId == -1)
        return NO;
    
    AJNProxyBusObject* proxy = [[AJNProxyBusObject alloc] initWithBusAttachment:mClientBusAttachment serviceName:busName objectPath:objectPath sessionId:sessionId enableSecurity:NO];
    QStatus qStatus = [proxy introspectRemoteObject];
    if (qStatus != ER_OK)
        return NO;
    
    //get the AJ property type
    AJNMessageArgument* values;
    qStatus = [proxy propertyValues:&values ofInterfaceWithName:interfaceName];
    if(qStatus != ER_OK)
        return NO;
    
    NSDictionary* propertyDict = nil;
    if(values.type)
        propertyDict = [EluxAjDataConverter propertyDataArgToDict:values];
    
    if(!propertyDict)
        return NO;
    
    NSString* propertyValueString = [propertyDict objectForKey:name];
    if(!propertyValueString)
        return NO;
    NSString* ajPropertyType = [EluxAjDataConverter getAJDataTypeFromAjString:propertyValueString];
    if(!ajPropertyType)
        return NO;
    
    AJNMessageArgument* msgarg = [[AJNMessageArgument alloc]init];
    [msgarg stabilize];
    
    switch((NSInteger)[EluxAjDataConverter getEluxPropertyValueTypeFromAjString:propertyValueString])
    {
        case eluxPropertyValueTypeIvalid:
            return NO;
        case eluxPropertyValueTypeInt8:
        {
            int8_t valueToPass = [value intValue];
            qStatus = [msgarg setValue:ajPropertyType, valueToPass];
            break;
        }
        case eluxPropertyValueTypeUInt8:
        {
            uint8_t valueToPass = [value intValue];
            qStatus = [msgarg setValue:ajPropertyType, valueToPass];
            break;
        }
        case eluxPropertyValueTypeInt16:
        {
            int16_t valueToPass = [value intValue];//[((NSNumber*)value) shortValue];
            qStatus = [msgarg setValue:ajPropertyType, valueToPass];
            break;
        }
        case eluxPropertyValueTypeUInt16:
        {
            uint16_t valueToPass = [value intValue];//[((NSNumber*)value) unsignedShortValue];
            qStatus = [msgarg setValue:ajPropertyType, valueToPass];
            break;
        }
        case eluxPropertyValueTypeInt32:
        {
            int32_t valueToPass = [value intValue];//[((NSNumber*)value) intValue];
            qStatus = [msgarg setValue:ajPropertyType, valueToPass];
            break;
        }
        case eluxPropertyValueTypeUInt32:
        {
            uint32_t valueToPass = [value intValue];//[((NSNumber*)value) unsignedIntValue];
            qStatus = [msgarg setValue:ajPropertyType, valueToPass];
            break;
        }
        case eluxPropertyValueTypeInt64:
        {
            int64_t valueToPass = [value intValue];//[((NSNumber*)value) longLongValue];
            qStatus = [msgarg setValue:ajPropertyType, valueToPass];
            break;
        }
        case eluxPropertyValueTypeUInt64:
        {
            uint64_t valueToPass = [value intValue];//[((NSNumber*)value) unsignedLongLongValue];
            qStatus = [msgarg setValue:ajPropertyType, valueToPass];
            break;
        }
        case eluxPropertyValueTypeUTF8String:
        {
            const char* valueToPass = [((NSString*)value) UTF8String];
            qStatus = [msgarg setValue:ajPropertyType, valueToPass];
            break;
        }
        case eluxPropertyValueTypeBool:
        {
            BOOL valueToPass = [value boolValue];//[((NSString*)value) boolValue];
            qStatus = [msgarg setValue:ajPropertyType, valueToPass];
            break;
        }
    }
    
    [msgarg stabilize];
    
    if (qStatus != ER_OK)
    {
        NSString *message = [NSString stringWithFormat:@"ERROR: Failed to set property. %@", [AJNStatus descriptionForStatusCode:qStatus]];
        NSLog(@"%@", message);
        return NO;
    }
    
    qStatus = [proxy setPropertyWithName:name forInterfaceWithName:interfaceName toValue:msgarg];
    if (qStatus != ER_OK)
    {
        NSString *message = [NSString stringWithFormat:@"ERROR: Failed to set property. %@", [AJNStatus descriptionForStatusCode:qStatus]];
        NSLog(@"%@", message);
        return NO;
    }
    return YES;
}

#pragma mark AjEvents
-(BOOL)startAjEventsConsumer
{
    if(!mAjEventHandler)
        mAjEventHandler = [[EluxAjEventHandler alloc]initWithBusAttachment:mClientBusAttachment andAppName:[self getAppNAme]];
    
    return [mAjEventHandler start];
}

-(void)stopAjEventsConsumer
{
    if(mAjEventHandler)
        [mAjEventHandler stop];
    mAjEventHandler = nil;
}

/**
 * Called by the bus when an existing session becomes disconnected.
 *
 * @param sessionId     Id of session that was lost.
 */
- (void)sessionWasLost:(AJNSessionId)sessionId
{
    NSArray* keys = [mDeviceSessionDict allKeys];
    for(NSString* key in keys)
    {
        NSNumber* num = [mDeviceSessionDict objectForKey:key];
        if([num unsignedIntValue] == sessionId)
        {
            [mDeviceSessionDict removeObjectForKey:key];
            return;
        }
    }
}

/**
 * Called by the bus when an existing session becomes disconnected.
 *
 * @param sessionId     Id of session that was lost.
 * @param reason        The reason for the session being lost
 *
 */
- (void)sessionWasLost:(AJNSessionId)sessionId forReason:(AJNSessionLostReason)reason
{
    NSLog(@"Session %d was lost", sessionId);
    [self sessionWasLost:sessionId];
}

/**
 * Called by the bus when a member of a multipoint session is added.
 *
 * @param memberName    Unique name of member who was added.
 * @param sessionId     Id of session whose member(s) changed.
 */
- (void)didAddMemberNamed:(NSString *)memberName toSession:(AJNSessionId)sessionId
{
    
}

/**
 * Called by the bus when a member of a multipoint session is removed.
 *
 * @param memberName    Unique name of member who was added.
 * @param sessionId     Id of session whose member(s) changed.
 */
- (void)didRemoveMemberNamed:(NSString *)memberName fromSession:(AJNSessionId)sessionId
{
    
}

@end
