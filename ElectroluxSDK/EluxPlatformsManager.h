//
//  EluxPlatformsManager.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

@class EluxApplianceManager;
@class EluxAppliance;
@class EluxConnectivityPlatformAdapter;
@class EluxConnectivityPlatformType;

@protocol EluxApplianceWifiScanDelegate;
@protocol EluxApplianceDiscoveryDelegate;
@protocol EluxPlatformConnectionStateDelegate;

@interface EluxPlatformsManager : NSObject
{
    NSString* TAG;
    unsigned long NR_SUPPORTED_PLATFORMS;
    NSMutableArray* mPlatformConnectors;
    BOOL mAllConnected;
    EluxApplianceManager* mApplianceManager;
    NSMutableArray* mDiscoveryListeners;
    id<EluxApplianceDiscoveryDelegate> mSDKApplianceDiscoveryListener;
}

+(EluxPlatformsManager*)sharedInstance;
+(EluxPlatformsManager*)sharedInstanceWithApplianceManager:(EluxApplianceManager*)am;
-(unsigned long)getPlatformIndexForSSID:(NSString*)ssid;
-(void)setApplianceManager:(EluxApplianceManager*)am;
-(NSArray*)getDiscoveryListeners;
-(NSArray*)getPlatformAdapters;
-(EluxApplianceManager*)getApplianceManager;
-(unsigned long)getPlatformIndex:(EluxConnectivityPlatformType*)platform;
+(unsigned long)getPlatformIndex:(EluxConnectivityPlatformType*)platform;
-(EluxConnectivityPlatformAdapter*)getPlatformAdapterForPlatform:(EluxConnectivityPlatformType*)platform;
-(BOOL)disconnect;
-(BOOL)registerConnectionStateListener:(id<EluxPlatformConnectionStateDelegate>)listener;
-(BOOL)unregisterConnectionStateListener:(id<EluxPlatformConnectionStateDelegate>)listener;
-(BOOL)connect;
//-(BOOL)wifiScan:(EluxAppliance*)appliance withWifiListener:(EluxApplianceWifiScanListener*) listener;
-(BOOL)wifiScan:(EluxAppliance*)appliance withWifiListener:(id<EluxApplianceWifiScanDelegate>) listener;
-(BOOL)isConnected:(EluxApplianceManager*)applianceManager;
-(BOOL)isCurrentSSIDApplienceAP;
-(NSString*)getCurrentSSID;
-(void)registerDiscoveryListener:(id<EluxApplianceDiscoveryDelegate>)listener;
-(void)unregisterDiscoveryListener:(id<EluxApplianceDiscoveryDelegate>)listener;


@end
