//
//  EluxGenericWifiScan.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxGenericWifiScan.h"
#import "SystemConfiguration/CaptiveNetwork.h"

@implementation EluxGenericWifiScan

+(NSString*)getCurrentSSID
{
    NSString* currentSSID = nil;
    NSArray *supportedInterfaces = (__bridge_transfer id)CNCopySupportedInterfaces();
    id interfaceInformation = nil;
    for (NSString *interfaceName in supportedInterfaces)
    {
        interfaceInformation = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)interfaceName);
        NSLog(@"    %@:%@", interfaceName, interfaceInformation);
        NSDictionary *dict = interfaceInformation;
        currentSSID = dict[@"SSID"];
    }
    return currentSSID;
}

@end
