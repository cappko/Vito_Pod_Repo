//
//  EluxApplianceManager.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

#define INVALID_ENTITY_ID -1

@class EluxPlatformsManager;
@class EluxApplianceInfo;
@class EluxAppliance;
@class EluxConnectivityPlatformType;
@protocol EluxPlatformApplianceProtocol;
@protocol EluxApplianceDiscoveryDelegate;

//Passing this as an argument for setShowPorpertyListFilteringMask will filter no properties at all. This is the default value.
#define kFilteringMaskNone      0
//Passing this as an argument for setShowPorpertyListFilteringMask will filter out all properties called "Version"
#define kFilteringMaskVersion   1
//Passing this as an argument for setShowPorpertyListFilteringMask will filter out all properties.
#define kFilteringMaskAll       2


@interface EluxApplianceManager : NSObject
{
    NSString* TAG;
    EluxPlatformsManager* mPlatformsManager;
    NSMutableDictionary* mCachedAppliances;
    NSInteger mPropertyFilteringMask;
}


/**
 * Creates an EluxApplianceManager singleton
 *
 * @param pm Instance of EluxPlatformsManager
 * @return If successfull returns an instance of EluxApplianceManager, nil otherwise
 */
+(EluxApplianceManager*)sharedInstanceWithPlatformManager:(EluxPlatformsManager*)pm;


-(void)setPlatformsManager:(EluxPlatformsManager*)pm;
-(EluxPlatformsManager*)getPlatformsManager;


/**
 * Returns a NSDictionary in form: <NSString uniqueId, EluxAppliance appliance>
 * @return Returns a NSDictionary in form: <NSString uniqueId, EluxAppliance appliance>
 */
-(NSDictionary*)getCachedAppliances;

/**
 * Disconnect the platforms, notifies the attached discovery listeners and clear appliance cache
 */
-(void)forceClearAppliancesCache;

/**
 * Get appliance by its SDK wide unique ID
 * @param applianceId id of the device
 * @return Appliance or nil if not found
 */
-(EluxAppliance*)getApplianceById:(NSInteger)applianceId;

/**
 * Get appliance by its SDK wide unique ID in a string format
 * @param applianceId id of the device
 * @return Appliance or nil if not found
 */
-(EluxAppliance*)getApplianceByStringId:(NSString*)applianceId;

/**
 * Create fresh instance of appliance.
 * @param platform Platform the device was created for
 * @param appInfo Appliance info object
 * @return new instance of Appliance
 */
-(EluxAppliance*)createAppliance:(EluxConnectivityPlatformType*)platform withApplianceInfo:(EluxApplianceInfo*)appInfo;

/**
 * Create fresh instance of appliance.
 * @param platform Platform the device was created for
 * @param appInfo Appliance info object
 * @return new instance of Appliance
 */
+(EluxAppliance*)createAppliance:(EluxConnectivityPlatformType*)platform withApplianceInfo:(EluxApplianceInfo*)appInfo;

/**
 * Add an appliance to the collection of appliances managed by EluxApplianceManager
 * @param appliance an appliance to be added
 */
-(void)addApplianceToCachedAppliances:(EluxAppliance*)appliance;

/**
 * Gets remote Platform appliance for SDK appliance.
 * @param appliance Appliance
 * @return Plaform appliance or nil if not found.
 */
-(id<EluxPlatformApplianceProtocol>)getPlatformAppliance:(EluxAppliance*)appliance;

/**
 * Get index in array of platfrom adapters.
 * @param platform Platform
 * @return index in the array of adapters.
 */
-(NSInteger)getPlatformIndex:(EluxConnectivityPlatformType*) platform;

/**
 * Converts appliance unique id represented by NSString to NSInteger
 * @param idString unique id represented by NSString
 * @return unique id represented by NSInteger
 */
-(NSInteger)uniqueIdStringToInteger:(NSString*)idString;

/**
 * Converts appliance unique id represented by NSInteger to NSString
 * @param idInteger unique id represented by NSInteger
 * @return unique id represented by NSString
 */
-(NSString*)uniqueIdIntegerToString:(NSInteger)idInteger;

/**
 *  Try to search for all visible/available appliances
 * @param listener EluxApplianceDiscoveryDelegate
 */
-(void)searchForAvailableAppliances:(id<EluxApplianceDiscoveryDelegate>)listener;

/**
 * By default all remote properties from an appliance are listed. 
 * This method specifies which properties we want to omit.
 * @param filteringMask A bit mask that specifies what properties to omit.
 */
-(void)setShowPorpertyListFilteringMask:(NSInteger)filteringMask;

/**
 * By default all remote properties from an appliance are listed.
 * This method allows you to get the bit mask specifying which properties are omited.
 * @return The bit mask specifying which properties are omited.
 */
-(NSInteger)getShowPorpertyListFilteringMask;


@end
