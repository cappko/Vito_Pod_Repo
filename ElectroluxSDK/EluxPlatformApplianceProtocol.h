//
//  EluxPlatformApplianceProtocol.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>
#import "EluxApplianceSimpleDataTypes.h"

@class EluxRemoteProperty;
@class EluxRemoteMethod;
@class EluxRemoteValueContainer;

@protocol EluxApplianceStateDelegate;
@protocol EluxRemoteOpProgressDelegate;

@protocol EluxPlatformApplianceProtocol <NSObject>

-(BOOL)isSupported;
-(enum EluxApplianceType)getType;
-(BOOL)connect;
-(BOOL)isConnected;
-(BOOL)disconnect;
-(BOOL)getRemotePropertyValue:(EluxRemoteProperty*) prop withRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener;
-(BOOL)setRemotePropertyValue:(EluxRemoteProperty*) prop
     withRemoteValueContainer:(EluxRemoteValueContainer*)value
  andRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener;

-(BOOL)setApplianceStateListener:(id<EluxApplianceStateDelegate>)listener;
-(BOOL)removeApplianceStateListener:(id<EluxApplianceStateDelegate>)listener;
-(NSInteger)getUniqueId;

/**
 * Return list of supported paths (identifiers) of subunits, that can be controlled
 * by this platform device.
 *
 * @return List os supported platform device subunit paths.
 */
-(NSArray*)getSupportedSubUnitPaths; //NSArray of NSStrings

/**
 *
 * @return an array of available remote methods.
 */
-(NSArray*)getRemoteMethods:(NSString*)subUnitPath;

/**
 *
 * @return collection of available remoteproperties.
 */
-(NSArray*)getRemoteProperties:(NSString*)subUnitPath;

/**
 * Triggrer invocation of method on a remote device.
 * @param remoteMethod AjMethod to be invoked.
 * @param args Arguments of method invocation.
 * @param listener Remote action progress listener.
 * @return true, if invocation was requested successfully, false otherwise.
 */

-(BOOL)invokeMethod:(EluxRemoteMethod*)remoteMethod withArguments:(NSArray*)args andRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener;

/**
 * Return information about method arguments
 *
 * @param remoteMethod remote method to be queried.
 *
 * TODO - to be removed
 * @deprecated, use method.getArgumentTypes instead.
 */
-(NSArray*)getMethodArgumentTypes:(EluxRemoteMethod*)remoteMethod __deprecated; //return NSArray of ids

/**
 * Check, if the appliance is onboarded.
 * @return true on success, false on fail
 */
-(BOOL)isOnboarded;

/**
 * Check, if the appliance can be onboarded to local network.
 * @return true on success, false on fail
 */
-(BOOL)isOnboardable;

/**
 * Check, if the appliance can be offboarded from local network.
 * @return true on success, false on fail
 */
-(BOOL)isOffboardable;

@end

#ifdef NOT_PROTOCOL_ONLY
@interface EluxPlatformAppliance : NSObject <EluxPlatformApplianceProtocol>
{
    
}
@end
#endif //NOT_PROTOCOL_ONLY
