//
//  EluxOnboardingDelegate.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

@class EluxAppliance;
@class EluxSSIDInfo;

@protocol EluxOnboardingDelegate <NSObject>

/**
* Called on appliance on-boarding process finished successfully
*
* @param onboardedAppliance Appliance object that can be connected or null if appliance not accessible or can not be connected at the moment.
*/
-(void)onApplianceOnboardingSucceeded:(EluxAppliance*)onboardedAppliance;

/**
 * * Called on appliance on-boarding process failed
 *
 * @param error Error specifying, what went wrong.
 */
-(void)onApplianceOnboardingFailed:(NSError*)error;


/**
 Called when password for SSID needed

 @param ssid Particular SSID. Can be obtained by calling searchForAvailableSSID on an instance of EluxOnboardingManager
 @return Supposed to return password for the particullar SSID
 */
-(NSString*)passwordForSSID:(EluxSSIDInfo*)ssid;


/**
 Called when any user interaction is needed (e.g. change wifi settings in iOS settings)
 
 @param interactionCode Code that scpecifies the particular user interaction that is needed
 @param interactionString Human readable string that scpecifies the particular user interaction that is needed
 */
-(void)userInteractionNeeded:(NSInteger)interactionCode andInterActionString:(NSString*)interactionString;

@end
