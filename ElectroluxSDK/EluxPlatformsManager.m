//
//  EluxPlatformsManager.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//


#import "EluxApplianceManager.h"
#import "EluxConnectivityPlatformType.h"
#import "EluxPlatformConnectionStateDelegate.h"
#import "EluxAppliance.h"
#import "EluxApplianceWifiScanDelegate.h"
#import "EluxApplianceDiscoveryDelegate.h"
#import "EluxApplianceWifiScan.h"
#import "EluxConnectivityPlatformAdapter.h"
#import "EluxAjPlatformAdapter.h"
#import "EluxGenericWifiScan.h"

#import "EluxPlatformsManager.h"

#define ALLJOYN_INDEX   0
#define HACLV4_INDEX    1

@implementation EluxPlatformsManager

+(EluxPlatformsManager*)sharedInstanceInner
{
    static EluxPlatformsManager* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[EluxPlatformsManager alloc] init];
        sharedInstance->TAG = NSStringFromClass([EluxPlatformsManager class]);
        sharedInstance->NR_SUPPORTED_PLATFORMS = [EluxConnectivityPlatformType values].count;
        sharedInstance->mDiscoveryListeners = [[NSMutableArray alloc]init];
        sharedInstance->mPlatformConnectors = [EluxConnectivityPlatformAdapter createNewAdapters:sharedInstance->NR_SUPPORTED_PLATFORMS];
    });
    return sharedInstance;
}

+(EluxPlatformsManager*)sharedInstance
{
    EluxPlatformsManager* sharedInstance = [EluxPlatformsManager sharedInstanceInner];
    EluxApplianceManager* applianceManager = [EluxApplianceManager sharedInstanceWithPlatformManager:sharedInstance];
    [sharedInstance setApplianceManager:applianceManager];
    [sharedInstance initAdapters];
    return sharedInstance;
}

+(EluxPlatformsManager*)sharedInstanceWithApplianceManager:(EluxApplianceManager*)am
{
    EluxPlatformsManager* sharedInstance = [EluxPlatformsManager sharedInstanceInner];
    [sharedInstance setApplianceManager:am];
    [sharedInstance initAdapters];
    return sharedInstance;
}

-(void)setApplianceManager:(EluxApplianceManager*)am
{
    mApplianceManager = am;
    [mApplianceManager setPlatformsManager:self];
}

-(void)initAdapters
{
    [self initAllJoynAdapter];
    [self initHACLv4Adapter];
    
}

-(void)initAllJoynAdapter
{
    EluxAjPlatformAdapter* platformAdapter = [[EluxAjPlatformAdapter alloc] initWithApplianceManager:mApplianceManager];
    [mPlatformConnectors setObject:platformAdapter atIndexedSubscript:ALLJOYN_INDEX];
}

-(void)initHACLv4Adapter
{
    
}

-(NSArray*)getDiscoveryListeners
{
    return mDiscoveryListeners;
}

-(NSArray*)getPlatformAdapters
{
    return mPlatformConnectors;
}

-(EluxApplianceManager*)getApplianceManager
{
    return mApplianceManager;
}

-(EluxConnectivityPlatformAdapter*)getPlatformAdapterForPlatform:(EluxConnectivityPlatformType*)platform
{
    if(mPlatformConnectors)
        return [mPlatformConnectors objectAtIndex:[self getPlatformIndex:platform]];
    return nil;
}

-(unsigned long)getPlatformIndexForSSID:(NSString*)ssid
{
    if([ssid hasPrefix:@"AJ_"])
        return ALLJOYN_INDEX;
    return -1;
}

+(unsigned long)getPlatformIndex:(EluxConnectivityPlatformType*)platform
{
    NSString* name = platform.getName;
    NSLog(@"name: %@", name);
    if([name isEqualToString: ALL_JOYN_NAME])
        return ALLJOYN_INDEX;
    if([name isEqualToString: HACLv4_NAME])
        return HACLV4_INDEX;
    return -1;
}

/**
 * Get index in array of platfrom adapters.
 * @param platform Platform
 * @return index in the array of adapters.
 */
-(unsigned long)getPlatformIndex:(EluxConnectivityPlatformType*)platform
{
    return [EluxPlatformsManager getPlatformIndex:platform];
    //    return [[EluxConnectivityPlatformType values]indexOfObject:platform];
}

/**
 * Disconnects from all the underlined platforms
 *
 * @return YES if the process was started successfully, NO otherwise.
 */
-(BOOL)disconnect
{
    mAllConnected = NO;
    BOOL ret = YES;
    for (EluxConnectivityPlatformAdapter* adapter in mPlatformConnectors)
    {
        if (adapter)
            ret &= [adapter disconnect];
    }
    /* when disconnecting, clear the appliances cache */
    [mApplianceManager forceClearAppliancesCache];
    return ret;
}

/**
 * Register connection state listener
 *
 * @param listener listener to be used
 * @return YES if the listener was registered successfully, NO otherwise
 */
-(BOOL)registerConnectionStateListener:(id<EluxPlatformConnectionStateDelegate>)listener
{
    BOOL ret = YES;
    for (EluxConnectivityPlatformAdapter* adapter in mPlatformConnectors)
    {
        if(adapter)
            ret &= [adapter registerConnectionStateListener:listener];
    }
    return ret;
}

/**
 * Unregister connection state listener
 *
 * @param listener listener to be used
 * @return YES if the listener was unregistered successfully, NO otherwise
 */
-(BOOL)unregisterConnectionStateListener:(id<EluxPlatformConnectionStateDelegate>)listener
{
    BOOL ret = YES;
    for (EluxConnectivityPlatformAdapter* adapter in mPlatformConnectors)
    {
        if(adapter)
            ret &= [adapter unregisterConnectionStateListener:listener];
    }
    return ret;
}

/**
 * Connect to all underlined platforms (allow discovery of all accessible devices)
 *
 * @return YES if the process was successful for all the underlined platforms, NO otherwise
 */
-(BOOL)connect
{
    if(mAllConnected)
    {
        NSLog(@"%@: %@", TAG, @"Attempt to connected already connected instance on manager...");
        return NO;
    }
    BOOL ret = YES;
    
    for(int i = 0; i < mPlatformConnectors.count; i++)
    {
        //EluxConnectivityPlatformAdapter* adapter = mPlatformConnectors[i];
        id adapter = mPlatformConnectors[i];
        if(!adapter)
        {
            NSLog(@"%@: %@ %d", TAG, @"Platform adapter is empty on index: ", i );
            continue;
        }
        NSLog(@"adapter: %@", [[adapter class] description]);
        ret &= [adapter connect:mSDKApplianceDiscoveryListener];
    }
    mAllConnected = true;
    return ret;
}

/**
 * Request the appliance to perform a Wifi scan, i.e., get the list of wifi networks visible
 * from it
 *
 * @param appliance {@link Appliance} to enquiry
 * @param listener {@link IApplianceWifiScan} to notify results
 * @return YES if request was sent successfully, NO otherwise
 */
-(BOOL)wifiScan:(EluxAppliance*)appliance withWifiListener:(id<EluxApplianceWifiScanDelegate>) listener
{
    if (listener)
    {
        EluxConnectivityPlatformType* type = [appliance getConnectivityPlatform];
        EluxConnectivityPlatformAdapter* platformAdapter = [mPlatformConnectors objectAtIndex:[self getPlatformIndex:type]];
        return [platformAdapter wifiScan:appliance withApplianceWifiScan:listener];
    }
    else
        NSLog(@"%@: %@", TAG, @"Wifi Scan: not enough parameters. EluxApplianceWifiScanListener == null");
    return NO;
}

-(BOOL)isCurrentSSIDApplienceAP
{
    NSString* currentSSID = [self getCurrentSSID];
    if(!currentSSID || !mPlatformConnectors)
        return NO;
    if(!currentSSID.length || !mPlatformConnectors)
        return NO;
    
    NSArray* arr = [[EluxConnectivityPlatformType allJoyn] getDiscoveryFilter];
    if(!arr)
        return NO;
    for(NSString* filter in arr)
    {
        if([currentSSID rangeOfString:filter].location == NSOrderedSame)
            return YES;
    }
    return NO;
}

-(NSString*)getCurrentSSID
{
    return [EluxGenericWifiScan getCurrentSSID];
}

/**
 * Returns if the manager is connected or not.
 * @return YES, if connectAll() was called and disconnectAll was not called yet, NO otherwise
 * @param applianceManager of type EluxApplianceManager*
 */
-(BOOL)isConnected:(EluxApplianceManager*)applianceManager
{
    return mAllConnected;
}

/**
 * Register appliance discovery listener. Listener callbacks will be called also for previously
 * discovered appliances, that are still recognized as accessible
 *
 * @param listener Discovery listener.
 */
-(void)registerDiscoveryListener:(id<EluxApplianceDiscoveryDelegate>)listener
{
    [mDiscoveryListeners addObject:listener];
    NSArray* appliances = [[mApplianceManager getCachedAppliances]allValues];
    for(EluxAppliance* appliance in appliances)
        [listener onApplianceDiscovered:appliance];
}

/**
 * Unregister appliance discovery listener. Listener callbacks will be called also for previously
 * discovered appliances, that are still recognized as accessible
 *
 * @param listener Discovery listener.
 */
-(void)unregisterDiscoveryListener:(id<EluxApplianceDiscoveryDelegate>)listener
{
    [mDiscoveryListeners removeObject:listener];
}

@end

