//
//  EluxConnectivityPlatformType.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//


#import <Foundation/Foundation.h>

#define ALL_JOYN_NAME   @"ALLJOYN"
#define HACLv4_NAME     @"HACLv4"

@interface EluxConnectivityPlatformType : NSObject
{
    NSString* mName;
    NSArray* mDiscoveryFilter;
}

-(NSString*)getName;
-(NSArray*)getDiscoveryFilter;
-(EluxConnectivityPlatformType*)initWithPlatformType:(NSString*)name andFilter:(NSArray*)onboardingFilter;
+(NSArray*)values;
+(EluxConnectivityPlatformType*)allJoyn;
+(EluxConnectivityPlatformType*)HACLv4;

@end
