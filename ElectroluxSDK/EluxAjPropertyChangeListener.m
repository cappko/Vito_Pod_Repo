//
//  EluxAjPropertyChangeListener.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//


#import "EluxAjPropertyChangeListener.h"
#import "EluxAjDataConverter.h"
#import "EluxApplianceManager.h"
#import "EluxPlatformsManager.h"
#import "EluxPropertyChangeDelegate.h"
#import "EluxRemoteValueContainer.h"

@implementation EluxListenerInfoContainer

-(EluxListenerInfoContainer*)initWithListener:(id<EluxPropertyChangeDelegate>)_listener property:(NSString*)_propertyName andSubUnit:(EluxSubUnit*)_subUnit
{
    self = [super init];
    if(self)
    {
        self.listener = _listener;
        self.propertyName = _propertyName;
        self.subUnit = _subUnit;
    }
    return self;
}

@synthesize listener;
@synthesize propertyName;
@synthesize subUnit;

@end


@implementation EluxAjPropertyChangeListener

-(EluxAjPropertyChangeListener*)init
{
    self = [super init];
    if(self)
        mListenersInfo = [[NSMutableArray alloc]init];
    return self;
}

/**
 * Callback to receive property changed events.
 *
 * @param obj           Remote bus object that owns the property that changed.
 * @param ifaceName     Name of the interface that defines the property.
 * @param changed       Property values that changed as an array of dictionary entries, signature "a{sv}".
 * @param invalidated   Properties whose values have been invalidated, signature "as".
 * @param context       Caller provided context passed in to RegisterPropertiesChangedListener
 */

- (void)didPropertiesChanged:(AJNProxyBusObject *)obj inteface:(NSString *)ifaceName changedMsgArg:(AJNMessageArgument *)changed invalidatedMsgArg:(AJNMessageArgument *)invalidated context:(AJNHandle)context
{
    NSString* str = obj.serviceName;
    NSLog(@"%@", str);
    str = obj.uniqueName;
    NSLog(@"%@", str);
    NSDictionary* propertyDict = nil;
    if(changed.type)
        propertyDict = [EluxAjDataConverter propertyDataArgToDict:changed];
/*
    if(invalidated.type)
        propertyDict = [EluxAjDataConverter propertyDataArgToDict:invalidated];
*/
    for(EluxListenerInfoContainer* listenerInfo in mListenersInfo)
    {
        //A NSDictionary of <NString* propertyName, EluxRemoteProperty* property>
        NSMutableDictionary* passedValuesDict = [[NSMutableDictionary alloc]init];
        for(NSString* propertyName in [propertyDict allKeys])
        {
            if([listenerInfo.propertyName isEqualToString: propertyName])
            {
                NSString* propertyValueString = [propertyDict objectForKey:propertyName];
                eluxPropertyType propertyType = eluxPropertyValueTypeIvalid;
                id propertyValue = [EluxAjDataConverter getEluxPropertyValueFromAjString:propertyValueString andType:&propertyType];
                
                EluxRemoteValueContainer* valueContainer = [[EluxRemoteValueContainer alloc]initWithType:propertyType andValue:propertyValue];
                EluxRemoteProperty* remoteProperty = [[EluxRemoteProperty alloc]initWithName:propertyName path:ifaceName valueContainer:valueContainer isWritable:YES];
                [passedValuesDict setObject:remoteProperty forKey:propertyName];
            }
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [listenerInfo.listener onPropertyChanged: passedValuesDict];
        });
    }
}

-(BOOL)registerListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit
{
    EluxListenerInfoContainer* listenerInfo = [[EluxListenerInfoContainer alloc]initWithListener:listener property:propertyName andSubUnit:subUnit];
    [mListenersInfo addObject:listenerInfo];
    return YES;
}

-(void)unRegisterListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit
{
    for(EluxListenerInfoContainer* listenerInfo in mListenersInfo)
    {
        if((listenerInfo.listener == listener) && (listenerInfo.propertyName == propertyName) && (listenerInfo.subUnit == subUnit))
            [mListenersInfo removeObject:listenerInfo];
    }
}

@end
