//
//  EluxAjEventHandler.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxAjEventHandler.h"
#import "EluxApplianceManager.h"
#import "EluxPlatformsManager.h"
#import "EluxAppliance.h"

@implementation EluxAjEventHandler

-(EluxAjEventHandler*)initWithBusAttachment:(AJNBusAttachment *)busAttachment andAppName:(NSString*)appName
{
    self = [super init];
    if(self)
    {
        mConsumerService = [AJNSNotificationService sharedInstance];
        mBusAttachment = busAttachment;
        mAppName = appName;
    }
    return self;
}

- (QStatus)start
{
    QStatus status;
//    [self.consumerService setLogLevel:QLEVEL_DEBUG];
    
    if (!mBusAttachment)
        return ER_FAIL;
    
    status = [mConsumerService startReceive:mBusAttachment withReceiver:self];
    if (status != ER_OK)
        return ER_FAIL;

    return ER_OK;
}

- (void)stop
{
    [mConsumerService shutdownReceiver];
    mConsumerService = nil;
}

- (void)parseNotification:(AJNSNotification *)ajnsNotification
{
    AJNSNotificationText *nt = ajnsNotification.ajnsntArr[0];
    
    EluxApplianceMessage* message = [[EluxApplianceMessage alloc]init];
    message.applianceName = [ajnsNotification appName];
    message.messageID = [NSString stringWithFormat:@"%d", [ajnsNotification messageId]];
    message.messageType = [AJNSNotificationEnums AJNSMessageTypeToString:[ajnsNotification messageType]];
    message.applianceID = [ajnsNotification deviceId];
    message.applianceName = [ajnsNotification deviceName];
    message.busName = [ajnsNotification senderBusName];
    message.appId = [ajnsNotification appId];
    message.lang = [nt getLanguage];
    message.text = [nt getText];
    
#ifdef DEBUG
    NSMutableString *notificationContent = [[NSMutableString alloc] init];
    [notificationContent appendFormat:@"Application name: %@\n[current app name is %@]\n", [ajnsNotification appName], mAppName];
    [notificationContent appendFormat:@"Message id: '%d'\n", [ajnsNotification messageId]];
    [notificationContent appendFormat:@"MessageType: '%@'\n", [AJNSNotificationEnums AJNSMessageTypeToString:[ajnsNotification messageType]]];
    [notificationContent appendFormat:@"Device id: '%@'\n", [ajnsNotification deviceId]];
    [notificationContent appendFormat:@"Device Name: '%@'\n", [ajnsNotification deviceName]];
    [notificationContent appendFormat:@"Sender: '%@'\n", [ajnsNotification senderBusName]];
    [notificationContent appendFormat:@"AppId: '%@'\n", [ajnsNotification appId]];
//  [notificationContent appendFormat:@"CustomAttributes: '%@'\n",[ajnsNotification.customAttributes description]];
    [notificationContent appendFormat:@"richIconUrl: '%@'\n", [ajnsNotification richIconUrl]];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [ajnsNotification richAudioUrl:array];
    
    if ([array count])
    {
        [notificationContent appendString:@"RichAudioUrl: "];
        for (AJNSRichAudioUrl *richAudioURL in array)
            [notificationContent appendFormat:@"'%@'", [richAudioURL url]];
        [notificationContent appendString:@"\n"];
    }
    else
        [notificationContent appendFormat:@"RichAudioUrl is empty\n"];
    
    [notificationContent appendFormat:@"richIconObjPath: '%@'\n", [ajnsNotification richIconObjectPath]];
    [notificationContent appendFormat:@"RichAudioObjPath: '%@'\n", [ajnsNotification richAudioObjectPath]];
    [notificationContent appendFormat:@"CPS Path: '%@'\n", [ajnsNotification controlPanelServiceObjectPath]];
    [notificationContent appendFormat:@"First msg: '%@' [total: %lu]\n", [nt getText], (unsigned long)[ajnsNotification.ajnsntArr count]];
    [notificationContent appendFormat:@"Received message Lang: '%@'\n", [nt getLanguage]];
    
    NSLog(@"Received new Notification:\n%@", notificationContent);
#endif
    EluxApplianceManager* applianceManager = [EluxApplianceManager sharedInstanceWithPlatformManager:[EluxPlatformsManager sharedInstance]];
    if(!applianceManager)
        return;
    
    EluxAppliance* appliance = [applianceManager getApplianceById:[[ajnsNotification deviceId]integerValue]];
    if(appliance)
        [appliance passMessageToListeners:nil];
}

#pragma mark - AJNSNotificationReceiver protocol methods
- (void)dismissMsgId:(const int32_t)msgId appId:(NSString *)appId
{

}

// Parse AJNSNotification into a string
- (void)receive:(AJNSNotification *)ajnsNotification
{
    [self parseNotification:ajnsNotification];
}

@end
