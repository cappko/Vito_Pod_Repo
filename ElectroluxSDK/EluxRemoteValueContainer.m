//
//  EluxRemoteValueContainer.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxRemoteValueContainer.h"
#import "EluxAppliance.h"

@implementation EluxRemoteValueContainer

-(EluxRemoteValueContainer*)initWithType:(eluxPropertyType)type andValue:(id)value
{
    self = [super init];
    if(self)
    {
        if([EluxRemoteValueContainer type:type corespondsToValue:value])
        {
            mType = type;
            mValue = value;
        }
        else
            self = nil;
    }
    return self;
}

-(enum eluxPropertyType)getType
{
    return mType;
}

-(id)getValue
{
    return mValue;
}

+(BOOL)type:(NSInteger)type corespondsToValue:(id)value
{
    //Given vaule is a valid number
    if(([value isKindOfClass:[NSNumber class]]) && [EluxRemoteValueContainer isNumber:type])
        return YES;
    if(([value isKindOfClass:[NSString class]]) && (type == eluxPropertyValueTypeUTF8String))
        return YES;
    return NO;
}

+(BOOL)isNumber:(NSInteger)type
{
    if(type > eluxPropertyValueTypeUInt64)
        return NO;
    return YES;
}
           
@end
