//
//  EluxAjPlatformAdapter.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxAjPlatformAdapter.h"///<ElectroluxSDK/EluxAjPlatformAdapter.h>
#import "EluxErrors.h"
#import "EluxErrorCodes.h"
#import "EluxOnboardingDelegate.h"
#import "EluxAjService.h"
#import "EluxOffboardingDelegate.h"
#import "EluxSSIDInfo.h"
#import "EluxAjAppliance.h"


@implementation EluxAjPlatformAdapter

-(EluxAjPlatformAdapter*)initWithApplianceManager:(EluxApplianceManager*)applianceManager
{
    self = [super init];
    mApplianceManager = applianceManager;
    TAG = NSStringFromClass([EluxAjPlatformAdapter class]);
    mAjService = [[EluxAjService alloc]init];
    mAjDeviceContainer = [[EluxAjApplianceContainer alloc]init];
    return self;
}

-(BOOL)connect:(id<EluxApplianceDiscoveryDelegate>)listener
{
    mSDKApplianceDicoveryListener = listener;
    return NO;
}

-(BOOL)disconnect
{
    return NO;
}

-(BOOL)assignPlatformAppliance:(EluxAjAppliance*)platformAppliance toEluxAppliance:(EluxAppliance*)appliance;
{
    if(!platformAppliance || !appliance)
        return NO;
    [platformAppliance setAppliance:appliance];
    [mAjDeviceContainer addAjAppliance:platformAppliance];
    return YES;
}

-(id<EluxPlatformApplianceProtocol>)getPlatformAppliance:(EluxAppliance*)appliance
{
    return (id<EluxPlatformApplianceProtocol>)[mAjDeviceContainer getAjAppliance:appliance];
}


-(void)onboard          :(EluxAppliance*)appliance
                  toSSID:(EluxSSIDInfo*)ssid
            withPassword:(NSString*)password
   andOnboardingListener:(id<EluxOnboardingDelegate>)listener
{
    if(mAjService)
        [mAjService onboard:appliance toSSID:ssid withListener:listener];
    else
    {
        NSLog(@"%@: AJ service not available (null)", TAG);
        [listener onApplianceOnboardingFailed: [EluxErrors createSimpleErrorWithCode:ERR_AJ_SERVICE_NOT_AVAILABLE andRerrorMEssage:ERR_AJ_SERVICE_NOT_AVAILABLE_STRING]];
    }
}

-(void)onboard          :(EluxAppliance*)appliance
         withWifiScanner:(EluxGenericWifiScan*)netInfo
       appliancePassword:(NSString*)appliancePassword
   andOnboardingListener:(id<EluxOnboardingDelegate>)listener
{
    [listener onApplianceOnboardingFailed: [EluxErrors createSimpleErrorWithCode:ERR_AJ_SERVICE_NOT_AVAILABLE andRerrorMEssage:ERR_AJ_SERVICE_NOT_AVAILABLE_STRING]];
}


-(void)offboard:(EluxAppliance*)appliance withOffboardingListener:(id<EluxOffboardingDelegate>)listener
{
    if(!listener)
        return;
    if(!mAjService)
    {
        [listener onApplianceOffboardingFailed:[EluxErrors createSimpleErrorWithCode:ERR_BAD_INITIALIZATION andRerrorMEssage:ERR_BAD_INITIALIZATION_STRING]];
        return;
    }
    [mAjService offboard:appliance withListener:listener];
}

-(BOOL)wifiScan:(EluxAppliance*)appliance withApplianceWifiScanDelegate:(id<EluxApplianceWifiScanDelegate>)listener
{
    if(!listener)
        return NO;
    if(!mAjService)
    {
        [listener listOfAvailableSSIDsFailedWithError:[EluxErrors createSimpleErrorWithCode:ERR_BAD_INITIALIZATION andRerrorMEssage:ERR_BAD_INITIALIZATION_STRING]];
        return NO;
    }
    [mAjService wifiScan:listener];
    
    return YES;
}

-(BOOL)bindAllJoynService
{
    return YES;
}

-(BOOL)unbindAlljoynService
{
    return NO;
}


-(void)didReceiveAnnounceOnBus:(NSString *)busName withVersion:(uint16_t)version withSessionPort:(AJNSessionPort)port withObjectDescription:(AJNMessageArgument *)objectDescriptionArg withAboutDataArg:(AJNMessageArgument *)aboutDataArg
{
    
}

-(BOOL)searchForAvailableAppliances:(id<EluxApplianceDiscoveryDelegate>)listener
{
    if(!listener)
        return NO;
    if(!mAjService)
    {
        [listener onDiscoveryFailed:[EluxErrors createSimpleErrorWithCode:ERR_BAD_INITIALIZATION andRerrorMEssage:ERR_BAD_INITIALIZATION_STRING]];
        return NO;
    }
    [mAjService searchForAvailableAppliances:listener];
    return YES;
}

-(EluxRemoteProperty*)getPropertyWithName:(NSString*)name inIterface:(NSString*)interfaceName inObject:(NSString*)objectPath withPort:(NSInteger)portNumber andBusName:(NSString*)busName
{
    if(!mAjService)
        return nil;
    return [mAjService getPropertyWithName:name inIterface:interfaceName inObject:objectPath withPort:portNumber andBusName:busName];
}

-(BOOL)setPropertyWithName:(NSString*)name inIterface:(NSString*)interfaceName inObject:(NSString*)objectPath withPort:(NSInteger)portNumber andBusName:(NSString*)busName toValue:(NSString*)value
{
    if(!mAjService)
        return NO;
    return [mAjService setPropertyWithName:name inIterface:interfaceName inObject:objectPath withPort:portNumber andBusName:busName toValue:value];
}

-(BOOL)startMessagesConsumer
{
    if(!mAjService)
        return NO;
    return [mAjService startAjEventsConsumer];
}

-(void)stopMessagesConsumer
{
    if(mAjService)
        [mAjService stopAjEventsConsumer];
}

-(BOOL)registerListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit
{
    if(mAjService)
        return [mAjService registerListener:listener forProperty:propertyName ofSubUnit:subUnit];
    return NO;
}

-(void)unRegisterListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit
{
    if(mAjService)
        [mAjService unRegisterListener:listener forProperty:propertyName ofSubUnit:subUnit];
}

@end
