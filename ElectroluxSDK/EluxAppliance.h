//
//  EluxAppliance.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>
#import "EluxApplianceSimpleDataTypes.h"
#import "EluxPlatformApplianceProtocol.h"
#import "EluxApplianceMessage.h"


@class EluxApplianceManager;
@class EluxConnectivityPlatformType;
@class EluxRemoteValueContainer;
@class EluxAppliance;

//@protocol EluxPlatformApplianceProtocol;
@protocol EluxPropertyChangeDelegate;


enum eluxPropertyType
{
    eluxPropertyValueTypeIvalid     = -1,
    eluxPropertyValueTypeInt8       = 0,
    eluxPropertyValueTypeUInt8      = 1,
    eluxPropertyValueTypeInt16      = 2,
    eluxPropertyValueTypeUInt16     = 3,
    eluxPropertyValueTypeInt32      = 4,
    eluxPropertyValueTypeUInt32     = 5,
    eluxPropertyValueTypeInt64      = 6,
    eluxPropertyValueTypeUInt64     = 7,
    eluxPropertyValueTypeUTF8String = 8,
    eluxPropertyValueTypeBool       = 9
};
 
static NSString* TAG_APPLIANCE_NAME                 = @"DeviceName";
static NSString* TAG_APPLIANCE_ID                   = @"DeviceId";
static NSString* TAG_DEFAULT_LANGUAGE               = @"DefaultLanguage";
static NSString* TAG_DESCRIPTION                    = @"Description";
static NSString* TAG_MANUFACTURER                   = @"Manufacturer";
static NSString* TAG_MODEL_NUMBER                   = @"ModelNumber";
static NSString* TAG_SOFTWARE_VERSION               = @"SoftwareVersion";
static NSString* TAG_CONNECTIVITY_PLATFORM          = @"connectivityPlatform";
static NSString* TAG_APPLIANCE_INTERFACE_METHODS    = @"applianceInterfaceMethods";
static NSString* TAG_APPLIANCE_PNC                  = @"pnc";
static NSString* TAG_APPLIANCE_ELC                  = @"elc";
static NSString* TAG_APPLIANCE_SERIAL               = @"serial";

@protocol EluxRemoteOpProgressDelegate <NSObject>
@end

@protocol EluxApplianceStateDelegate <NSObject>
@end

@protocol EluxEntityProtocol <NSObject>
-(NSInteger) getUniqueId;
@end


@interface EluxApplianceInfo : NSObject
{
    NSDictionary* mDeviceInfoDict;
}

-(EluxApplianceInfo*)initWithInfoDictionary:(NSDictionary*)dict;
-(NSString*)getAsString:(NSString*)key;
-(NSArray*)getKeys;
-(id)get:(NSString*)key;
-(NSString*)getName;
-(NSString*)getDescription;
-(NSString*)getDefaultLanguage;
-(NSString*)getApplianceId;
-(NSString*)getManufacturer;
-(NSString*)getModelNumber;
-(NSString*)getSoftwareVersion;
-(NSString*)getPnc;
-(NSString*)getElc;
-(NSString*)getSerialNumber;

@end

@interface EluxSubUnit : NSObject <EluxEntityProtocol>
{
    NSMutableArray<EluxRemoteMethod *>* mRemoteMethods;
    NSMutableArray<EluxRemoteProperty *>* mRemoteProperties;
    NSMutableDictionary* propertyChangeListeners;
    NSString* mPath;
    NSInteger mInstanceID;
    EluxAppliance* mAppliance;
}

-(EluxSubUnit*)initWithPath:(NSString*)path parentAppliance:(EluxAppliance*)appliance remoteProperties:(NSMutableArray<EluxRemoteProperty *>*)remoteProperties andRemoteMethods:(NSMutableArray<EluxRemoteMethod *>*)remoteMethods;
-(EluxApplianceState)getApplianceState;
-(void)clean;
-(NSString*)getName;
-(NSString*)getPath;
-(EluxAppliance*)getAppliance;
-(NSArray*)getMethods;
-(NSArray*)getProperites;
-(EluxRemoteProperty*)getPropertyByName:(NSString*)name;
-(EluxRemoteProperty*)getPropertyById:(NSInteger)entityId;
-(EluxRemoteMethod*)getMethod:(NSString*)name andTypeNames:(NSString*)argTypeNames;
-(EluxRemoteMethod*)getMethod:(NSInteger)methodEntityId;

-(id)getValueOfProperty:(EluxRemoteProperty*)remoteProperty;
-(id)getValueOfPropertyWithName:(NSString*)remoteProperty;
-(BOOL)setProperty:(EluxRemoteProperty*)property toValue:(id)value;
-(BOOL)setPropertyWithName:(NSString*)property toValue:(id)value;

-(void)setRemoteMethods:(NSMutableArray<EluxRemoteMethod *>*)remoteMethods;
-(void)setRemoteProperties:(NSMutableArray<EluxRemoteProperty *>*)remoteProperties;
-(BOOL)isSupported;
-(BOOL)registerPropertyListener:(NSArray*)propertyNames withListener:(id<EluxPropertyChangeDelegate>)listener;
-(void)unRegisterPropertyListener:(id<EluxPropertyChangeDelegate>)listener forProperties:(NSArray*)propertyNames;

@end

@interface EluxRemoteProperty : NSObject
{
    NSString* mPath;
    NSString* mName;
    EluxRemoteValueContainer* mValue;
    BOOL mIsWritable;
    NSInteger mInstanceID;
}

-(EluxRemoteProperty*)initWithName:(NSString*)name path:(NSString*)path valueContainer:(EluxRemoteValueContainer*)valueContainer isWritable:(BOOL)writable;
-(NSString*)getName;
-(NSString*)getSimpleName;
-(NSString*)getPath;

-(enum eluxPropertyType)getValueType;

/**
 * The method returns CACHED value of the property and do NOT perform any query on any underlying layer!
 * Can be used for an "offline" property checking, but DO NOT have to contain the actual vaule.
 * To receive actual value, allways call getValueOfProperty/getValueOfPropertyWithName method from the coresponding EluxSubUnit instance.
 *
 * @return  The method returns CACHED value of the property.
 */
-(id)getValue;

/**
 * Returns SKD-wide unique ID of this property
 * @return unique ID
 */
-(NSInteger) getUniqueId;
@end

@interface EluxRemoteMethod : NSObject
{
    NSInteger mInstanceID;
    NSString* mName;
    NSString* mPath;
}

-(EluxRemoteMethod*)initWithName:(NSString*)name;
-(NSString*)getName;
-(NSString*)getSimpleName;
-(NSString*)getSubunitPath;
-(BOOL)canBeInvoked;
-(BOOL)invoke:(NSArray*)args withListener:(id<EluxRemoteOpProgressDelegate>)listener;
-(NSInteger)getUniqueId;

@end


@interface EluxAppliance : NSObject <EluxPlatformApplianceProtocol>
{
    EluxApplianceManager* mApplianceManager;
    EluxApplianceInfo* mApplianceInfo;
    enum EluxApplianceType mType;
    enum EluxApplianceState mState;
    //NSArray of
    NSMutableArray<EluxSubUnit *> *mSubUnits;
    EluxConnectivityPlatformType* mPlatformType;
    NSInteger mInstanceID;
    NSString* muniqueIDstring;
    /** Appliance state listener - listen to changes between device states */
    NSMutableArray* mApplianceStateListeners;
    NSMutableArray<EluxApplianceMessagesDelegate>* mApplianceMessagesListeners;
    id mPlatformSpecificPort;
    id mPlatformSpecificBus;
}


-(void)setApplianceInfo:(EluxApplianceInfo*)applianceInfo;
-(void)setApplianceType:(enum EluxApplianceType)type;
-(void)setAppliancState:(enum EluxApplianceState)state;
-(void)setConnectivityPlatform:(EluxConnectivityPlatformType*)platformType;
-(void)setApplianceManager:(EluxApplianceManager*)applianceManager;
-(void)setSubUnits:(NSMutableArray<EluxSubUnit *>*)subUnits;
-(void)setPlatformSpecificPort:(id)port;
-(void)setPlatformSpecificBus:(id)bus;

-(id)getPlatformSpecificPort;
-(id)getPlatformSpecificBus;

-(EluxApplianceManager*)getApplianceManager;

-(EluxApplianceInfo*)getApplianceInfo;
-(enum EluxApplianceType)getApplianceType;
-(enum EluxApplianceState)getAppliancState;

/**
 * Getter for Platform type (see {@link ConnectivityPlatformType}
 *
 * @return State of the device
 */
-(EluxConnectivityPlatformType*) getConnectivityPlatform;

-(BOOL)isSupported;

-(enum EluxApplianceType)getType;

-(BOOL)connect;

-(BOOL)isConnected;

-(BOOL)disconnect;

-(BOOL)getRemotePropertyValue:(EluxRemoteProperty*) prop withRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener;

-(BOOL)setRemotePropertyValue:(EluxRemoteProperty*) prop
     withRemoteValueContainer:(EluxRemoteValueContainer*)value
  andRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener;

-(BOOL)setApplianceStateListener:(id<EluxApplianceStateDelegate>)listener;

-(BOOL)removeApplianceStateListener:(id<EluxApplianceStateDelegate>)listener;

-(NSArray*)getSupportedSubUnitPaths;

-(NSArray<EluxSubUnit*>*)getSubUnits;

-(NSArray*)getRemoteMethods:(NSString*)subUnitPath;

-(NSArray*)getRemoteProperties:(NSString*)subUnitPath;

-(NSInteger)getUniqueId;

-(NSString*)getUniqueIdString;

-(void)setUniqueId:(NSInteger)uniqueId;

-(void)setUniqueIdString:(NSString*)uniqueIdString;

-(BOOL)invokeMethod         :(EluxRemoteMethod*)remoteMethod
               withArguments:(NSArray*)args
 andRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener;

-(NSArray*)getMethodArgumentTypes:(EluxRemoteMethod*)remoteMethod __deprecated;

-(BOOL)isOnboarded;

-(BOOL)isOnboardable;

-(BOOL)isOffboardable;

-(BOOL)registerRemoteMessagesListeners:(NSArray<EluxApplianceMessagesDelegate>*)listeners;

-(BOOL)registerRemoteMessagesListener:(id<EluxApplianceMessagesDelegate>)listener;

-(void)unRegisterRemoteMessagesListeners:(NSArray<EluxApplianceMessagesDelegate>*)listeners;

-(void)unRegisterRemoteMessagesListener:(id<EluxApplianceMessagesDelegate>)listener;

-(void)passMessageToListeners:(EluxApplianceMessage*)message;

@end



