//
//  EluxApplianceWifiScanDelegate.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

@protocol EluxApplianceWifiScanDelegate <NSObject>


/**
 Called to pass an Array of found EluxSSIDInfo's instances

 @param availableSSIDs Array of EluxSSIDInfo's instances
 */
-(void)listOfAvailableSSIDsReceived:(NSArray*)availableSSIDs;

/**
 Called when an error occurs

 @param error Error description
 */
-(void)listOfAvailableSSIDsFailedWithError:(NSError*)error;


@end

#ifdef NOT_PROTOCOL_ONLY
@interface EluxApplianceWifiScanListener: NSObject <EluxApplianceWifiScanListener>

@end
#endif //NOT_PROTOCOL_ONLY
