//
//  EluxErrorCodes.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//


#ifndef EluxErrorCodes_h
#define EluxErrorCodes_h

//General error codes
#define ERR_OK                                      0
#define ERR_UNKNOWN                                 1
#define ERR_BAD_INITIALIZATION                      2
//General error messages
#define ERR_UNKNOWN_STRING                          @"Unknown error"
#define ERR_BAD_INITIALIZATION_STRING               @"Bad initialization"

//Onboarding errors
#define ERR_ONBOARDING_NO_SSID                          100
#define ERR_ONBOARDING_NOT_ONBOARDABLE                  101
#define ERR_ONBOARDING_WRONG_SSID                       102
#define ERR_ONBOARDING_NULL_PLATFORM_ADAPTER            103
#define ERR_ONBOARDING_UNSUPPORTED_PLATFORM             104
#define ERR_ONBOARDING_CONFIGURATION_FAILED             105
#define ERR_ONBOARDING_EMPTY_SSID                       107
#define ERR_ONBOARDING_CONNECT_FAILED                   108
#define ERR_ONBOARDING_CONFIRMATION_MSG_NOT_RECEIVED    109

//Onboarding error messages
#define ERR_ONBOARDING_NO_SSID_STRING                           @"No SSID available"
#define ERR_ONBOARDING_NOT_ONBOARDABLE_STRING                   @"Appliance is not onboardable"
#define ERR_ONBOARDING_WRONG_SSID_STRING                        @"Wrong SSID"
#define ERR_ONBOARDING_NULL_PLATFORM_ADAPTER_STRING             @"Platform adapter is null"
#define ERR_ONBOARDING_UNSUPPORTED_PLATFORM_STRING              @"Unsupported platform"
#define ERR_ONBOARDING_CONFIGURATION_FAILED_STRING              @"Configuration failed"
#define ERR_ONBOARDING_EMPTY_SSID_STRING                        @"Empty SSID"
#define ERR_ONBOARDING_CONNECT_FAILED_STRING                    @"Connect failed"
#define ERR_ONBOARDING_CONFIRMATION_MSG_NOT_RECEIVED_STRING     @"Confirmation announcement not received from onboardee."

//Offboarding errors
#define ERR_OFFBOARDING_COULD_NOT_START_ONBOAR_CLIENT       200
#define ERR_OFFBOARDING_COULD_NOT_OFFBOARD_APPLIANCE        201
#define ERR_OFFBOARDING_NO_SSID                             202
#define ERR_OFFBOARDING_NULL_PLATFORM_ADAPTER               103

//Offboarding error messages
#define ERR_OFFBOARDING_COULD_NOT_START_ONBOAR_CLIENT_STRING       @"Could not start onboarding client"
#define ERR_OFFBOARDING_COULD_NOT_OFFBOARD_APPLIANCE_STRING        @"Could not offboard appliance"
#define ERR_OFFBOARDING_NO_SSID_STRING                             @"No SSID available"
#define ERR_OFFBOARDING_NULL_PLATFORM_ADAPTER_STRING               @"Platform adapter is null"

//AllJoyn errors
#define ERR_AJ_SERVICE_NOT_AVAILABLE                1001
#define ERR_AJ_WIFI_SCAN_FAILED                     1002
#define ERR_AJ_WRONG_APPLIANCE_INFO                 1003
#define ERR_AJ_WRONG_APPLIANCE                      1004
#define ERR_AJ_WRONG_ABOUT_INFO                     1005
#define ERR_AJ_COULD_NOT_JOIN_SESSION               1005
#define ERR_AJ_COULD_NOT_INTROSPECT_OBJECT          1006
#define ERR_AJ_WRONG_ANNOUNCEMENT                   1007

//AllJoyn error messages
#define ERR_AJ_SERVICE_NOT_AVAILABLE_STRING         @"AllJoyn service not available"
#define ERR_AJ_WIFI_SCAN_FAILED_STRING              @"AllJoyn wifi scan failed"
#define ERR_AJ_WRONG_APPLIANCE_INFO_STRING          @"AllJoyn wrong appliance info"
#define ERR_AJ_WRONG_APPLIANCE_STRING               @"AllJoyn wrong appliance"
#define ERR_AJ_WRONG_ABOUT_INFO_STRING              @"AllJoyn wrong about info"
#define ERR_AJ_COULD_NOT_JOIN_SESSION_STRING        @"AllJoyn could not join session"
#define ERR_AJ_COULD_NOT_INTROSPECT_OBJECT_STRING   @"AllJoyn could not introspect remote object"
#define ERR_AJ_WRONG_ANNOUNCEMENT_STRING            @"AllJoyn wrong announcement"

#endif /* EluxErrorCodes_h */
