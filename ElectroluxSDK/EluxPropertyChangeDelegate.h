//
//  EluxPropertyChangeDelegate.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#ifndef EluxPropertyChangeListener_h
#define EluxPropertyChangeListener_h


/**
 * Listener for remote appliance property value change
 */
@protocol EluxPropertyChangeDelegate <NSObject>

/**
 * Called on value changed
 *
 * @param propertiesChanged A NSDictionary of <NString* propertyName, EluxRemoteProperty* property>
 */
-(void) onPropertyChanged:(NSDictionary*) propertiesChanged;

@end

#endif /* EluxPropertyChangeDelegate_h */
