//
//  EluxUserInteractionCodes.h
//  simple-app
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#ifndef EluxUserInteractionCodes_h
#define EluxUserInteractionCodes_h


//Onboarding
#define UI_ONBOARDING_CHANGE_WIFI                           10

//Onboarding strings
#define UI_ONBOARDING_CHANGE_WIFI_STRING                    @"Go to WiFi settings and connect to WiFi, that you tried onboard the appliance on"


#endif /* EluxUserInteractionCodes_h */
