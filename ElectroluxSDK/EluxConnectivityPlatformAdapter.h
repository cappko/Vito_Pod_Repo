//
//  EluxConnectivityPlatformAdapter.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

@protocol EluxPlatformApplianceProtocol;
@protocol EluxApplianceWifiScanDelegate;
@protocol EluxApplianceDiscoveryDelegate;
@protocol EluxPlatformConnectionStateDelegate;
@protocol EluxOnboardingDelegate;
@protocol EluxOffboardingDelegate;
@protocol EluxPropertyChangeDelegate;

@class EluxAppliance;
@class EluxSubUnit;
@class EluxGenericWifiScan;
@class EluxApplianceWifiScan;
@class EluxSSIDInfo;
@class EluxRemoteProperty;

@interface EluxConnectivityPlatformAdapter : NSObject
{
    NSMutableArray* mConnectionStateListeners;
}

+(NSMutableArray*)createNewAdapters:(unsigned long)numberOfSupportedPlatforms;

-(BOOL)connect:(id<EluxApplianceDiscoveryDelegate>)listener;
-(BOOL)disconnect;
-(BOOL)registerConnectionStateListener:(id<EluxPlatformConnectionStateDelegate>)listener;
-(BOOL)unregisterConnectionStateListener:(id<EluxPlatformConnectionStateDelegate>)listener;
-(id<EluxPlatformApplianceProtocol>)getPlatformAppliance:(EluxAppliance*)appliance;
-(BOOL)assignPlatformAppliance:(id<EluxPlatformApplianceProtocol>)platformAppliance toEluxAppliance:(EluxAppliance*)appliance;

-(void)onboard          :(EluxAppliance*)appliance
         withWifiScanner:(EluxGenericWifiScan*)netInfo
       appliancePassword:(NSString*)appliancePassword
   andOnboardingListener:(id<EluxOnboardingDelegate>)listener;


-(void)onboard          :(EluxAppliance*)appliance
                   toSSID:(EluxSSIDInfo*)ssid
             withPassword:(NSString*)password
   andOnboardingListener:(id<EluxOnboardingDelegate>)listener;

-(void)offboard:(EluxAppliance*)appliance withOffboardingListener:(id<EluxOffboardingDelegate>)listener;
-(BOOL)wifiScan:(EluxAppliance*)appliance withApplianceWifiScan:(EluxApplianceWifiScan*)scanResult;
-(BOOL)wifiScan:(EluxAppliance*)appliance withApplianceWifiScanDelegate:(id<EluxApplianceWifiScanDelegate>)scanResult;
-(BOOL)searchForAvailableAppliances:(id<EluxApplianceDiscoveryDelegate>)listener;
-(EluxRemoteProperty*)getPropertyWithName:(NSString*)name inIterface:(NSString*)interfaceName inObject:(NSString*)objectPath withPort:(NSInteger)portNumber andBusName:(NSString*)busName;
-(BOOL)setPropertyWithName:(NSString*)name inIterface:(NSString*)interfaceName inObject:(NSString*)objectPath withPort:(NSInteger)portNumber andBusName:(NSString*)busName toValue:(NSString*)value;
-(BOOL)startMessagesConsumer;
-(void)stopMessagesConsumer;
-(BOOL)registerListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit;
-(void)unRegisterListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit;

@end
