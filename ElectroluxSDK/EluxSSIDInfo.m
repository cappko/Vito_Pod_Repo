//
//  EluxSSIDInfo.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//


#import "EluxSSIDInfo.h"

@implementation EluxSSIDInfo

@synthesize name;
@synthesize password;
@synthesize authMethodString;
@synthesize authMethod;

/*
-(void)setName:(NSString*)_name
{
    self.name = _name;
}
-(void)setPassword:(NSString*)_password
{
    self.password = _password;
}
-(void)setAuthMethod:(NSInteger)_authMethod
{
    self.authMethod = _authMethod;
}
-(void)setAuthMethodString:(NSString *)_authMethodString
{
    self.authMethodString = _authMethodString;
}

-(NSString*)getName {return self.name; }
-(NSString*)getPassword {return self.password; }
-(NSInteger)getAuthMethod {return self.authMethod; }
-(NSString*)getAuthMethodString {return self.authMethodString; }
*/

@end
