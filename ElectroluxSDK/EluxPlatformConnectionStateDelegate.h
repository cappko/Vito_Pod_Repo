//
//  EluxPlatformConnectionStateDelegate.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

@protocol EluxPlatformConnectionStateDelegate <NSObject>

@end

#ifdef NOT_PROTOCOL_ONLY
@interface EluxPlatformConnectionStateListener: NSObject <EluxPlatformConnectionStateDelegate>

@end

#endif //NOT_PROTOCOL_ONLY
