//
//  AppliancesViewController.m
//  simple-app
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>


@interface EluxErrors : NSObject

+(NSError*)createSimpleErrorWithCode:(NSInteger)errorCode andRerrorMEssage:(NSString*)message;

@end
