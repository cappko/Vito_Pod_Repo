//
//  EluxAjDataConverter.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>
#import "EluxAppliance.h"
#import <alljoyn/MsgArg.h>
#import <alljoyn/samples_common/AJSCAboutDataConverter.h>


#define ELUX_VALUE_AND_TYPE_SEPARATOR   @"___VALUE_AND_TYPE_SEPARATOR___"

@interface EluxAjDataConverter : AJSCAboutDataConverter


/**
 * Convert AJNMessageArgument containing the property data to a NSMutableDictionary
 */
+(NSMutableDictionary *)propertyDataArgToDict:(AJNMessageArgument *)aboutDataArg;

+(NSString*)getAJDataTypeFromAjString:(NSString*)ajPropertyValueString;

+(eluxPropertyType)getEluxPropertyValueTypeFromAjString:(NSString*)ajPropertyValueString;

+(id)getEluxPropertyValueFromAjString:(NSString*)ajPropertyValueString andType:(eluxPropertyType*)type;


@end
