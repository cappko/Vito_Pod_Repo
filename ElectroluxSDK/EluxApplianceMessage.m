//
//  EluxApplianceMessage.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxApplianceMessage.h"

@implementation EluxApplianceMessage

@synthesize messageID;
@synthesize applianceID;
@synthesize applianceName;
@synthesize busName;
@synthesize richIconUrl;
@synthesize appId;
@synthesize richAudioUrl;
@synthesize richIconObjPath;
@synthesize richAudioObjPath;
@synthesize text;
@synthesize lang;

@end
