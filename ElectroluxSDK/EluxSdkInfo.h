//
//  EluxSdkInfo.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

#ifndef ELUX_SDK_INFO_H
#define ELUX_SDK_INFO_H


@interface EluxSdkInfo : NSObject

+(NSString*)getSdkVersion;
+(NSString*)getAllJoynVersion;
+(BOOL)isDebugBuild;
+(NSArray*)getSupportedPlatforms;


@end

#endif //ELUX_SDK_INFO_H
