//
//  EluxAjEventHandler.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>
#import <alljoyn/notification/AJNSNotificationReceiver.h>
#import <alljoyn/notification/AJNSNotificationService.h>


@interface EluxAjEventHandler : NSObject <AJNSNotificationReceiver>
{
    AJNSNotificationService* mConsumerService;
    AJNBusAttachment* mBusAttachment;
    NSString *mAppName;
}

-(EluxAjEventHandler*)initWithBusAttachment:(AJNBusAttachment *)busAttachment andAppName:(NSString*)appName;
-(QStatus)start;
-(void)stop;

@end
