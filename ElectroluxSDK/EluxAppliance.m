//
//  EluxAppliance.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//


#import "EluxAppliance.h"
#import "EluxPlatformApplianceProtocol.h"
#import "EluxConnectivityPlatformType.h"
#import "EluxRemoteValueContainer.h"
#import "EluxApplianceManager.h"
#import "EluxPlatformsManager.h"
#import "EluxConnectivityPlatformAdapter.h"

@implementation EluxRemoteProperty

-(EluxRemoteProperty*)initWithName:(NSString*)name path:(NSString*)path valueContainer:(EluxRemoteValueContainer*)valueContainer isWritable:(BOOL)writable;
{
    self = [super init];
    if(self)
    {
        mName = name;
        mPath = path;
        mValue = valueContainer;
        mIsWritable = writable;
        mInstanceID = arc4random();
    }
    return self;
}

-(NSString*)getName
{
    return mName;
}

-(NSString*)getSimpleName
{
    return mName;
}

-(NSString*)getPath
{
    return mPath;
}

-(NSInteger)getUniqueId
{
    return mInstanceID;
}

-(enum eluxPropertyType)getValueType
{
    return [mValue getType];
}

-(id)getValue
{
    return [mValue getValue];
}

@end

@implementation EluxSubUnit

-(EluxSubUnit*)initWithPath:(NSString*)path parentAppliance:(EluxAppliance*)appliance remoteProperties:(NSMutableArray<EluxRemoteProperty *>*)remoteProperties andRemoteMethods:(NSMutableArray<EluxRemoteMethod *>*)remoteMethods;
{
    self = [super init];
    if(self)
    {
        mRemoteProperties = remoteProperties;
        mRemoteMethods = remoteMethods;
        mPath = path;
        mInstanceID = arc4random();
        mAppliance = appliance;
    }
    return self;
}

-(EluxApplianceState)getApplianceState
{
    return CREATED;
}

-(void)clean
{
    
}

-(NSString*)getName
{
    return mPath;
}

-(NSString*)getPath
{
    return mPath;
}

-(EluxAppliance*)getAppliance
{
    return mAppliance;
}

-(NSArray*)getMethods
{
    return mRemoteMethods;
}

-(NSArray*)getProperites
{
    return mRemoteProperties;
}

-(EluxRemoteProperty*)getPropertyByName:(NSString*)name
{
    for(EluxRemoteProperty* property in mRemoteProperties)
    {
        if([[property getName] isEqualToString:name])
            return property;
    }
    return nil;
}

-(EluxRemoteProperty*)getPropertyById:(NSInteger)entityId
{
    for(EluxRemoteProperty* property in mRemoteProperties)
    {
        if([property getUniqueId] == entityId)
            return property;
    }
    return nil;
}

-(EluxRemoteMethod*)getMethod:(NSString*)name andTypeNames:(NSString*)argTypeNames
{
    return nil;
}

-(EluxRemoteMethod*)getMethod:(NSInteger)methodEntityId
{
    for(EluxRemoteMethod* method in mRemoteMethods)
    {
        if([method getUniqueId] == methodEntityId)
            return method;
    }
    return nil;
}

-(id)getValueOfProperty:(EluxRemoteProperty*)remoteProperty
{
    if([[[mAppliance  getConnectivityPlatform] getName]isEqualToString:ALL_JOYN_NAME])
    {
        NSArray* platformAdapters =[[[mAppliance getApplianceManager] getPlatformsManager]getPlatformAdapters];
        if(!platformAdapters)
            return nil;
        if(!platformAdapters.count)
            return nil;
        
        EluxConnectivityPlatformAdapter* adapter = [platformAdapters firstObject];
        NSNumber* ajPort = (NSNumber*)[mAppliance getPlatformSpecificPort];
        NSString* ajBus = (NSString*)[mAppliance getPlatformSpecificBus];
        EluxRemoteProperty* property = [adapter getPropertyWithName:[remoteProperty getName] inIterface:[remoteProperty getPath] inObject:[self getName] withPort:[ajPort integerValue] andBusName:ajBus];
        return [property getValue];
    }
    return nil;
}

-(id)getValueOfPropertyWithName:(NSString*)remoteProperty
{
    return [self getValueOfProperty:[self getPropertyByName:remoteProperty]];
}

-(BOOL)setProperty:(EluxRemoteProperty*)remoteProperty toValue:(id)value
{
    if(!remoteProperty)
        return NO;
    
    if([[[mAppliance  getConnectivityPlatform] getName]isEqualToString:ALL_JOYN_NAME])
    {
        NSArray* platformAdapters =[[[mAppliance getApplianceManager] getPlatformsManager]getPlatformAdapters];
        if(!platformAdapters)
            return NO;
        if(!platformAdapters.count)
            return NO;
        
        EluxConnectivityPlatformAdapter* adapter = [platformAdapters firstObject];
        NSNumber* ajPort = (NSNumber*)[mAppliance getPlatformSpecificPort];
        NSString* ajBus = (NSString*)[mAppliance getPlatformSpecificBus];
        
        return [adapter setPropertyWithName:[remoteProperty getName] inIterface:[remoteProperty getPath] inObject:[self getName] withPort:[ajPort integerValue] andBusName:ajBus toValue:value];
    }
    return NO;
}

-(BOOL)setPropertyWithName:(NSString*)property toValue:(id)value
{
    return [self setProperty:[self getPropertyByName:property] toValue:value];
}

-(void)setRemoteMethods:(NSMutableArray<EluxRemoteMethod *>*)remoteMethods
{
    mRemoteMethods = remoteMethods;
}

-(void)setRemoteProperties:(NSMutableArray<EluxRemoteProperty *>*)remoteProperties
{
    mRemoteProperties = remoteProperties;
}

-(BOOL)isSupported
{
    return YES;
}

-(BOOL)registerPropertyListener:(NSArray*)propertyNames withListener:(id<EluxPropertyChangeDelegate>)listener
{
    if(!propertyNames || !listener)
        return NO;
    if(!propertyNames.count)
        return NO;
    if(!propertyChangeListeners)
        propertyChangeListeners = [[NSMutableDictionary alloc]init];
    
    NSArray* platformAdapters = nil;
    if([[[mAppliance  getConnectivityPlatform] getName]isEqualToString:ALL_JOYN_NAME])
    {
        platformAdapters = [[[mAppliance getApplianceManager] getPlatformsManager]getPlatformAdapters];
        if(!platformAdapters)
            return NO;
        if(!platformAdapters.count)
            return NO;
    }
    else
        return NO;
    
    EluxConnectivityPlatformAdapter* adapter = [platformAdapters firstObject];
    
    BOOL ret = 0;
    for(NSString* propertyName in propertyNames)
    {
        [propertyChangeListeners setObject:listener forKey:propertyName];
        ret &= [adapter registerListener:listener forProperty:propertyName ofSubUnit:self];
    
    }
    return ret;
}

-(void)unRegisterPropertyListener:(id<EluxPropertyChangeDelegate>)listener forProperties:(NSArray*)propertyNames
{
    NSArray* platformAdapters = nil;
    if([[[mAppliance  getConnectivityPlatform] getName]isEqualToString:ALL_JOYN_NAME])
    {
        platformAdapters = [[[mAppliance getApplianceManager] getPlatformsManager]getPlatformAdapters];
        if(!platformAdapters)
            return;
        if(!platformAdapters.count)
            return;
    }
    else
        return;
    
    EluxConnectivityPlatformAdapter* adapter = [platformAdapters firstObject];
    
    NSArray* keys = [propertyChangeListeners allKeys];
    for(NSString* key in keys)
    {
        for(NSString* propertyName in propertyNames)
        {
            if([propertyName isEqualToString:key])
            {
                
                [adapter unRegisterListener:listener forProperty:propertyName ofSubUnit:self];
                 [propertyChangeListeners removeObjectForKey:propertyName];
            }
        }
    }
}

-(NSInteger)getUniqueId
{
    return mInstanceID;
}

@end

@implementation EluxApplianceInfo

-(EluxApplianceInfo*)initWithInfoDictionary:(NSDictionary*)dict
{
    if(!dict)
        return nil;
    self = [super init];
    if(self)
        mDeviceInfoDict = dict;
    return self;
}

-(NSString*)getAsString:(NSString*)key
{
    if(!key)
        return nil;
    id object = [mDeviceInfoDict objectForKey:key];
    if([object isKindOfClass:[NSString class]])
        return (NSString*)object;
    return [object description];
}

-(NSArray*)getKeys
{
    return [mDeviceInfoDict allKeys];
}

-(id)get:(NSString*)key
{
    if(!key)
        return nil;
    return [mDeviceInfoDict objectForKey:key];
}

-(NSString*)getName
{
    return [self getAsString:TAG_APPLIANCE_NAME];
}

-(NSString*)getDescription
{
    return [self getAsString:TAG_DESCRIPTION];
}

-(NSString*)getDefaultLanguage
{
    return [self getAsString:TAG_DEFAULT_LANGUAGE];
}

-(NSString*)getApplianceId
{
    return [self getAsString:TAG_APPLIANCE_ID];
}

-(NSString*)getManufacturer
{
    return [self getAsString:TAG_MANUFACTURER];
}

-(NSString*)getModelNumber
{
    return [self getAsString:TAG_MODEL_NUMBER];
}

-(NSString*)getSoftwareVersion
{
    return [self getAsString:TAG_SOFTWARE_VERSION];
}

-(NSString*)getPnc
{
    return [self getAsString:TAG_APPLIANCE_PNC];
}

-(NSString*)getElc
{
    return [self getAsString:TAG_APPLIANCE_ELC];
}

-(NSString*)getSerialNumber
{
    return [self getAsString:TAG_APPLIANCE_SERIAL];
}

@end

@implementation EluxAppliance

-(void)setApplianceInfo:(EluxApplianceInfo*)applianceInfo
{
    mApplianceInfo = applianceInfo;
}

-(void)setApplianceType:(enum EluxApplianceType)type
{
    mType = type;
}

-(void)setAppliancState:(enum EluxApplianceState)state
{
    mState = state;
}

-(void)setConnectivityPlatform:(EluxConnectivityPlatformType*)platformType
{
    mPlatformType = platformType;
}

-(EluxApplianceInfo*)getApplianceInfo
{
    return mApplianceInfo;
}

-(enum EluxApplianceType)getApplianceType
{
    return mType;
}

-(enum EluxApplianceState)getAppliancState
{
    return mState;
}

-(EluxConnectivityPlatformType*) getConnectivityPlatform
{
    return mPlatformType;
}

-(BOOL)isSupported
{
    return NO;
}

-(enum EluxApplianceType)getType
{
    return UNKNOWN;
}

-(BOOL)connect
{
    return NO;
}

-(BOOL)isConnected
{
    return NO;
}

-(BOOL)disconnect
{
    return NO;
}

-(BOOL)getRemotePropertyValue:(EluxRemoteProperty*) prop withRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener
{
    return NO;
}

-(BOOL)setRemotePropertyValue:(EluxRemoteProperty*) prop
     withRemoteValueContainer:(EluxRemoteValueContainer*)value
  andRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener
{
    return NO;
}

-(BOOL)setApplianceStateListener:(id<EluxApplianceStateDelegate>)listener
{
    return NO;
}

-(BOOL)removeApplianceStateListener:(id<EluxApplianceStateDelegate>)listener
{
    return NO;
}

-(void)setApplianceManager:(EluxApplianceManager*)applianceManager
{
    mApplianceManager = applianceManager;
}

-(void)setPlatformSpecificPort:(id)port
{
    mPlatformSpecificPort = port;
}

-(void)setPlatformSpecificBus:(id)bus
{
    mPlatformSpecificBus = bus;
}

-(id)getPlatformSpecificPort
{
    return mPlatformSpecificPort;
}

-(id)getPlatformSpecificBus
{
    return mPlatformSpecificBus;
}

-(EluxApplianceManager*)getApplianceManager
{
    return mApplianceManager;
}

-(NSArray*)getSupportedSubUnitPaths //NSArray of NSStrings
{
    return nil;
}

-(NSArray<EluxSubUnit*>*)getSubUnits
{
    return mSubUnits;
}

-(NSArray*)getRemoteMethods:(NSString*)subUnitPath
{
    return nil;
}

-(NSArray*)getRemoteProperties:(NSString*)subUnitPath
{
    return nil;
}

-(NSInteger)getUniqueId
{
    return mInstanceID;
}

-(NSString*)getUniqueIdString
{
    return muniqueIDstring;
}

-(void)setUniqueId:(NSInteger)uniqueId
{
    mInstanceID = uniqueId;
}

-(void)setUniqueIdString:(NSString*)uniqueIdString
{
    muniqueIDstring = uniqueIdString;
}

-(BOOL)invokeMethod         :(EluxRemoteMethod*)remoteMethod
               withArguments:(NSArray*)args
 andRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener
{
    return NO;
}

-(NSArray*)getMethodArgumentTypes:(EluxRemoteMethod*)remoteMethod __deprecated //return NSArray of ids
{
    return nil;
}

-(BOOL)isOnboarded
{
    return NO;
}

-(BOOL)isOnboardable
{
    return NO;
}

-(BOOL)isOffboardable
{
    return NO;
}

-(void)setSubUnits:(NSMutableArray<EluxSubUnit *> *)subUnits
{
    mSubUnits = subUnits;
}

-(BOOL)registerRemoteMessagesListeners:(NSArray<EluxApplianceMessagesDelegate> *)listeners
{
    BOOL ret = NO;
    if(!listeners)
        return NO;
    
    for(id<EluxApplianceMessagesDelegate> listener in listeners)
        ret &= [self registerRemoteMessagesListener:listener];
    return ret;
}

-(BOOL)registerRemoteMessagesListener:(id<EluxApplianceMessagesDelegate>)listener
{
    if(!listener)
        return NO;
    if(!mApplianceMessagesListeners)
        mApplianceMessagesListeners = [[NSMutableArray<EluxApplianceMessagesDelegate> alloc]init];
    [mApplianceMessagesListeners addObject:listener];
    
    if([[[self  getConnectivityPlatform] getName]isEqualToString:ALL_JOYN_NAME])
    {
        NSArray* platformAdapters =[[[self getApplianceManager] getPlatformsManager]getPlatformAdapters];
        if(!platformAdapters)
            return NO;
        if(!platformAdapters.count)
            return NO;
        
        EluxConnectivityPlatformAdapter* adapter = [platformAdapters firstObject];
        return [adapter startMessagesConsumer];
    }
    return NO;
}

-(void)unRegisterRemoteMessagesListeners:(NSArray<EluxApplianceMessagesDelegate>*)listeners
{
    if(!listeners)
        return;
    
    for(id<EluxApplianceMessagesDelegate> listener in listeners)
        [self unRegisterRemoteMessagesListener:listener];
}

-(void)unRegisterRemoteMessagesListener:(id<EluxApplianceMessagesDelegate>)listener
{
    if(!listener)
        return;
    if(!mApplianceMessagesListeners)
        return
        [mApplianceMessagesListeners removeObject:listener];
    
    if(!mApplianceMessagesListeners.count)
    {
        if([[[self  getConnectivityPlatform] getName]isEqualToString:ALL_JOYN_NAME])
        {
            NSArray* platformAdapters =[[[self getApplianceManager] getPlatformsManager]getPlatformAdapters];
            if(!platformAdapters)
                return;
            if(!platformAdapters.count)
                return;
            
            EluxConnectivityPlatformAdapter* adapter = [platformAdapters firstObject];
            [adapter stopMessagesConsumer];
        }
    }
}

-(void)passMessageToListeners:(EluxApplianceMessage*)message
{
    if(!mApplianceMessagesListeners)
        return;
    for(id<EluxApplianceMessagesDelegate> listener in mApplianceMessagesListeners)
        [listener didReceiveMessage:message FromAppliance:self];
}

@end
