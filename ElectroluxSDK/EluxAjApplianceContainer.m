//
//  EluxAjApplianceContainer.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxAjApplianceContainer.h"
#import "EluxAjAppliance.h"

@implementation EluxAjApplianceContainer

-(EluxAjApplianceContainer*)init
{
    self = [super init];
    if(self)
        mAjAppliances = [[NSMutableDictionary alloc]init];
    return self;
}

-(EluxAjAppliance*)getAjAppliance:(EluxAppliance*)appliance
{
    if(!mAjAppliances || !appliance)
        return nil;
    if(!mAjAppliances.count || ![appliance getUniqueIdString])
        return nil;
    for(EluxAjAppliance* appl in mAjAppliances)
    {
        if([appl getAppliance] == appliance)
            return appl;
    }
    return  nil;
}

-(void)addAjAppliance:(EluxAjAppliance*)appliance
{
    if(!mAjAppliances)
        mAjAppliances = [[NSMutableDictionary alloc]init];
    [mAjAppliances setObject:appliance forKey:[appliance getDeviceId]];
}


@end
