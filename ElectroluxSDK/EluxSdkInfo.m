//
//  EluxSdkInfo.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxSdkInfo.h"
#import "EluxConnectivityPlatformType.h"
#import "../Libs/AllJoyn/Include/AJNVersion.h"

const NSString* EluxSDKVersion = @"0.0.8";

#ifdef DEBUG
const NSString* EluxSDKBuildConfiguration = @"-DEBUG";
#else
const NSString* EluxSDKBuildConfiguration = @"-RELEASE";
#endif

#define LONG_VERSION_NUMBER PPCAT( VERSION_NUMNBER , BUILD_CONFIGURATION )


@implementation EluxSdkInfo


+(NSString*)getAllJoynVersion
{
    return [AJNVersion versionInformation];
}

+(NSString*)getSdkVersion
{
    return  [NSString stringWithFormat:@"%@%@", EluxSDKVersion, EluxSDKBuildConfiguration];
}

+(BOOL)isDebugBuild
{
#ifdef DEBUG
    return YES;
#else
    return NO;
#endif
}


+(NSArray*)getSupportedPlatforms
{
    return [EluxConnectivityPlatformType values];
}


@end
