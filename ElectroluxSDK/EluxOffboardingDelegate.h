//
//  EluxOffboardingDelegate.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

@class EluxAppliance;

@protocol EluxOffboardingDelegate <NSObject>

/**
* Called on appliance off-boarding process finished successfully
*/
-(void)onApplianceOffboardingSucceeded:(EluxAppliance*) appliance;

/**
* * Called on appliance off-boarding process failed
*
* @param error Error specifying, what went wrong.
*/
-(void)onApplianceOffboardingFailed:(NSError*) error;

@end
