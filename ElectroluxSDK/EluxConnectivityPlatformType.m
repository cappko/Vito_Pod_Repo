//
//  EluxConnectivityPlatformType.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxConnectivityPlatformType.h"

@implementation EluxConnectivityPlatformType


-(EluxConnectivityPlatformType*)initWithPlatformType:(NSString*)name andFilter:(NSArray*)onboardingFilter
{
    EluxConnectivityPlatformType* tmp = [super init];
    if(!tmp)
        return nil;
    tmp->mName = name;
    tmp->mDiscoveryFilter = onboardingFilter;
    return tmp;
}

-(NSString*)getName{ return mName; };
-(NSArray*)getDiscoveryFilter{ return mDiscoveryFilter; };

+(NSArray*)values
{
    EluxConnectivityPlatformType* ALLJOYN = [[EluxConnectivityPlatformType alloc] initWithPlatformType:ALL_JOYN_NAME andFilter:[NSArray arrayWithObjects: @"AJ_", @"_AJ", nil]];
    EluxConnectivityPlatformType* HACLv4  = [[EluxConnectivityPlatformType alloc] initWithPlatformType:HACLv4_NAME andFilter:[NSArray arrayWithObjects: @"", nil]];
    if(!ALLJOYN || !HACLv4)
        return nil;
    return [NSArray arrayWithObjects:ALLJOYN, HACLv4, nil];
}

+(EluxConnectivityPlatformType*)getPlatformTypeForString:(NSString*)platformString
{
    NSArray* arr = [EluxConnectivityPlatformType values];
    for(int i = 0; i < arr.count; i++)
    {
        EluxConnectivityPlatformType* platformType = [arr objectAtIndex:i];
        if([[platformType getName]isEqualToString:platformString])
            return platformType;
    }
    return nil;
}

+(EluxConnectivityPlatformType*)allJoyn
{
    return [EluxConnectivityPlatformType getPlatformTypeForString:ALL_JOYN_NAME];
}
+(EluxConnectivityPlatformType*)HACLv4
{
    return [EluxConnectivityPlatformType getPlatformTypeForString:HACLv4_NAME];
}

@end
