//
//  EluxAjDataConverter.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxAjDataConverter.h"
#import "EluxRemoteValueContainer.h"
#import "EluxAppliance.h"
#import <alljoyn/samples_common/AJSCAboutDataConverter.h>

@implementation EluxAjDataConverter

+ (NSMutableDictionary *)propertyDataArgToDict:(AJNMessageArgument *)propertyDataArg
{
    if(!propertyDataArg)
        return nil;
    size_t dataSize = 0;
    ajn::MsgArg *msgArg;
    QStatus status = [propertyDataArg value:@"a{sv}", &dataSize, &msgArg];
    
    if(status != ER_OK)
        return nil;
    
    char *key = NULL;
    ajn::MsgArg *value = NULL;
    
    NSMutableDictionary *aboutDataDict = [[NSMutableDictionary alloc]init];
    for(int i = 0; i < dataSize; ++i)
    {
        msgArg[i].Get("{sv}", &key, &value);
        NSString *keyNS = [NSString stringWithCString:key encoding:NSUTF8StringEncoding];
        
//        NSLog(@"value->Signature(): %s for key: %@", value->Signature().c_str(), keyNS);
/*
        kAJNTypeInvalid          =  0,     ///< AllJoyn INVALID typeId
        kAJNTypeArray            = 'a',    ///< AllJoyn array container type
        kAJNTypeBoolean          = 'b',    ///< AllJoyn boolean basic type, @c 0 is @c FALSE and @c 1 is @c TRUE - Everything else is invalid
        kAJNTypeDouble           = 'd',    ///< AllJoyn IEEE 754 double basic type
        kAJNTypeDictionaryEntry  = 'e',    ///< AllJoyn dictionary or map container type - an array of key-value pairs
        kAJNTypeSignature        = 'g',    ///< AllJoyn signature basic type
        kAJNTypeHandle           = 'h',    ///< AllJoyn socket handle basic type
        kAJNTypeInt32            = 'i',    ///< AllJoyn 32-bit signed integer basic type
        kAJNTypeInt16            = 'n',    ///< AllJoyn 16-bit signed integer basic type
        kAJNTypeObjectPath       = 'o',    ///< AllJoyn Name of an AllJoyn object instance basic type
        kAJNTypeUInt16           = 'q',    ///< AllJoyn 16-bit unsigned integer basic type
        kAJNTypeStruct           = 'r',    ///< AllJoyn struct container type
        kAJNTypeString           = 's',    ///< AllJoyn UTF-8 NULL terminated string basic type
        kAJNTypeUInt64           = 't',    ///< AllJoyn 64-bit unsigned integer basic type
        kAJNTypeUInt32           = 'u',    ///< AllJoyn 32-bit unsigned integer basic type
        kAJNTypeVariant          = 'v',    ///< AllJoyn variant container type
        kAJNTypeInt64            = 'x',    ///< AllJoyn 64-bit signed integer basic type
        kAJNTypeByte             = 'y',    ///< AllJoyn 8-bit unsigned integer basic type
*/
        if(value->Signature() == "x")
        {
            int64_t unwrappedVal = 0;
            status = value->Get("x", &unwrappedVal);
            
            if(status == ER_OK)
                [aboutDataDict setObject:[NSString stringWithFormat:@"%d%@%lld", eluxPropertyValueTypeInt64, ELUX_VALUE_AND_TYPE_SEPARATOR, unwrappedVal] forKey:keyNS];
        }
        else if(value->Signature() == "u")
        {
            uint32_t unwrappedVal = 0;
            status = value->Get("u", &unwrappedVal);
            
            if(status == ER_OK)
                [aboutDataDict setObject:[NSString stringWithFormat:@"%d%@%d", eluxPropertyValueTypeUInt32, ELUX_VALUE_AND_TYPE_SEPARATOR, unwrappedVal] forKey:keyNS];
        }
        else if(value->Signature() == "t")
        {
            uint64_t unwrappedVal = 0;
            status = value->Get("t", &unwrappedVal);
            
            if(status == ER_OK)
                [aboutDataDict setObject:[NSString stringWithFormat:@"%d%@%llu", eluxPropertyValueTypeUInt64, ELUX_VALUE_AND_TYPE_SEPARATOR, unwrappedVal] forKey:keyNS];
        }
        else if(value->Signature() == "n")
        {
            int16_t unwrappedVal = 0;
            status = value->Get("n", &unwrappedVal);
            
            if(status == ER_OK)
                [aboutDataDict setObject:[NSString stringWithFormat:@"%d%@%d", eluxPropertyValueTypeInt16, ELUX_VALUE_AND_TYPE_SEPARATOR, unwrappedVal] forKey:keyNS];
        }
        else if(value->Signature() == "i")
        {
            int32_t unwrappedVal = 0;
            status = value->Get("i", &unwrappedVal);
            
            if(status == ER_OK)
                [aboutDataDict setObject:[NSString stringWithFormat:@"%d%@%d", eluxPropertyValueTypeInt32, ELUX_VALUE_AND_TYPE_SEPARATOR, unwrappedVal] forKey:keyNS];
        }
        else if(value->Signature() == "b")
        {
            BOOL unwrappedVal = NO;
            status = value->Get("b", &unwrappedVal);
            
            if(status == ER_OK)
                [aboutDataDict setObject:[NSString stringWithFormat:@"%d%@%d", eluxPropertyValueTypeBool, ELUX_VALUE_AND_TYPE_SEPARATOR, unwrappedVal] forKey:keyNS];
        }
        else if(value->Signature() == "q")
        {
            uint16_t unwrappedVal = 0;
            status = value->Get("q", &unwrappedVal);
            
            if(status == ER_OK)
                [aboutDataDict setObject:[NSString stringWithFormat:@"%d%@%d", eluxPropertyValueTypeUInt16, ELUX_VALUE_AND_TYPE_SEPARATOR, unwrappedVal] forKey:keyNS];
        }
        else if(value->Signature() == "y")
        {
            uint8_t unwrappedVal = 0;
            status = value->Get("y", &unwrappedVal);
            
            if(status == ER_OK)
                [aboutDataDict setObject:[NSString stringWithFormat:@"%d%@%d", eluxPropertyValueTypeUInt8, ELUX_VALUE_AND_TYPE_SEPARATOR, unwrappedVal] forKey:keyNS];
        }
        else if(value->Signature() == "s")
        {
            char *unwrappedVal;
            status = value->Get("s", &unwrappedVal);
            
            if(status == ER_OK)
                [aboutDataDict setObject:[NSString stringWithCString:(unwrappedVal ? unwrappedVal : "") encoding:NSUTF8StringEncoding] forKey:keyNS];
        }
        else if (value->Signature() == "ay")
        {
            uint8_t *appID;
            size_t appIDSize;
            
            status = value->Get("ay", &appIDSize, &appID);
            
            if(status == ER_OK)
            {
                NSMutableData *argData = [NSMutableData dataWithBytes:appID length:(NSInteger)appIDSize];
                NSString *trimmedArray = [[argData description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
                [aboutDataDict setObject:trimmedArray forKey:keyNS];
            }
            
        }
        else if (value->Signature() == "as")
        {
            ajn::MsgArg *strings;
            size_t numOfString;
            
            status = value->Get("as", &numOfString, &strings);
            
            if(status == ER_OK)
            {
                NSMutableArray *stringArrayNS = [[NSMutableArray alloc] init];
                for (int i = 0; i < numOfString; ++i)
                {
                    char *tmpString;
                    strings[i].Get("s", &tmpString);
                    [stringArrayNS addObject:[NSString stringWithCString:tmpString encoding:NSUTF8StringEncoding]];
                }
                [aboutDataDict setObject:stringArrayNS forKey:keyNS];
            }
        }
    }
    return aboutDataDict;
}

+(id)getEluxPropertyValueFromAjString:(NSString*)ajPropertyValueString andType:(eluxPropertyType*)type
{
    if(!ajPropertyValueString)
        return nil;
    if(!ajPropertyValueString.length)
        return nil;
    NSArray* arr = [ajPropertyValueString componentsSeparatedByString:ELUX_VALUE_AND_TYPE_SEPARATOR];
    if(!arr)
        return ajPropertyValueString;
    if(arr.count < 2)
        return ajPropertyValueString;
    
    NSString* value = [arr objectAtIndex:1];
    
    *type = [EluxAjDataConverter getEluxPropertyValueTypeFromAjString:ajPropertyValueString];
    if(*type <= eluxPropertyValueTypeInt64)
        return [NSNumber numberWithInteger:[value integerValue]];
    
    if(*type == eluxPropertyValueTypeBool)
        return [NSNumber numberWithBool:[value boolValue]];

    return nil;
}

+(eluxPropertyType)getEluxPropertyValueTypeFromAjString:(NSString*)ajPropertyValueString
{
    if(!ajPropertyValueString)
        return eluxPropertyValueTypeIvalid;
    if(!ajPropertyValueString.length)
        return eluxPropertyValueTypeIvalid;
    NSArray* arr = [ajPropertyValueString componentsSeparatedByString:ELUX_VALUE_AND_TYPE_SEPARATOR];
    if(!arr)
        return eluxPropertyValueTypeUTF8String;
    if(arr.count < 2)
        return eluxPropertyValueTypeUTF8String;
    
    NSString* type = [arr firstObject];
    return (eluxPropertyType)[type integerValue];
}

+(NSString*)getAJDataTypeFromAjString:(NSString*)ajPropertyValueString
{
    eluxPropertyType type = [EluxAjDataConverter getEluxPropertyValueTypeFromAjString:ajPropertyValueString];
    
    switch (type)
    {
        case eluxPropertyValueTypeIvalid:
            return nil;
            
        case eluxPropertyValueTypeUInt8:
            return @"y";
        
        case eluxPropertyValueTypeUInt16:
            return @"q";
            
        case eluxPropertyValueTypeInt16:
            return @"n";
            
        case eluxPropertyValueTypeUInt32:
            return @"u";
            
        case eluxPropertyValueTypeInt32:
            return @"i";
            
        case eluxPropertyValueTypeUInt64:
            return @"t";
            
        case eluxPropertyValueTypeInt64:
            return @"x";
            
        case eluxPropertyValueTypeBool:
            return @"b";
            
        case eluxPropertyValueTypeUTF8String:
            return @"s";
        default:
            break;
    }
    return nil;
}

@end
