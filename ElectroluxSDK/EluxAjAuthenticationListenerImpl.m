//
//  EluxAjAuthenticationListenerImpl.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxAjAuthenticationListenerImpl.h"

@implementation EluxAjAuthenticationListenerImpl

@synthesize defaultPasscode;

- (AJNSecurityCredentials *)requestSecurityCredentialsWithAuthenticationMechanism:(NSString *)authenticationMechanism peerName:(NSString *)peerName authenticationCount:(uint16_t) authenticationCount userName:(NSString *)userName credentialTypeMask:(AJNSecurityCredentialType)mask
{
    NSLog(@"requestSecurityCredentialsWithAuthenticationMechanism:%@ forRemotePeer%@ userName:%@", authenticationMechanism, peerName, userName);
    
    AJNSecurityCredentials* credentials = [[AJNSecurityCredentials alloc]init];
    if (mask & kAJNSecurityCredentialTypePassword)
        credentials.password = defaultPasscode ? defaultPasscode : @"00000";
    return credentials;
}

- (void)authenticationUsing:(NSString *)authenticationMechanism forRemotePeer:(NSString *)peer didCompleteWithStatus:(BOOL)success
{
    if(success)
        NSLog(@"Authentication %@ for: %@ was successfull", authenticationMechanism, peer);
    else
        NSLog(@"Authentication %@ for: %@ failed", authenticationMechanism, peer);
}

- (BOOL)verifySecurityCredentials:(AJNSecurityCredentials *)credentials usingAuthenticationMechanism:(NSString *)authenticationMechanism forRemotePeer:(NSString *)peerName
{
    return NO;
}

- (void)securityViolationOccurredWithErrorCode:(QStatus)errorCode forMessage:(AJNMessage *)message
{
//    NSString* msg = message.errorName;
//    if(msg)
//        NSLog(@"error: %@", msg);
    
    NSLog(@"Decryption failed!!!");
    NSString* msg = message.errorDescription;
    if(msg)
        NSLog(@"error: %@", msg);
    msg = message.description;
    if(msg)
        NSLog(@"error: %@", msg);
}

@end
