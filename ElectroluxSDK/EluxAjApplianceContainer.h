//
//  EluxAjApplianceContainer.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>
#import "EluxAppliance.h"

@class EluxAjAppliance;

@interface EluxAjApplianceContainer : NSObject
{
    NSMutableDictionary* mAjAppliances;
}


-(EluxAjAppliance*)getAjAppliance:(EluxAppliance*)appliance;
-(void)addAjAppliance:(EluxAjAppliance*)appliance;

@end
