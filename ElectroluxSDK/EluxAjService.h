//
//  EluxAjService.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#ifndef QCC_OS_GROUP_POSIX
#define QCC_OS_GROUP_POSIX
#endif

#import <Foundation/Foundation.h>
#import "EluxApplianceWifiScanDelegate.h"
#import "EluxAjEventHandler.h"
#import "EluxAjApplianceContainer.h"
#import "EluxPlatformConnectionStateDelegate.h"
#import "EluxApplianceDiscoveryDelegate.h"
#import "EluxAjAuthenticationListenerImpl.h"
//#import "EluxAjPropertyChangeListener.h"

#import <AJNBusListener.h>
#import <AJNAboutListener.h>
#import <AJNSessionListener.h>
#import <alljoyn/onboarding/AJOBSOnboardingClient.h>
#import <alljoyn/samples_common/AJSCClientInformation.h>
#import <AJNObserver.h>

static NSString *const AJ_AP_PREFIX = @"AJ_";
static NSString *const AJ_AP_SUFFIX = @"_AJ";


@protocol EluxApplianceDiscoveryDelegate;
@protocol EluxOnboardingDelegate;
@protocol EluxOffboardingDelegate;
@class EluxSSIDInfo;
@class EluxAjPropertyChangeListener;
@class EluxApplianceManager;

@interface EluxAjService : NSObject <AJNBusListener, AJNAboutListener, AJNSessionListener, AJOBOnboardingClientListener, AJNSessionPortListener, AJNObserverListener>
{
    NSMutableArray* mOnboardeeNetworks;
    NSMutableDictionary* mClientInformationDict;
//    NSMutableDictionary* mServiceSessionDict;
    NSMutableDictionary* mDeviceSessionDict;
    AJOBSOnboardingClient* mOnboardingClient;
    AJNSessionId mSessionId;
    AJNBusAttachment* mClientBusAttachment;
    AJSCClientInformation *mClientInformation;
    EluxAjAuthenticationListenerImpl* mAuthenticationListenerImpl;
    NSString* TAG;
    NSString *mOnboardeeBus;
    NSString* mRealmBusName;
    NSString* mCurrentSSID;
    NSString* mTargetSSID;
    EluxAjEventHandler* mAjEventHandler;
    EluxAjPropertyChangeListener* mAjPropertyChangeListener;
    EluxAjApplianceContainer* mAjDeviceContainer;
    id<EluxPlatformConnectionStateDelegate> mConnectionStateListener;
    id<EluxApplianceDiscoveryDelegate> mApplianceDiscoveryListener;
    id<EluxApplianceWifiScanDelegate> mEluxApplianceWifiScanDelegate;
    id<EluxOnboardingDelegate> mEluxOnboardingDelegate;
    id<EluxOffboardingDelegate> mEluxOffboardingDelegate;
    
    NSInteger mTaskId;
    dispatch_queue_t mCreationQueue;
    dispatch_queue_t mFoundAppliancesQueue;
    
    BOOL mIsWaitingForOnboardee;
    BOOL mIsAboutClientConnected;
    
    AJNObserver* doorObserver;
    AJNProxyBusObject* tempProxy;
    NSMutableArray* mObservedObjects;
    EluxApplianceManager* mApplianceManager;
}

-(void)wifiScan:(id<EluxApplianceWifiScanDelegate>)listener;
-(void)wifiScan:(EluxAppliance*)appliance withWifiScanListener:(id<EluxApplianceWifiScanDelegate>)listener;
-(void)onboard:(EluxAppliance*)appliance toSSID:(EluxSSIDInfo*)ssid withListener:(id<EluxOnboardingDelegate>)listener;
-(void)offboard:(EluxAppliance*)appliance withListener:(id<EluxOffboardingDelegate>)listener;
-(void)searchForAvailableAppliances:(id<EluxApplianceDiscoveryDelegate>)listener;
-(EluxRemoteProperty*)getPropertyWithName:(NSString*)name inIterface:(NSString*)interfaceName inObject:(NSString*)objectPath withPort:(NSInteger)portNumber andBusName:(NSString*)busName;
-(BOOL)setPropertyWithName:(NSString*)name inIterface:(NSString*)interfaceName inObject:(NSString*)objectPath withPort:(NSInteger)portNumber andBusName:(NSString*)busName toValue:(NSString*)value;
-(BOOL)startAjEventsConsumer;
-(void)stopAjEventsConsumer;
-(BOOL)registerListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit;
-(void)unRegisterListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit;

@end
