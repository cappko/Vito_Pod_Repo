//
//  EluxOnboardingManager.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxOnboardingManager.h"
#import "EluxPlatformsManager.h"
#import "EluxOnboardableDiscoveryManager.h"
#import "EluxIskWifiManager.h"
#import "EluxOnboardableApplianceDiscoveryDelegate.h"
#import "EluxAppliance.h"
#import "EluxOnboardingDelegate.h"
#import "EluxPlatformApplianceProtocol.h"
#import "EluxConnectivityPlatformAdapter.h"
#import "EluxErrors.h"
#import "EluxErrorCodes.h"
#import "EluxApplianceWifiScanDelegate.h"
#import "EluxOffboardingDelegate.h"
#import "EluxGenericWifiScan.h"
#import "EluxSSIDInfo.h"

#define EOM_DELAY (30*100)

@implementation EluxOnboardingManager

-(EluxOnboardingManager*)init
{
    NSLog(@"Unsupported initialization");
    return nil;
}

+(EluxOnboardingManager*)sharedInstanceWithPlatformManager:(EluxPlatformsManager*)pm
{
    static EluxOnboardingManager* sharedInstance = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedInstance = [[EluxOnboardingManager alloc] initWithPlatformManager:pm];
    });
    return sharedInstance;
}



/**
 * Create an Onboarding Manager to on-board and off-board appliances
 *
 * @param pm {@link PlatformsManager}
 */
-(EluxOnboardingManager*)initWithPlatformManager:(EluxPlatformsManager*)pm
{
    self = [super init];
    if(self)
    {
        TAG = NSStringFromClass([EluxPlatformsManager class]);
        mPlatformsManager = pm;
        mDiscoveryManager = [[EluxOnboardableDiscoveryManager alloc]init];
    }
    return self;
}

/**
 * Starts discovery for available (reachable) appliances, that could be on-boarded
 *
 * @param listener Listener where discovery results are reported
 */
-(void)startOnboardableApplianceDiscovery:(id<EluxOnboardableApplianceDiscoveryDelegate>)listener
{
    if(mDiscoveryManager)
    {
        [mDiscoveryManager setListener:listener];
        [mDiscoveryManager startDiscovery];
    }
}

/**
 * Stops the discovery manager
 */
-(void)startOnboardableApplianceDiscovery
{
    if(mDiscoveryManager)
        [mDiscoveryManager tearDown];
}

/**
 * Forces the discovery of the onboardable appliances
 */
-(void)forceOnboardableApplianceDiscovery
{
    if(mDiscoveryManager)
        [mDiscoveryManager forceDiscovery];
}

/**
 * Start on-boarding process for a given discovered appliance
 *
 * @param appliance Appliance to be onboarded (obtained from onboardable device discovery, see @link{startOnboardableApplianceDiscovery}  )
 * @param netInfo Network information needed for on-boarding. For Wifi connected appliances, this will be @link{WifiInfo}
 * @param appliancePassword Appliance password for authentication (don't confuse with network password passed in netInfo)
 * @param listener {@link IOnboardingListener} to notify on-boarding related events
 * @return True, if the on-boarding process was started successfully, false otherwise. Note, that returning true does not mean onboarding itself succeeded (@see{IOnboardingListener})
 */
-(BOOL)onboardAppliance:(EluxAppliance*)appliance
  withAppliancePassword:(NSString*)appliancePassword
            andListener:(id<EluxOnboardingDelegate>)listener
{
    return [self onboard:appliance withAppliancePassword:appliancePassword andListener:listener];
}

/**
 * Helper function that onboards appliances
 *
 * @param appliance {@link Appliance} to onboard
 * @param netInfo {@link GenericWifiScan} Network to provide to the appliance
 * @param appliancePassword Network password
 * @param listener {@link IOnboardingListener} listener to notify onboarding events
 * @return true if request send successfully, false otherwise
 */
-(BOOL)onboard         :(EluxAppliance*)appliance
  withAppliancePassword:(NSString*)appliancePassword
            andListener:(id<EluxOnboardingDelegate>)listener
{
    if([appliance isOnboardable])
    {
        /* get the Platform Adapter and onboard the appliance using the technology provided by
         * the adapter */
        EluxConnectivityPlatformType* type = [appliance getConnectivityPlatform];
        NSInteger index = [mPlatformsManager getPlatformIndex:type];
        EluxConnectivityPlatformAdapter* pa = [[mPlatformsManager getPlatformAdapters]objectAtIndex:index];
        
        [pa onboard:appliance withWifiScanner:nil appliancePassword:appliancePassword andOnboardingListener:listener];
        
        [self completeOnboarding:appliance withWifiScan:nil andOnboardingDelegate:listener];
        return YES;
    }
    else
        [self sendOnboardingFailed:listener withError:nil];
    return NO;
}

/**
 * Helper method that sends to the listener an onboarding message failure
 *
 * @param listener {@link IOnboardingListener} listener to invoke
 * @param message Failure message to deliver
 */
-(void)sendOnboardingFailed:(id<EluxOnboardingDelegate>)listener withError:(NSError*)error
{
    [listener onApplianceOnboardingFailed:error];
}

/**
 * Helper method that sends off-boarding failure message to listener
 *
 * @param listener {@link IOffboardingListener} listener to invoke
 * @param message Failure message to deliver
 */
-(void)sendOffboardingFailed:(id<EluxOffboardingDelegate>)listener withError:(NSError*)error
{
    [listener onApplianceOffboardingFailed:error];
}

/**
 * Helper method that sends on-boarding success message to listener
 *
 * @param listener {@link IOffboardingListener} listener to invoke
 * @param appliance {@link Appliance} that has been off-boarded
 */
-(void)sendOffboardingSuccess:(id<EluxOffboardingDelegate>)listener withAppliance:(EluxAppliance*)appliance
{
    [listener onApplianceOffboardingSucceeded:appliance];
}

-(void)completeOnboarding:(EluxAppliance*)appliance
             withWifiScan:(EluxGenericWifiScan*)targetNet
    andOnboardingDelegate:(id<EluxOnboardingDelegate>)listener
{
    
}


/**
 * Start off-boarding process for a given appliance
 *
 * @param appliance Appliance to be off-boarded
 * @param listener {@link IOffboardingListener} to notify off-boarding related events
 * @return True if the onboarding process was started successful, false otherwise
 */
-(BOOL)offboardAppliance:(EluxAppliance*)appliance withOffboardingDelegate:(id<EluxOffboardingDelegate>)listener
{
    if([appliance isOnboarded])
    {
        EluxConnectivityPlatformType* type = [appliance getConnectivityPlatform];
        NSInteger index = [mPlatformsManager getPlatformIndex:type];
        EluxConnectivityPlatformAdapter* pa = [[mPlatformsManager getPlatformAdapters]objectAtIndex:index];
        
        [pa offboard:appliance withOffboardingListener:listener];
        
        [self sendOffboardingSuccess:listener withAppliance:appliance];
        return YES;
    }
    else
        [self sendOffboardingFailed:listener withError:nil];
    
    return false;
}


/**
 * Start on-boarding process for an appliance we are connected to
 *
 * @param appliance Appliance to be on-boarded
 * @param SSID SSID of the WiFi network to which the appliance is about to on board
 * @param password Password to the WiFi specified by SSID
 * @return True if the onboarding process was started successful, false otherwise
 */
-(BOOL)onboardAppliance:(EluxAppliance*)appliance
                 toSSID:(NSString*)SSID
       withSSIDPassword:(NSString*)password
            andListener:(id<EluxOnboardingDelegate>)listener
{
    if([appliance isOnboardable])
    {
        /* get the Platform Adapter and onboard the appliance using the technology provided by
         * the adapter */
        EluxConnectivityPlatformType* type = [appliance getConnectivityPlatform];
        NSInteger index = [mPlatformsManager getPlatformIndex:type];
        EluxConnectivityPlatformAdapter* pa = [[mPlatformsManager getPlatformAdapters]objectAtIndex:index];
        
        [pa onboard:appliance withWifiScanner:nil appliancePassword:password andOnboardingListener:listener];
        [self completeOnboarding:appliance withWifiScan:nil andOnboardingDelegate:listener];
        return YES;
    }
    else
        [self sendOnboardingFailed:listener withError:nil];
    return NO;
}

/**
 Tries to find all SSIDs that are available to the appliance that is about to be onboarded
 
 @param listener An instance of class that conforms to EluxApplianceWifiScanDelegate protocol. The listener receives a list of available SSIDs or a particullar NSError.
 @return Returns YES if the process of searching successfully started, NO otherwise.
 */
-(BOOL)searchForAvailableSSID:(id<EluxApplianceWifiScanDelegate>)listener
{
    if(!listener)
        return NO;
       
    mWifiScanDeelgate = listener;
    
    NSString* SSID = [EluxGenericWifiScan getCurrentSSID];
    if(!SSID)
    {
        [listener listOfAvailableSSIDsFailedWithError:[EluxErrors createSimpleErrorWithCode:ERR_ONBOARDING_NO_SSID andRerrorMEssage:ERR_ONBOARDING_NO_SSID_STRING]];
        return NO;
    }

    if(!mPlatformsManager)
    {
        [listener listOfAvailableSSIDsFailedWithError:[EluxErrors createSimpleErrorWithCode:ERR_BAD_INITIALIZATION andRerrorMEssage:ERR_BAD_INITIALIZATION_STRING]];
        return NO;
    }
    
    if(![mPlatformsManager isCurrentSSIDApplienceAP])
    {
        [listener listOfAvailableSSIDsFailedWithError:[EluxErrors createSimpleErrorWithCode:ERR_ONBOARDING_WRONG_SSID andRerrorMEssage:ERR_ONBOARDING_WRONG_SSID_STRING]];
        return NO;
    }

    NSInteger index = [mPlatformsManager getPlatformIndexForSSID:SSID];
    if(index < 0)
    {
        [listener listOfAvailableSSIDsFailedWithError:[EluxErrors createSimpleErrorWithCode:ERR_ONBOARDING_UNSUPPORTED_PLATFORM andRerrorMEssage:ERR_ONBOARDING_UNSUPPORTED_PLATFORM_STRING]];
        return NO;
    }
    EluxConnectivityPlatformAdapter* pa = [[mPlatformsManager getPlatformAdapters]objectAtIndex:index];
    if(!pa)
    {
        [listener listOfAvailableSSIDsFailedWithError:[EluxErrors createSimpleErrorWithCode:ERR_ONBOARDING_NULL_PLATFORM_ADAPTER andRerrorMEssage:ERR_ONBOARDING_NULL_PLATFORM_ADAPTER_STRING]];
        return NO;
    }
    
    NSLog(@"Using %@ as platform adapter", [[pa class] description]);
    [pa wifiScan:nil withApplianceWifiScanDelegate:self];
    return YES;
}

-(void)listOfAvailableSSIDsReceived:(NSArray*)availableSSIDs
{
    if(mWifiScanDeelgate)
        [mWifiScanDeelgate listOfAvailableSSIDsReceived:availableSSIDs];
}

-(void)listOfAvailableSSIDsFailedWithError:(NSError*)error
{
    if(mWifiScanDeelgate)
        [mWifiScanDeelgate listOfAvailableSSIDsFailedWithError:error];
}

-(BOOL)onboardAppliance:(EluxAppliance*)appliance toSSID:(EluxSSIDInfo*)ssidInfo withListener:(id<EluxOnboardingDelegate>)listener
{
    if(!listener)
        return NO;
    
    mOnboardingDelegate = listener;
    
    NSString* SSID = [EluxGenericWifiScan getCurrentSSID];
    if(!SSID)
    {
        [listener onApplianceOnboardingFailed:[EluxErrors createSimpleErrorWithCode:ERR_ONBOARDING_NO_SSID andRerrorMEssage:ERR_ONBOARDING_NO_SSID_STRING]];
        return NO;
    }
    
    if(!mPlatformsManager)
    {
        [listener onApplianceOnboardingFailed:[EluxErrors createSimpleErrorWithCode:ERR_BAD_INITIALIZATION andRerrorMEssage:ERR_BAD_INITIALIZATION_STRING]];
        return NO;
    }
    
    if(![mPlatformsManager isCurrentSSIDApplienceAP])
    {
        [listener onApplianceOnboardingFailed:[EluxErrors createSimpleErrorWithCode:ERR_ONBOARDING_WRONG_SSID andRerrorMEssage:ERR_ONBOARDING_WRONG_SSID_STRING]];
        return NO;
    }
    
    NSInteger index = [mPlatformsManager getPlatformIndexForSSID:SSID];
    EluxConnectivityPlatformAdapter* pa = [[mPlatformsManager getPlatformAdapters]objectAtIndex:index];
    if(!pa)
    {
        [listener onApplianceOnboardingFailed:[EluxErrors createSimpleErrorWithCode:ERR_ONBOARDING_NULL_PLATFORM_ADAPTER andRerrorMEssage:ERR_ONBOARDING_NULL_PLATFORM_ADAPTER_STRING]];
        return NO;
    }
    
    [pa onboard:appliance toSSID:ssidInfo withPassword:@"" andOnboardingListener:listener];
    return YES;
}

-(BOOL)offboardAppliance:(EluxAppliance*)appliance withListener:(id<EluxOffboardingDelegate>)listener
{
    if(!listener)
        return NO;
    
    mOffboardingDelegate = listener;
    
    NSString* SSID = [EluxGenericWifiScan getCurrentSSID];
    if(!SSID)
    {
        [listener onApplianceOffboardingFailed:[EluxErrors createSimpleErrorWithCode:ERR_OFFBOARDING_NO_SSID andRerrorMEssage:ERR_OFFBOARDING_NO_SSID_STRING]];
        return NO;
    }
    
    if(!mPlatformsManager)
    {
        [listener onApplianceOffboardingFailed:[EluxErrors createSimpleErrorWithCode:ERR_BAD_INITIALIZATION andRerrorMEssage:ERR_BAD_INITIALIZATION_STRING]];
        return NO;
    }
    
    NSInteger index = [mPlatformsManager getPlatformIndexForSSID:SSID];
    EluxConnectivityPlatformAdapter* pa = [[mPlatformsManager getPlatformAdapters]objectAtIndex:index];
    if(!pa)
    {
        [listener onApplianceOffboardingFailed:[EluxErrors createSimpleErrorWithCode:ERR_OFFBOARDING_NULL_PLATFORM_ADAPTER andRerrorMEssage:ERR_OFFBOARDING_NULL_PLATFORM_ADAPTER_STRING]];
        return NO;
    }
    
    [pa offboard:appliance withOffboardingListener:listener];
    return YES;
}

-(void)setPassword:(NSString*)password forSSID:(EluxSSIDInfo*)ssid
{
    ssid.password = password;
}

@end
