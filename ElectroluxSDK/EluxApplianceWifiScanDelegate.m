//
//  EluxApplianceWifiScanDelegate.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxApplianceWifiScanDelegate.h"


#ifdef NOT_PROTOCOL_ONLY
@implementation EluxApplianceWifiScanListener

@end
#endif //NOT_PROTOCOL_ONLY
