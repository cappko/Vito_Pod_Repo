//
//  EluxAjAuthenticationListenerImpl.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//


#import <Foundation/Foundation.h>
#import "AJNAuthenticationListener.h"//<alljoyn/samples_common/AJSCAuthenticationListenerImpl.h>

@interface EluxAjAuthenticationListenerImpl : NSObject <AJNAuthenticationListener>

@property(nonatomic, retain)NSString* defaultPasscode;

- (AJNSecurityCredentials *)requestSecurityCredentialsWithAuthenticationMechanism:(NSString *)authenticationMechanism peerName:(NSString *)peerName authenticationCount:(uint16_t) authenticationCount userName:(NSString *)userName credentialTypeMask:(AJNSecurityCredentialType)mask;

- (void)authenticationUsing:(NSString *)authenticationMechanism forRemotePeer:(NSString *)peer didCompleteWithStatus:(BOOL)success;

- (BOOL)verifySecurityCredentials:(AJNSecurityCredentials *)credentials usingAuthenticationMechanism:(NSString *)authenticationMechanism forRemotePeer:(NSString *)peerName;

- (void)securityViolationOccurredWithErrorCode:(QStatus)errorCode forMessage:(AJNMessage *)message;

@end
