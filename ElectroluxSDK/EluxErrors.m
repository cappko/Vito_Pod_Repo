//
//  AppliancesViewController.m
//  simple-app
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxErrors.h"
#import "EluxErrorCodes.h"

@implementation EluxErrors

+(NSError*)createSimpleErrorWithCode:(NSInteger)errorCode andRerrorMEssage:(NSString*)message
{
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    NSString* str = [NSString stringWithFormat:@"Error: %ld, message", (long)errorCode];
    [details setValue:str forKey:NSLocalizedDescriptionKey];
    [details setValue:str forKey:NSLocalizedFailureReasonErrorKey];
    NSError* error = [NSError errorWithDomain:@"ElectroluxSDK" code:errorCode userInfo:details];
    return error;
}

@end
