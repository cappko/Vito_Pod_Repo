//
//  EluxApplianceDiscoveryDelegate.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>
#import "EluxCredentialsContainer.h"

@class EluxAppliance;

@protocol EluxApplianceDiscoveryDelegate <NSObject>
/**
 * Called on device discovered
 *
 * @param appliance discovered appliance
 */
-(void)onApplianceDiscovered:(EluxAppliance*) appliance;

/**
 * Called on device lost (can not be accessed or (re) connected anymore).
 *
 * This is typically called if you disconnect from network, where device is onboarded
 *
 * @param appliance The lost appliance
 * @param stillVisibleAppliances List of appliances, that are currently accessible/visible
 */
-(void)onApplianceLost:(EluxAppliance*) appliance stillVisibleAppliances:(NSMutableArray*)stillVisibleAppliances;

/**
 * Called when some non recoverable error occurs
 *
 * @param error NSError describing what happened
 */
-(void)onDiscoveryFailed:(NSError*)error;


/**
 * Called when an appliance needs user to provide some authentication information. Usually not called at all because the valid default password is alreday set. 
 *
 * @param peerName Name of the appliance
 * @param userName User name privided by the appliance. Can be nil or empty
 * @param mask Required credentials type
 * @return Should return a valid instance of EluxCredentialsContainer
 */
-(EluxCredentialsContainer*)onApplianceNeedsCredentialsForPeer:(NSString *)peerName userName:(NSString *)userName andCredentialTypeMask:(EluxCredentialsTypeMask)mask;

@end

#ifdef NOT_PROTOCOL_ONLY
@interface EluxApplianceDiscoveryListener : NSObject <EluxApplianceDiscoveryListener>

@end
#endif //NOT_PROTOCOL_ONLY
