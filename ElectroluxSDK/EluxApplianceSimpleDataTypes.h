//
//  EluxApplianceSimpleDataTypes.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#ifndef EluxApplianceSimpleDataTypes_h
#define EluxApplianceSimpleDataTypes_h

enum EluxApplianceType : int
{
    UNKNOWN = 0,
    NIUX_ONLY = 1,
    WM1 = 2,
    TD1 = 3,
    OTHER = 4,
};


enum EluxApplianceState
{
    /** Created - user should never get device in this state */
    CREATED,
    /** Discovered in the network, but not yet connected */
    DISCOVERED,
    /** Connection was requested, but it was not finished yet */
    CONNECTING,
    /** Ready to be used - methods can be invoked, properties set */
    CONNECTED,
    /** Disconnect called on device, remote device dos not confirm disconnection yet */
    DISCONNECTING,
    /** Device lost - e.g., due to network error or internal SW error */
    LOST
};

#endif /* EluxApplianceSimpleDataTypes_h */
