//
//  EluxAjAppliance.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxAjAppliance.h"
#import "EluxAppliance.h"
#import "EluxAjApplianceType.h"
#import "EluxAjEventHandler.h"

@implementation EluxBasicInfo

-(NSString*)getBusName { return mBusName; }
-(NSString*)getSerial { return mSerial; }
-(NSString*)getPnc { return mPnc; }
-(NSString*)getElc { return mElc; }
-(void)setBusName:(NSString*)busName { mBusName = busName; }
-(void)setSerial:(NSString*)serial { mSerial = serial; }
-(void)setPnc:(NSString*)pnc { mPnc = pnc; }
-(void)setElc:(NSString*)elc { mElc = elc; }
-(BOOL)isEqual:(id)object
{
    if([object isKindOfClass:[EluxBasicInfo class]])
        return NO;
    EluxBasicInfo* tmp = (EluxBasicInfo*)object;
    if(![[tmp getBusName] isEqualToString:[self getBusName]])
        return NO;
    if(![[tmp getSerial] isEqualToString:[self getSerial]])
        return NO;
    if(![[tmp getPnc] isEqualToString:[self getPnc]])
        return NO;
    if(![[tmp getElc] isEqualToString:[self getElc]])
        return NO;
    return YES;
}
@end

@implementation EluxAjAppliance

-(EluxAjAppliance*)initWithBusName:(NSString*)busName eventHandler:(EluxAjEventHandler*)eventHandler andAppliance:(EluxAppliance*)appliance
{
    self = [super init];
    if(self)
    {
        TAG = NSStringFromClass([EluxAjAppliance class]);
        ONBOARDING_OBJ_PATH = @"/Onboarding";
        mProxyBusObjects = [[NSMutableDictionary alloc]init];
        mIfacesClasses = [[NSMutableDictionary alloc]init];
        mObjPaths = [[NSMutableArray alloc]init];
        mInterfaces = [[NSMutableDictionary alloc]init];
        mProperties = [[NSMutableDictionary alloc]init];
        mMethods = [[NSMutableDictionary alloc]init];
        mObservers = [[NSMutableArray alloc]init];
        
        mBasicInfo = [[EluxBasicInfo alloc]init];
        mAjEvenHandler = eventHandler;
        mAboutData = [[NSMutableDictionary alloc]init];
        mAppliance = appliance;
        //AboutObjectDescription[] mObjectDescriptions;
    }
    return self;
}

-(BOOL)isSupported
{
    return [mAjApplianceType isKindOfClass:[EluxAjApplianceTypeUnknown class]];
}

-(EluxApplianceType)getType
{
    if([mAjApplianceType isKindOfClass:[EluxAjApplianceTypeWM class]])
        return WM1;
    if([mAjApplianceType isKindOfClass:[EluxAjApplianceTypeTD class]])
        return TD1;
    if([mAjApplianceType isKindOfClass:[EluxAjApplianceTypeNIU class]])
        return NIUX_ONLY;
    return OTHER;
}

-(BOOL)connect
{
    return NO;
}

-(BOOL)isConnected
{
    return NO;
}

-(BOOL)disconnect
{
    return NO;
}

-(BOOL)getRemotePropertyValue:(EluxRemoteProperty*) prop withRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener
{
    return NO;
}

-(BOOL)setRemotePropertyValue:(EluxRemoteProperty*) prop
     withRemoteValueContainer:(EluxRemoteValueContainer*)value
  andRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener
{
    return NO;
}

-(BOOL)setApplianceStateListener:(id<EluxApplianceStateDelegate>)listener
{
    return NO;
}

-(BOOL)removeApplianceStateListener:(id<EluxApplianceStateDelegate>)listener
{
    return NO;
}

-(NSArray*)getSupportedSubUnitPaths
{
    return nil;
}

-(NSArray*)getRemoteMethods:(NSString*)subUnitPath
{
    return nil;
}

-(NSArray*)getRemoteProperties:(NSString*)subUnitPath
{
    return nil;
}

-(BOOL)invokeMethod         :(EluxRemoteMethod*)remoteMethod
               withArguments:(NSArray*)args
 andRemoteOpProgressListener:(id<EluxRemoteOpProgressDelegate>)listener
{
    return NO;
}

-(NSArray*)getMethodArgumentTypes:(EluxRemoteMethod*)remoteMethod __deprecated //return NSArray of ids
{
    return nil;
}

-(BOOL)isOnboarded
{
    return NO;
}

-(BOOL)isOnboardable
{
    return NO;
}

-(BOOL)isOffboardable
{
    return NO;
}

-(NSInteger)getUniqueId
{
    return -1;
}

/**
 * Request a control token
 */
-(NSInteger)getControlToken { return mControllToken; }

/**
 * Release control token
 */
-(void)releaseControlToken { }

/**
 * Get the Proxy Bus Object for a specific object path
 *
 * @param opath Object path
 * @return The alljoyn proxy bus object that refers to the object path
 */

//-(ProxyBusObject*) getProxyBusObject:(NSString*) opath;


/**
 * Add proxy bus object related to this appliance
 * @param opath obejct path
 * @param pbo proxy bus object
 */
//-(void)addProxyBusObject:(ProxyBusObject*) pbo withObjectPath:(NSString*) oPath;

-(void)removeProxyBusObj:(NSString*) oPath { }

-(NSArray*)getObjPaths  { return mObjPaths; }

/**
 * Set new set of observed objects. If objects is null, it just unregister all listeners and
 * calcel observing.
 *
 * @param objects objects to be observed.
 */
//-(BOOL)bindObservedObjects:(NSArray*) objects withBus:(BusAttachment*) bus;

/**
 * Getter for associated EluxAppliance
 * @return associated EluxAppliance
 */
-(EluxAppliance*)getAppliance { return mAppliance; }

//-(BOOL) createAjObservers:(NSArray*) trackedObjects withBus:(BusAttachment*) bus;

/*
 * Bind appliance - start appliance introspection and prepare methods and properties to be used.
 */
-(BOOL) bindAppliance  { return NO; }

/**
 * Join an session on the announced port and create the corresponding {@link EluxAjAppliance} object
 *
 * @param busAttachment BusAttachment object reference
 * @param port          Alljoyn port to use to create the session
 * @param busName       Device busName
 */
//-(BOOL) joinSession:(BusAttachment*) busAttachment onPort:(NSInteger) port withBusNAme:(NSString*) busName;


//-(BOOL)requestRemoteIntrospectionString:(BusAttachment*) busAttachment withBusNAme:(NSString*) busName andProxyObject:(ProxyBusObject*) pbo;

/**
 * Create the proxy bus objects
 */
-(void) createProxyBusObjects:(NSInteger) sessionId { }

/**
 * Get the device Bus Name
 *
 * @return Representing the busName in the AJ bus
 */
-(NSString*)getBusName { return [mBasicInfo getBusName]; }

/**
 * Get the Aj port of the appliance
 *
 * @return Aj port number
 */
-(NSInteger) getAjPort { return mAjPort; }

/**
 * Set the Aj port for the appliance
 * @param port Aj port number
 */
-(void) setAjPort:(NSInteger) port { mAjPort = port; }

-(NSString*) getAppId { return mAppId; }

-(NSString*) getDeviceName { return mDeviceName; }


-(NSDictionary*) getAboutData { return mAboutData; }

-(void)setAppliance:(EluxAppliance*)appliance { mAppliance = appliance; };

-(void) setAppId:(NSString*) appId { mAppId = appId; }

-(void) setDeviceName:(NSString*) deviceName { mDeviceName = deviceName; }

-(void) setAboutData:(NSMutableDictionary*) aboutData { mAboutData = aboutData; }

/**
 * Set the appliance type
 *
 * @param mAjApplianceType Appliance type
 */
-(void) setApplianceType:(EluxAjApplianceType*) ajApplianceType { mAjApplianceType = ajApplianceType; }


/**
 * Set the Hacl object paths
 *
 * @param objectDescriptions {@link AboutObjectDescription} object reference
 */
-(void) setObjectDescription:(NSArray*) objectDescriptions { };

/**
 * Set the Hacl object paths and will internal maping to implemented interfaces.
 *
 * @param objectDescriptions {@link AboutObjectDescription} object reference
 */
-(void) fillMappingWithObjectDescription:(NSArray*) objectDescriptions { }

/**
 * Create, for each object path, an array with the Java class references of the interfaces
 */
-(void) fillClassMapping:(NSString*) oPath { }

/**
 * Retrieve the object path that "contains" a specific interface
 *
 * @param iface Interface string name
 * @return Object path or empty string if not found
 */
-(NSString*) getObjectPath:(NSString*) iface { return nil;}

-(NSString*) getSerial { return [mBasicInfo getSerial]; }

-(void) setSerial:(NSString*) serial { [mBasicInfo setSerial:serial]; }

-(NSString*) getPnc { return [mBasicInfo getPnc]; }

-(void) setPnc:(NSString*) pnc { [mBasicInfo setPnc:pnc]; }

-(NSString*) getElc { return [mBasicInfo getElc]; }

-(void) setElc:(NSString*) elc { [mBasicInfo setElc:elc]; }


/**
 * Update a property value on the remote device, NOT in the local cache
 * The local data structure must be updated if an EmitPropertyChanged signal is received.
 *
 * @param prop {@link AjProperty} to be updated
 * @param value {@link Variant} property value to be sent
 */
//-(void) setRemoteProperty:(AjProperty*) prop toValue:(Variant*) value;

/**
 * Update a property value on the remote device, NOT in the local data structure
 * The local data structure must be updated if an EmitPropertyChanged signal is received
 *
 * This version of the ajMethod run async task for this and provided listener is called to retrieve
 * notification about result.
 *
 * @param prop {@link AjProperty} to be updated
 * @param value {@link Variant} property value to be sent
 */
//-(void) setRemoteProperty:(AjProperty*) prop withVAliue:(Variant*) value andListener: (id<RemotePropertyValueOpListener>) delegate;


-(void) clearAndAnnounceDeviceLost { }

//-(BOOL)getRemotePropertyValue:(EluxRemoteProperty*) property withListener:(id<RemoteOpProgressListener>) delegate

//-(BOOL)setRemotePropertyValue:(EluxRemoteProperty*) property withValueContaiuner: (RemoteValueContainer*) value  andLisetener:(id<RemoteOpProgressListener>) delegate;


//-(BOOL) invokeMethod:(EluxRemoteMethod*) remoteMethod withValueContainer:(NSArray*) args, andListener:(id<RemoteOpProgressListener>) listener


/**
 * Request refresh of Aj property value
 * @param opath Object  Path of the property
 * @param iface Interface of the property
 * @param propertyName AjProperty name
 * @param listener Listener to notify result
 *
 */
// TODO consider moving this to the property class
//-(void)getProperty:(NSString*) oPath iFace: (NSString*) iface propName:(NSString*) propertyName andListener: (id<IGetPropertyListener>) delegate;

-(BOOL) isConnectionInProgress { return NO; }

/**
 * Retrieve for a specific object path, the interfaces associated
 * @param opath Object path string
 * @return
 */
-(NSArray*) getInterfaces:(NSString*) opath { return nil; }

/**
 * Retrieves all the device alljoyn methods
 */
-(void)listRemoteMethodsWithoutToken:(BOOL) skipMethodsWithoutToken skipMethodsWithToken: (BOOL) skipMethodsWithToken { }

/**
 * Retrieves methods for a given bus object
 */
-(BOOL) listRemoteMethodsForBusObject:(NSString*) oPath skipMethodsWithoutToken:(BOOL) skipMethodsWithoutToken skipMethodsWithToken: (BOOL) skipMethodsWithToken { return NO; }


-(BOOL)listAndReadremotePropertyValueForObjectPath:(NSString*) oPath skipVersionProperty:(BOOL) skipVersionProperty  { return NO; }

/**
 * Get a {@link BasicInfo} representation of the current {@link AjAppliance}
 *
 * @return {@link BasicInfo} object
 */
-(EluxBasicInfo*) getBasicInfo { return mBasicInfo; }

/**
 * Displays device identification ids
 *
 * @return String containing appliance serial number, pnc, elc and busname
 */
-(NSString*) getDeviceId
{
    return [NSString stringWithFormat:@"(%@ %@ %@ %@)", [self getSerial], [self getPnc], [self getElc], [self getBusName]];
}


@end
