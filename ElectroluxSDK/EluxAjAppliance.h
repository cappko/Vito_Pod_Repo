//
//  EluxAjAppliance.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

#import "EluxPlatformApplianceProtocol.h"

@class EluxAjEventHandler;
@class EluxAppliance;
@class EluxAjApplianceType;

@interface EluxBasicInfo: NSObject
{
    NSString* mBusName;
    NSString* mSerial;
    NSString* mPnc;
    NSString* mElc;
}
-(NSString*)getBusName;
-(NSString*)getSerial;
-(NSString*)getPnc;
-(NSString*)getElc;
-(void)setBusName:(NSString*)busName;
-(void)setSerial:(NSString*)serial;
-(void)setPnc:(NSString*)pnc;
-(void)setElc:(NSString*)elc;
-(BOOL)isEqual:(id)object;
@end

@interface EluxAjAppliance: NSObject <EluxPlatformApplianceProtocol>
{
    NSString* TAG;
    NSString* ONBOARDING_OBJ_PATH;
    /* <object path, proxy bus object reference> */
    NSMutableDictionary* mProxyBusObjects;
    /* <object path, list of interfaces that a proxy bus object implements> */
    NSMutableDictionary* mIfacesClasses;
    /* list of object paths the device implements (the one that starts with /Hack4 */
    NSMutableArray* mObjPaths;
    /* map between object paths and interfaces */
    NSMutableDictionary* mInterfaces;
    /* opath -> <property -> platform propety> */
    NSMutableDictionary* mProperties;
    NSMutableDictionary* mMethods;
    BOOL mStopUpdateProps;
    NSMutableArray* mObservers;

    EluxBasicInfo* mBasicInfo;
    NSString* mDeviceName;
    NSString* mAppId;
    NSInteger mAjPort;
    EluxAjEventHandler* mAjEvenHandler;
    NSMutableDictionary* mAboutData;
    id mAjApplianceType;
    BOOL mBound;
//private Appliance.IApplianceStateListener mApplianceListener;
    EluxAppliance* mAppliance;
    //AboutObjectDescription[] mObjectDescriptions;
    BOOL mConnectionInProgress;
    NSInteger mControllToken;
    NSInteger mSessionId;
    NSString* mDeviceIntrospectionDescription;
    enum EluxApplianceState mState;
}

-(EluxAjAppliance*)initWithBusName:(NSString*)busName eventHandler:(EluxAjEventHandler*)eventHandler andAppliance:(EluxAppliance*)appliance;

/**
 * Request a control token
 */
-(NSInteger)getControlToken;

/**
 * Release control token
 */
-(void)releaseControlToken;

/**
 * Get the Proxy Bus Object for a specific object path
 *
 * @param opath Object path
 * @return The alljoyn proxy bus object that refers to the object path
 */

//-(ProxyBusObject*) getProxyBusObject:(NSString*) opath;


/**
 * Add proxy bus object related to this appliance
 * @param opath obejct path
 * @param pbo proxy bus object
 */
//-(void)addProxyBusObject:(ProxyBusObject*) pbo withObjectPath:(NSString*) oPath;

-(void)removeProxyBusObj:(NSString*) oPath;

-(NSArray*)getObjPaths;

/**
 * Set new set of observed objects. If objects is null, it just unregister all listeners and
 * calcel observing.
 *
 * @param objects objects to be observed.
 */
//-(BOOL)bindObservedObjects:(NSArray*) objects withBus:(BusAttachment*) bus;

/**
 * Getter for associated EluxAppliance
 * @return associated EluxAppliance
 */
-(EluxAppliance*)getAppliance;

//-(BOOL) createAjObservers:(NSArray*) trackedObjects withBus:(BusAttachment*) bus;

/*
 * Bind appliance - start appliance introspection and prepare methods and properties to be used.
 */
-(BOOL) bindAppliance;

/**
 * Join an session on the announced port and create the corresponding {@link EluxAjAppliance} object
 *
 * @param busAttachment BusAttachment object reference
 * @param port          Alljoyn port to use to create the session
 * @param busName       Device busName
 */
//-(BOOL) joinSession:(BusAttachment*) busAttachment onPort:(NSInteger) port withBusNAme:(NSString*) busName;


//-(BOOL)requestRemoteIntrospectionString:(BusAttachment*) busAttachment withBusNAme:(NSString*) busName andProxyObject:(ProxyBusObject*) pbo;

/**
 * Create the proxy bus objects
 */
-(void) createProxyBusObjects:(NSInteger) sessionId;


/**
 * Get the device Bus Name
 *
 * @return Representing the busName in the AJ bus
 */
-(NSString*)getBusName;

/**
 * Get the Aj port of the appliance
 *
 * @return Aj port number
 */
-(NSInteger) getAjPort;

/**
 * Set the Aj port for the appliance
 * @param port Aj port number
 */
-(void) setAjPort:(NSInteger) port;

-(NSString*) getAppId;

-(NSString*) getDeviceName;


-(NSDictionary*) getAboutData;

-(void)setAppliance:(EluxAppliance*)appliance;

-(void) setAppId:(NSString*) appId;

-(void) setDeviceName:(NSString*) deviceName;

-(void) setAboutData:(NSDictionary*) aboutData;

/**
 * Set the appliance type
 *
 * @param mAjApplianceType Appliance type
 */
-(void) setApplianceType:(EluxAjApplianceType*) ajApplianceType;


/**
 * Set the Hacl object paths
 *
 * @param objectDescriptions {@link AboutObjectDescription} object reference
 */
-(void) setObjectDescription:(NSArray*) objectDescriptions;

/**
 * Set the Hacl object paths and will internal maping to implemented interfaces.
 *
 * @param objectDescriptions {@link AboutObjectDescription} object reference
 */
-(void) fillMappingWithObjectDescription:(NSArray*) objectDescriptions;

/**
 * Create, for each object path, an array with the Java class references of the interfaces
 */
-(void) fillClassMapping:(NSString*) oPath;

/**
 * Retrieve the object path that "contains" a specific interface
 *
 * @param iface Interface string name
 * @return Object path or empty string if not found
 */
-(NSString*) getObjectPath:(NSString*) iface;

-(NSString*) getSerial;

-(void) setSerial:(NSString*) serial;

-(NSString*) getPnc;

-(void) setPnc:(NSString*) pnc;

-(NSString*) getElc;

-(void) setElc:(NSString*) elc;

-(BOOL)isSupported;

-(enum EluxApplianceType)getType;


-(BOOL) connect;

/**
 * Update a property value on the remote device, NOT in the local cache
 * The local data structure must be updated if an EmitPropertyChanged signal is received.
 *
 * @param prop {@link AjProperty} to be updated
 * @param value {@link Variant} property value to be sent
 */
//-(void) setRemoteProperty:(AjProperty*) prop toValue:(Variant*) value;

/**
 * Update a property value on the remote device, NOT in the local data structure
 * The local data structure must be updated if an EmitPropertyChanged signal is received
 *
 * This version of the ajMethod run async task for this and provided listener is called to retrieve
 * notification about result.
 *
 * @param prop {@link AjProperty} to be updated
 * @param value {@link Variant} property value to be sent
 */
//-(void) setRemoteProperty:(AjProperty*) prop withVAliue:(Variant*) value andListener: (id<RemotePropertyValueOpListener>) delegate;

-(BOOL) disconnect;

-(void) clearAndAnnounceDeviceLost;

//-(BOOL)getRemotePropertyValue:(EluxRemoteProperty*) property withListener:(id<RemoteOpProgressListener>) delegate

//-(BOOL)setRemotePropertyValue:(EluxRemoteProperty*) property withValueContaiuner: (RemoteValueContainer*) value  andLisetener:(id<RemoteOpProgressListener>) delegate;


-(NSArray*) getSupportedSubUnitPaths;

-(NSMutableArray*) getRemoteMethods:(NSString*) subUnitPath;

-(NSMutableArray*) getRemoteProperties:(NSString*) subUnitPath;

//-(BOOL) invokeMethod:(EluxRemoteMethod*) remoteMethod withValueContainer:(NSArray*) args, andListener:(id<RemoteOpProgressListener>) listener

-(NSArray*) getMethodArgumentTypes:(EluxRemoteMethod*) remoteMethod;

-(BOOL)isOnboarded;

-(BOOL)isOnboardable;

-(BOOL)isOffboardable;

-(BOOL)isConnected;

/**
 * Request refresh of Aj property value
 * @param opath Object  Path of the property
 * @param iface Interface of the property
 * @param propertyName AjProperty name
 * @param listener Listener to notify result
 *
 */
// TODO consider moving this to the property class
//-(void)getProperty:(NSString*) oPath iFace: (NSString*) iface propName:(NSString*) propertyName andListener: (id<IGetPropertyListener>) delegate;

-(BOOL) isConnectionInProgress;

/**
 * Retrieve for a specific object path, the interfaces associated
 * @param opath Object path string
 * @return
 */
-(NSArray*) getInterfaces:(NSString*) opath;

/**
 * Retrieves all the device alljoyn methods
 */
-(void)listRemoteMethodsWithoutToken:(BOOL) skipMethodsWithoutToken skipMethodsWithToken: (BOOL) skipMethodsWithToken;

/**
 * Retrieves methods for a given bus object
 */
-(BOOL) listRemoteMethodsForBusObject:(NSString*) oPath skipMethodsWithoutToken:(BOOL) skipMethodsWithoutToken skipMethodsWithToken: (BOOL) skipMethodsWithToken;


-(BOOL)listAndReadremotePropertyValueForObjectPath:(NSString*) oPath skipVersionProperty:(BOOL) skipVersionProperty;

/**
 * Get a {@link BasicInfo} representation of the current {@link AjAppliance}
 *
 * @return {@link BasicInfo} object
 */
-(EluxBasicInfo*) getBasicInfo;

/**
 * Displays device identification ids
 *
 * @return String containing appliance serial number, pnc, elc and busname
 */
-(NSString*) getDeviceId;

/**
 * A "light" representation of a device
 * specifically designed for displaying on the recyclerview
 * <p>
 * Created by Possamai Lino on 05/07/2016.
 */
@end

@interface EluxAjMethod
{
    NSString* mObjectPath;
    NSString* mInterface;
    NSString* mReplySignature;
    NSString* mSignature;
    id mSignatureEnums;
    id mParamValues;
    
    NSString* OTA_RELEASE_CONTROL_TOKEN;
    NSString* OTA_GET_CONTROL_TOKEN;
}
@end
