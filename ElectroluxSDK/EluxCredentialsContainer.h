//
//  EluxCredentialsContainer.h
//  simple-app
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

typedef int32_t EluxCredentialsTypeMask;
const int kEluxCredentialsTypePassword          = 0x1;
const int kEluxCredentialsTypeUserName          = 0x2;
const int kEluxCredentialsX509                  = 0x4;
const int kEluxCredentialsTypePrivateKey        = 0x8;
const int kEluxCredentialsTypeExpirationDate    = 0x10;

@interface EluxCredentialsContainer : NSObject

/**
 * a password, pincode, or passphrase
 */
@property(nonatomic, retain)NSString* password;

/**
 * a user name
 */
@property(nonatomic, retain)NSString* userName;

/**
 * a PEM-encoded X509 certificates chain
 */
@property(nonatomic, retain)NSString* x509;

/**
 * a PEM-encoded private key
 */
@property(nonatomic, retain)NSString* privateKey;

/**
 * a logon entry that can be used to logon a remote user
 */
@property(nonatomic, retain)NSString* logonEntry;

/**
 * an expiration time
 */
@property(nonatomic, retain)NSDate* expirationTime;

/**
 * Simple init functions, that automatically sets property "password"
 *
 * @param pass A password that is going to be used as a value for the "password" property
 * @return Returns a valid instance of EluxCredentialsContainer or nil if fails
 */
-(EluxCredentialsContainer*)initWithPassword:(NSString*)pass;

/**
 * Simple init functions, that allows you to set all properties at once
 *
 * @param pass  A password that is going to be used as a value for the "password" property
 * @param name  A name that is going to be used as a value for the "userName" property
 * @param cert  A PEM-encoded X509 certificates chain that is going to be used as a value for the "x509" property
 * @param pKey  A PEM-encoded private key that is going to be used as a value for the "privateKey" property
 * @param lgEntry A logon entry that can be used to logon a remote user that is going to be used as a value for the "logonEntry" property
 * @param expTime An expiration time that is going to be used as a value for the "expirationTime" property
 @return Returns a valid instance of EluxCredentialsContainer or nil if fails
 */
-(EluxCredentialsContainer*)initWithPassword:(NSString*)pass userName:(NSString*)name x509:(NSString*)cert privateKey:(NSString*)pKey logonEntry:(NSString*)lgEntry andExpirationTime:(NSDate*)expTime;


@end
