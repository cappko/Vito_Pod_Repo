//
//  EluxAjApplianceType.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>

@interface EluxAjApplianceType : NSObject
{
    NSString* mHeader;
    NSString* mType;
    BOOL mIsPrimary;
    NSArray* mSupportedPaths;
}

/**
 *
 * Returns all known Object paths...
 * @return all known Object paths...
 */
+(NSArray*)getAllSupportedObjectPaths;
+(NSString*)getObjectPath;
+(BOOL)isPrimaryPath;

/**
 * Retrieve the EluxAjApplianceType from a NSArray of object paths
 *
 * @param objects {@link AboutObjectDescription} object reference
 * @return An EluxAjApplianceType or nil
 */
+(EluxAjApplianceType*)getTypeFromAboutDescription:(NSArray*)objects;
/**
 * Check if an object path is supported by the app, that is,
 * understood by the app
 *
 * @param opath Object path to check
 * @return true if the object path is supported, false otherwise
 */
+(BOOL)isObjPathSupported:(NSString*)objPath;
@end


@interface EluxAjApplianceTypeWM : EluxAjApplianceType
@end

@interface EluxAjApplianceTypeTD : EluxAjApplianceType
@end

@interface EluxAjApplianceTypeOV : EluxAjApplianceType
@end

@interface EluxAjApplianceTypeRF : EluxAjApplianceType
@end

@interface EluxAjApplianceTypeWD : EluxAjApplianceType
@end

@interface EluxAjApplianceTypeDW : EluxAjApplianceType
@end

@interface EluxAjApplianceTypeAC : EluxAjApplianceType
@end

@interface EluxAjApplianceTypeHB : EluxAjApplianceType
@end

@interface EluxAjApplianceTypeCOW : EluxAjApplianceType
@end

@interface EluxAjApplianceTypeNIU : EluxAjApplianceType
@end

@interface EluxAjApplianceTypeONBOARDING : EluxAjApplianceType
@end

@interface EluxAjApplianceTypeUnknown : EluxAjApplianceType
@end

