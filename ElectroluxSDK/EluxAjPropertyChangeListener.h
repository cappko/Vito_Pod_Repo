//
//  EluxAjPropertyChangeListener.h
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import <Foundation/Foundation.h>
#import <AJNObserver.h>
#import <AJNProxyBusObject.h>

@class EluxSubUnit;
@protocol EluxPropertyChangeDelegate;

@interface EluxListenerInfoContainer : NSObject

@property(nonatomic, retain) id<EluxPropertyChangeDelegate> listener;
@property(nonatomic, retain) NSString* propertyName;
@property(nonatomic, retain) EluxSubUnit* subUnit;

@end


@interface EluxAjPropertyChangeListener : NSObject <AJNPropertiesChangedDelegate>
{
    NSMutableArray<EluxListenerInfoContainer*>* mListenersInfo;
}

-(BOOL)registerListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit;
-(void)unRegisterListener:(id<EluxPropertyChangeDelegate>)listener forProperty:(NSString*)propertyName ofSubUnit:(EluxSubUnit*)subUnit;


@end
