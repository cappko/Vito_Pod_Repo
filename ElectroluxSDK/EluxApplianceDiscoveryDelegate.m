//
//  EluxApplianceDiscoveryDelegate/m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxApplianceDiscoveryDelegate.h"

#ifdef NOT_PROTOCOL_ONLY
@implementation EluxApplianceDiscoveryListener

/**
 * Called on device discovered
 *
 * @param appliance discovered appliance
 */
-(void)onApplianceDiscovered:(EluxAppliance*) appliance
{
    
}

/**
 * Called on device lost (can not be accessed or (re) connected anymore).
 *
 * This is typically called if you disconnect from network, where device is onboarded
 *
 * @param appliance The lost appliance
 * @param stillVisibleAppliances List of appliances, that are currently accessible/visible
 */
-(void)onApplianceLost:(EluxAppliance*) appliance stillVisibleAppliances:(NSMutableArray*)stillVisibleAppliances
{
    
}
@end

#endif //NOT_PROTOCOL_ONLY
