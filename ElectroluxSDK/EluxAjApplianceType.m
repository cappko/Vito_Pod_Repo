//
//  EluxAjApplianceType.m
//  ElectroluxSDK
//
//  EluxAjApplianceType.m
//  ElectroluxSDK
//
//  This code is proprietary and cannot be published, used, modified or distributed
//  without written permission of Electrolux AB
//  S:t Göransgatan 143, Stadshagen Stockholm, Sweden.
//

#import "EluxAjApplianceType.h"

#define kHeaders @"/HaclV4/", @"/HaclV4/", @"/HaclV4/", @"/HaclV4/", @"/HaclV4/", @"/HaclV4/", @"/HaclV4/", @"/HaclV4/", @"/Cowera", @"/HaclV4/", @"/Onboarding", @"/", nil
#define kTypes   @"WM1",      @"TD1",      @"OV1",      @"RF1",      @"WD1",      @"DW1",      @"AC1",      @"HB1",      @"",        @"NIU",      @"",            @"",  nil

#define WM_INDEX            0
#define TD_INDEX            1
#define OV_INDEX            2
#define RF_INDEX            3
#define WD_INDEX            4
#define DW_INDEX            5
#define AC_INDEX            6
#define HB_INDEX            7
#define COW_INDEX           8
#define NIU_INDEX           9
#define ONBOARDING_INDEX    10
#define UNKNOWN_INDEX       11



@implementation EluxAjApplianceType

/**
 *
 * Returns all known Object paths...
 * @return all known Object paths...
 */
+(NSArray*)getAllSupportedObjectPaths
{
    NSArray* headers = [NSArray arrayWithObjects:kHeaders, nil];
    NSArray* types = [NSArray arrayWithObjects:kTypes, nil];
    NSMutableArray* supportedPaths = [[NSMutableArray alloc]initWithCapacity:headers.count];
    for(int i = 0; i < headers.count; i++)
    {
        NSString* path = [NSString stringWithFormat:@"%@%@", [headers objectAtIndex:i], [types objectAtIndex:i]];
        [supportedPaths addObject:path];
    }
    return supportedPaths;
}
//Supposed to be overriden
+(NSString*)getObjectPath { return nil; }
//Supposed to be overriden
+(BOOL)isPrimaryPath { return NO; }

+(EluxAjApplianceType*)getTypeFromAboutDescription:(NSArray*)objects
{
    return nil;
}

/**
 * Check if an object path is supported by the app, that is,
 * understood by the app
 *
 * @param opath Object path to check
 * @return true if the object path is supported, false otherwise
 */
+(BOOL)isObjPathSupported:(NSString*)objPath { return NO; };
@end


@implementation EluxAjApplianceTypeWM
+(NSString*)getObjectPath
{
    return [[EluxAjApplianceType getAllSupportedObjectPaths]objectAtIndex:WM_INDEX];
}
+(BOOL)isPrimaryPath { return YES; }

@end

@implementation EluxAjApplianceTypeTD
+(NSString*)getObjectPath
{
    return [[EluxAjApplianceType getAllSupportedObjectPaths]objectAtIndex:TD_INDEX];
}
+(BOOL)isPrimaryPath { return YES; }
@end

@implementation EluxAjApplianceTypeOV
+(NSString*)getObjectPath
{
    return [[EluxAjApplianceType getAllSupportedObjectPaths]objectAtIndex:OV_INDEX];
}
+(BOOL)isPrimaryPath { return YES; }
@end

@implementation EluxAjApplianceTypeRF
+(NSString*)getObjectPath
{
    return [[EluxAjApplianceType getAllSupportedObjectPaths]objectAtIndex:RF_INDEX];
}
+(BOOL)isPrimaryPath { return YES; }
@end

@implementation EluxAjApplianceTypeWD
+(NSString*)getObjectPath
{
    return [[EluxAjApplianceType getAllSupportedObjectPaths]objectAtIndex:WD_INDEX];
}
+(BOOL)isPrimaryPath { return YES; }
@end

@implementation EluxAjApplianceTypeDW
+(NSString*)getObjectPath
{
    return [[EluxAjApplianceType getAllSupportedObjectPaths]objectAtIndex:DW_INDEX];
}
+(BOOL)isPrimaryPath { return YES; }
@end

@implementation EluxAjApplianceTypeAC
+(NSString*)getObjectPath
{
    return [[EluxAjApplianceType getAllSupportedObjectPaths]objectAtIndex:AC_INDEX];
}
+(BOOL)isPrimaryPath { return YES; }
@end

@implementation EluxAjApplianceTypeHB
+(NSString*)getObjectPath
{
    return [[EluxAjApplianceType getAllSupportedObjectPaths]objectAtIndex:HB_INDEX];
}
+(BOOL)isPrimaryPath { return YES; }
@end

@implementation EluxAjApplianceTypeCOW
+(NSString*)getObjectPath
{
    return [[EluxAjApplianceType getAllSupportedObjectPaths]objectAtIndex:COW_INDEX];
}
+(BOOL)isPrimaryPath { return YES; }
@end

@implementation EluxAjApplianceTypeNIU
+(NSString*)getObjectPath
{
    return [[EluxAjApplianceType getAllSupportedObjectPaths]objectAtIndex:NIU_INDEX];
}
+(BOOL)isPrimaryPath { return NO; }
@end

@implementation EluxAjApplianceTypeONBOARDING
+(NSString*)getObjectPath
{
    return [[EluxAjApplianceType getAllSupportedObjectPaths]objectAtIndex:ONBOARDING_INDEX];
}
+(BOOL)isPrimaryPath { return NO; }
@end

@implementation EluxAjApplianceTypeUnknown
+(NSString*)getObjectPath
{
    return [[EluxAjApplianceType getAllSupportedObjectPaths]objectAtIndex:UNKNOWN_INDEX];
}
+(BOOL)isPrimaryPath { return NO; }
@end
