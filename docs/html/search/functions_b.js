var searchData=
[
  ['searchforavailablessid_3a',['searchForAvailableSSID:',['../interface_elux_onboarding_manager.html#af9fcc08db7ab3a243ddaf168a62b93cb',1,'EluxOnboardingManager']]],
  ['setappliancemanager_3a',['setApplianceManager:',['../interface_elux_platforms_manager.html#ae3034b00b9386025008e2e6b4c8e2461',1,'EluxPlatformsManager']]],
  ['setappliancestatelistener_3a',['setApplianceStateListener:',['../interface_elux_appliance.html#a92a18e6968beab26b9e1b168efaa9e4f',1,'EluxAppliance::setApplianceStateListener:()'],['../protocol_elux_platform_appliance_protocol_01-p.html#aa7fa7a254827708f710284d22635827e',1,'EluxPlatformApplianceProtocol -p::setApplianceStateListener:()']]],
  ['setlistener_3a',['setListener:',['../interface_elux_onboardable_discovery_manager.html#aec1dcb5a4a399655db8e569b17453d35',1,'EluxOnboardableDiscoveryManager']]],
  ['setpassword_3aforssid_3a',['setPassword:forSSID:',['../interface_elux_onboarding_manager.html#ad6a17390e2e4cab37113e47551679ed0',1,'EluxOnboardingManager']]],
  ['setplatformsmanager_3a',['setPlatformsManager:',['../interface_elux_appliance_manager.html#a7626f2e43816225ce8606e9d19c45fdd',1,'EluxApplianceManager']]],
  ['setremotepropertyvalue_3awithremotevaluecontainer_3aandremoteopprogresslistener_3a',['setRemotePropertyValue:withRemoteValueContainer:andRemoteOpProgressListener:',['../interface_elux_appliance.html#a71d5df45e18e71cb0322f7ad941e6c6f',1,'EluxAppliance::setRemotePropertyValue:withRemoteValueContainer:andRemoteOpProgressListener:()'],['../protocol_elux_platform_appliance_protocol_01-p.html#ab1779c870dc3eafc77fdfc6285f30783',1,'EluxPlatformApplianceProtocol -p::setRemotePropertyValue:withRemoteValueContainer:andRemoteOpProgressListener:()']]],
  ['setuniqueid_3a',['setUniqueId:',['../interface_elux_appliance.html#a8480c57ea49a17997658d7e4de5d6227',1,'EluxAppliance']]],
  ['setuniqueidstring_3a',['setUniqueIdString:',['../interface_elux_appliance.html#add6c739684019c33248330ae925b6d9a',1,'EluxAppliance']]],
  ['sharedinstance',['sharedInstance',['../interface_elux_platforms_manager.html#a1beb2f93da14fd196980cfac34954f8b',1,'EluxPlatformsManager']]],
  ['sharedinstancewithappliancemanager_3a',['sharedInstanceWithApplianceManager:',['../interface_elux_platforms_manager.html#a24881cac66f026c792245b874efc59f3',1,'EluxPlatformsManager']]],
  ['sharedinstancewithplatformmanager_3a',['sharedInstanceWithPlatformManager:',['../interface_elux_onboarding_manager.html#aaea6f1742a7353a3e07b351d392425a5',1,'EluxOnboardingManager']]],
  ['startdiscovery',['startDiscovery',['../interface_elux_onboardable_discovery_manager.html#a6fa3b17fe153789c5b008309f5e5ce8c',1,'EluxOnboardableDiscoveryManager']]]
];
