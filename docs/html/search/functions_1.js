var searchData=
[
  ['canbeinvoked',['canBeInvoked',['../interface_elux_remote_method.html#a8e92fb5a369af4843239282b5ac41bf5',1,'EluxRemoteMethod']]],
  ['clean',['clean',['../interface_elux_sub_uint.html#abfeabb1956ff0d65c92994ede930ca49',1,'EluxSubUint']]],
  ['connect',['connect',['../interface_elux_appliance.html#ae7b9fd49085c9a57fafe3003f0035d81',1,'EluxAppliance::connect()'],['../protocol_elux_platform_appliance_protocol_01-p.html#a7cf90fe710ee40dd663f6dd23f10ad0f',1,'EluxPlatformApplianceProtocol -p::connect()'],['../interface_elux_platforms_manager.html#aafeeb7beb8be9a25ebf69fd4101d8164',1,'EluxPlatformsManager::connect()']]],
  ['connect_3a',['connect:',['../interface_elux_connectivity_platform_adapter.html#a69767b71083b4b4c4d90fda8090ae1d2',1,'EluxConnectivityPlatformAdapter']]],
  ['createappliance_3awithapplianceinfo_3a',['createAppliance:withApplianceInfo:',['../interface_elux_appliance_manager.html#a2889a79232ffc7bf1072d58f68529803',1,'EluxApplianceManager']]],
  ['createnewadapters_3a',['createNewAdapters:',['../interface_elux_connectivity_platform_adapter.html#af5c1f777cfe7e7bc280b15e6ff17b424',1,'EluxConnectivityPlatformAdapter']]],
  ['createsimpleerrorwithcode_3aandrerrormessage_3a',['createSimpleErrorWithCode:andRerrorMEssage:',['../interface_elux_errors.html#a2baa62dfc7b9ee5b0ea3c705e098df32',1,'EluxErrors']]]
];
