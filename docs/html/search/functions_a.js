var searchData=
[
  ['registerconnectionstatelistener_3a',['registerConnectionStateListener:',['../interface_elux_connectivity_platform_adapter.html#ab2b79e47b7631cfb7cbcfbe0784171ed',1,'EluxConnectivityPlatformAdapter::registerConnectionStateListener:()'],['../interface_elux_platforms_manager.html#ab102e65f1221c1b70d9562b556459696',1,'EluxPlatformsManager::registerConnectionStateListener:()']]],
  ['registerdiscoverylistener_3a',['registerDiscoveryListener:',['../interface_elux_platforms_manager.html#a77140aec566d03457afc704da3128a20',1,'EluxPlatformsManager']]],
  ['registerpropertylistener_3awithlistener_3a',['registerPropertyListener:withListener:',['../interface_elux_sub_uint.html#af48d8e818974f92844b6741aa0e88e41',1,'EluxSubUint']]],
  ['removeappliancestatelistener_3a',['removeApplianceStateListener:',['../interface_elux_appliance.html#a684feb7e5cef94ef71aaa84e51c01c2f',1,'EluxAppliance::removeApplianceStateListener:()'],['../protocol_elux_platform_appliance_protocol_01-p.html#a75aa9652cbb121b00b049c3dbdc2489d',1,'EluxPlatformApplianceProtocol -p::removeApplianceStateListener:()']]]
];
